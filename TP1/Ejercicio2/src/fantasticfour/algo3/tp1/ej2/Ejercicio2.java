/**
 * 
 */
package fantasticfour.algo3.tp1.ej2;

/**
 * @author bit-man
 *
 */
public class Ejercicio2 {

	private static final String PREFIJO_DEFAULT = "test/Tp1Ej2";

	/**
	 * @param args
	 * Esta clase es la que se usa como entrada desde la l�nea de comandos.
	 * Los argumentos cual indican el prefijo de los archivos de entrada 
	 * (donde se encuentra el problema), de salida (donde se escribirá la solución)
	 * y de cantidad de operaciones ejecutadas.
	 * 
	 * Ej.: java fantasticfour.algo3.tp1.Ejercicio2 /home/ff/test /home/ff/deltaPI
	 *      resolverá los problemas ubicados en el archivo /home/ff/test.in,
	 *      grabará las respuestas en el archivo /home/ff/test.out y
	 *      la cantidad de operaciones realizadas en /home/ff/test.op
	 *      Lo mismo ocurrir� con el archivo /home/ff/deltaPI.in, /home/ff/deltaPI.out
	 *      y /home/ff/deltaPI.op
	 *      
	 *  Ej.: java fantasticfour.algo3.tp1.Ejercicio2 
	 *      Al no colocar nombre de archivo se usará test/Tp1Ej2 por default,
	 *      resolviendo los problemas ubicados en el archivo test/Tp1Ej2.in,
	 *      grabando las respuestas en el archivo test/Tp1Ej2.out y
	 *      la cantidad de operaciones realizadas en test/Tp1Ej2.op
	 *      
	 */
	public static void main(String[] args) {
		
		String[] prefijoArchivo = null;
		
		if ( args.length == 0 ) {
			prefijoArchivo = new String[] { PREFIJO_DEFAULT };
		} else {
			prefijoArchivo = args;
		};

		for ( int i=0; i < prefijoArchivo.length; i++ ) {
			Ej2 ejercicio = new Ej2( prefijoArchivo[i] );
			ejercicio.resolver();
		}
	}
}
