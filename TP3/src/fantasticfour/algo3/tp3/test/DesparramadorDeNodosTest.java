
package fantasticfour.algo3.tp3.test;

import java.util.HashMap;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import fantasticfour.algo3.tp3.ej2.DesparramadorDeNodos;

public class DesparramadorDeNodosTest
{
	private static final int CANT_NODOS_MAX = 10;
	private static final int CANT_NODOS_MIN_DEL_TEST = 3;
	private LinkedList<LinkedList<Integer>> generados;
	
	@Before
	public void setup() {
		generados = new LinkedList<LinkedList<Integer>>();
	}

	/***
	 * Este test no muestra una situación real, porque si tengo 1 solo nodo
	 * tengo 2 huecos, y esa es la mínima situación.
	 * @throws Exception
	 */
	@Test
	public void testMinimoRidiculo() throws Exception {
		int nNodos = 4;
		int nHuecos = 1;
		
		DesparramadorDeNodos d = new DesparramadorDeNodos(nHuecos, nNodos);
		assertFalse( d.noHayMasCombinaciones() );
		assertTrue( d.nodosEnElHueco(0) == 4 );
		
		d.next();
		assertTrue( d.noHayMasCombinaciones() );
	}

	@Test
	public void testDosHuecosSinNodos() throws Exception {
		int nNodos = 0;
		int nHuecos = 2;
		
		DesparramadorDeNodos d = new DesparramadorDeNodos(nHuecos, nNodos);
		assertFalse( d.noHayMasCombinaciones() );
        assertTrue( cantidadNodos(d) == nNodos);
		assertTrue( secuenciaNoRepetida(d) );
        
        d.next();
		assertTrue( d.noHayMasCombinaciones() );
	}
	
	@Test
	public void testDosHuecos() throws Exception {
		int nNodos = 4;
		int nHuecos = 2;

		DesparramadorDeNodos d = new DesparramadorDeNodos(nHuecos, nNodos);
		assertFalse( d.noHayMasCombinaciones() );
        assertTrue( cantidadNodos(d) == nNodos);
		assertTrue( d.nodosEnElHueco(0) == 4 );
		assertTrue( d.nodosEnElHueco(1) == 0 );
		assertTrue( secuenciaNoRepetida(d) );

		d.next();
		assertFalse( d.noHayMasCombinaciones() );
        assertTrue( cantidadNodos(d) == nNodos);
		assertTrue( d.nodosEnElHueco(0) == 3 );
		assertTrue( d.nodosEnElHueco(1) == 1 );
		assertTrue( secuenciaNoRepetida(d) );
	
		d.next();
		assertFalse( d.noHayMasCombinaciones() );
        assertTrue( cantidadNodos(d) == nNodos);
		assertTrue( d.nodosEnElHueco(0) == 2 );
		assertTrue( d.nodosEnElHueco(1) == 2 );
		assertTrue( secuenciaNoRepetida(d) );

		d.next();
		assertFalse( d.noHayMasCombinaciones() );
        assertTrue( cantidadNodos(d) == nNodos);
		assertTrue( d.nodosEnElHueco(0) == 1 );
		assertTrue( d.nodosEnElHueco(1) == 3 );
		assertTrue( secuenciaNoRepetida(d) );

		d.next();
		assertFalse( d.noHayMasCombinaciones() );
        assertTrue( cantidadNodos(d) == nNodos);
		assertTrue( d.nodosEnElHueco(0) == 0 );
		assertTrue( d.nodosEnElHueco(1) == 4 );
		assertTrue( secuenciaNoRepetida(d) );
		
		d.next();
		assertTrue( d.noHayMasCombinaciones() );
		// Una vez que dio overflow ya no puedo aseverar
		// ninguna de las condiciones anteiorre, simplemente
		// se vuelve inusable y generar un nuevo objeto

	}
	
    /**
	 * @param d
	 * @return
     * @throws Exception 
	 */
	private boolean secuenciaNoRepetida(DesparramadorDeNodos d) throws Exception {
		LinkedList<Integer> s = new LinkedList<Integer>();
		for(int i=0; i < d.getHuecos(); i++)
			s.add(d.nodosEnElHueco(i));
		
		boolean yaExiste = generados.contains(s);
		generados.add(s);
		return ! yaExiste;
	}
	
	@Test
    public void testBasic()
    {
        int cantidadDeNodos = 4;
        int cantidadDeHuecos = 4;
        try {
            DesparramadorDeNodos despi = new DesparramadorDeNodos(cantidadDeHuecos,cantidadDeNodos);
            despi.next();
            assertTrue( cantidadNodos(despi) == cantidadDeNodos);
            despi.next();
            assertTrue( cantidadNodos(despi) == cantidadDeNodos);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private int cantidadNodos(DesparramadorDeNodos despi)
        throws Exception
    {
        int result = 0 ;
        
        System.out.println("#huecos : " + despi.getHuecos());

        for(int i=0; i < despi.getHuecos(); i++) {
        	result += despi.nodosEnElHueco(i);
    		System.out.println("  #nodos : " + result);
        }
        
        return result;
    }
    

	/***
	 * DE ACA PARA ABAJO SON TEST NUEVOS.
	 * PROBAR QUE FUNCIONE BIEN EN AMBOS CONTADORES, EL VIEJO Y EL NUEVO
	 * @throws Exception 
	 */
    
	
  
	
	@Test
	public void testCuatroHuecosDosNodos() throws Exception {
		int nodos = 2;
		int huecos = 4;
		int lastCantCombinaciones = 0;
		testGeneric( nodos, lastCantCombinaciones, huecos );
	}

	
	@Test
    public void testNnodosNhuecos() throws Exception
    {
		System.out.println("testNnodosTresHuecos()");
        for( int cantidadDeNodos = CANT_NODOS_MIN_DEL_TEST; cantidadDeNodos <= CANT_NODOS_MAX; cantidadDeNodos++ ) {
        	System.out.println("#nodos : " + cantidadDeNodos );
        	int lastCantCombinaciones = 0;

	        for( int cantidadDeHuecos = DesparramadorDeNodos.CANT_HUECOS_MIN; 
	        		 cantidadDeHuecos < DesparramadorDeNodos.CANT_HUECOS_MIN + 5; 
	        		 cantidadDeHuecos++ ) { 
	        				testGeneric(cantidadDeNodos, lastCantCombinaciones,
										cantidadDeHuecos);
	        }
        }
    }

	/**
	 * Prueba, en forma genérica, un desparramador de nodos
	 * @param cantidadDeNodos
	 * @param lastCantCombinaciones
	 * @param cantidadDeHuecos
	 * @throws Exception
	 */
	private void testGeneric(int cantidadDeNodos, int lastCantCombinaciones,
			int cantidadDeHuecos) throws Exception {
		DesparramadorDeNodos d = new DesparramadorDeNodos(cantidadDeHuecos,cantidadDeNodos);
		ValidadorCombinaciones validador = new ValidadorCombinaciones(d);
		int cantCombinaciones = 0;
		
		while( ! d.noHayMasCombinaciones() ) {
			assertTrue( secuenciaNoRepetida(d) );
		    assertTrue( cantidadNodos(d) == cantidadDeNodos);
		    assertTrue( d.getHuecos() == cantidadDeHuecos );
		    assertTrue( validador.combinacionValida() );
			d.next();

			cantCombinaciones++;
		}
		
		assertTrue( cantCombinaciones > lastCantCombinaciones );
		assertTrue( validador.terminoBien() );
		lastCantCombinaciones = cantCombinaciones;
	}
	
	private class ValidadorCombinaciones {
		
		private DesparramadorDeNodos d;
		private boolean registrado;
		private HashMap<String,LinkedList<LinkedList<Integer>>> registry;
		private int combinacionesFallidas;
		private String key;
		
		public ValidadorCombinaciones( DesparramadorDeNodos d ) throws Exception {
			setupRegistry();				
			this.d = d;
			registrado = desparramoRegistrado();
			
			if (registrado) {
				System.out.println("Clave regsitrada : " + getKey() );
				key = getKey();
			} else {
				key = null;
				d = null;
			}
			
			combinacionesFallidas = 0;
		}

		private void setupRegistry() {
			registry = new HashMap<String,LinkedList<LinkedList<Integer>>>();
			
			String key = "0200"; // dos huecos, sin nodos
			LinkedList<LinkedList<Integer>> validos = new LinkedList<LinkedList<Integer>>();
			LinkedList<Integer> nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);
			registry.put(key, validos);


			key = "0302";   // tres huecos, dos nodos
			validos = new LinkedList<LinkedList<Integer>>();
			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(2);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);
			
			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);
			
			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			validos.add(nodosEnElHueco);
			
			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(2);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);
			
			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(1);
			validos.add(nodosEnElHueco);
			
			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(2);
			validos.add(nodosEnElHueco);

			registry.put(key, validos);
			


			key = "0402";   // cuatro huecos, dos nodos
			validos = new LinkedList<LinkedList<Integer>>();
			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(2);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(2);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(2);
			nodosEnElHueco.add(0);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(1);
			nodosEnElHueco.add(1);
			validos.add(nodosEnElHueco);

			nodosEnElHueco = new LinkedList<Integer>();
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(0);
			nodosEnElHueco.add(2);
			validos.add(nodosEnElHueco);
			
			registry.put(key, validos);
			
		}

		private boolean desparramoRegistrado() throws Exception {
			return registry.containsKey( getKey() );
		}

		/**
		 * obtains key to enter into the registry
		 * @param d : desparramador de nodos
		 * @return clave para acceder a la registry
		 * @throws Exception 
		 */
		private String getKey() throws Exception {
			String key = String.format("%02d%02d", d.getHuecos(), cantidadNodos(d) );
			System.out.println("getKey(): #huecos : " + d.getHuecos()
							 + ", #nodos : " + cantidadNodos(d)
							 + ", clave : " + key);
			return key;
		}

		public boolean terminoBien() {
			if ( ! registrado )
				return true;

			System.out.println("registry : " + registry);
			System.out.println("key : " + key );
			System.out.println("list : " + registry.get(key));
			return (registry.get(key).size() == 0)
				&& (combinacionesFallidas == 0);
		}

		public boolean combinacionValida() {
			if ( ! registrado )
				return true;

			LinkedList<Integer> comb = generarCombinacion();
			boolean esValida = borrarCombinacion(comb);
			if (! esValida)
				combinacionesFallidas++;
			
			return esValida;
		}

		/**
		 * Borra la combinacion pasada de la registry
		 * @param comb : combinación a borrar
		 * @return Devuelve si la combnación existía o no en la registry
		 */
		private boolean borrarCombinacion(LinkedList<Integer> comb) {
			boolean esValida = registry.get(key).contains(comb);
			
			if (esValida )
				registry.get(key).remove(comb);
			return esValida;
		}

		/**
		 * Extrae la combinacion actual 
		 * @return
		 */
		private LinkedList<Integer> generarCombinacion() {
			LinkedList<Integer> comb = new LinkedList<Integer>();
			for(int i=0; i < d.getHuecos(); i++)
				try {
					comb.add(d.nodosEnElHueco(i));
				} catch (Exception e) {
					System.out.println("Si esta excepción se muestra hay un bug en DesaprramadorDeNodos");
					e.printStackTrace();
				}
			return comb;
		}
	}



}
