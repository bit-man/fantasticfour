/**
 * 
 */
package fantasticfour.algo3.tp3.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp3.Nodo;

/**
 * @author bit-man
 *
 */
public class NodoTest {

	/**
	 * 
	 */
	private static final int	RANDOM_99	= 99;
	Nodo n;
	
	@Before
	public void setUp() {
		n = new Nodo(RANDOM_99, false);
	}
	
	/**
	 * Test method for {@link fantasticfour.algo3.tp3.Nodo#Nodo(int, boolean)}.
	 */
	@Test
	public void testNodo() {
		Nodo n2 = new Nodo(RANDOM_99, false);
		assertTrue( this.n.equals(n2));
	}
	
	@Test
	public void testNodoB() {
		Nodo n2 = new Nodo(RANDOM_99);
		assertTrue( this.n.equals(n2));
	}

	/**
	 * Test method for {@link fantasticfour.algo3.tp3.Nodo#ponerRaiz(boolean)}.
	 */

	@Test
	public void testEqualsB() {
		Nodo nn = new Nodo(RANDOM_99);
		assertTrue( nn.equals(n));
		
		nn = new Nodo(RANDOM_99 + 1);
		assertFalse( nn.equals(n));
	}
	
	@Test
	public void testEquals() {
		Nodo nn = new Nodo(RANDOM_99, false);
		assertTrue( nn.equals(n));
		
		nn = new Nodo(RANDOM_99 + 1, false);
		assertFalse( nn.equals(n));
	}

}
