/**
 * 
 */
package fantasticfour.algo3.tp3;

/**
 * @author bit-man
 *
 */
public class MatrizAdyacenciaDiferencial extends MatrizAdyacencia {

	public MatrizAdyacenciaDiferencial( Grafo g ) {
		super(g);
	}
	
	public MatrizAdyacenciaDiferencial( Instancia i ) {
		super(i);
	}

	/***
	 * TODO En los siguientes métodos cambiar el nodo indicado de lugar y recalcular
	 * 		en forma diferencial la cant. de cruces
	 * @param nodo
	 */
	
	public void moverV1arriba(int nodo) {
		
	}
	
	public void moverV1abajo(int nodo) {
		
	}

	public void moverV2arriba(int nodo) {
		
	}
	
	public void moverV2abajo(int nodo) {
		
	}
	

}
	