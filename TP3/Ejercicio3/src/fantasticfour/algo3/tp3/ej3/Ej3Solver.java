package fantasticfour.algo3.tp3.ej3;

import fantasticfour.algo3.tp3.*;
import java.util.LinkedList;

public class Ej3Solver
{
    private Instancia instancia;
    private long cantOps;

    public Ej3Solver(Instancia instancia)
    {
        this.instancia = instancia;
        cantOps = 0;
    }

    protected void incrementarOperaciones(long cantidadDeOperaciones)
    {
        cantOps = cantOps + cantidadDeOperaciones;
    }

    public long cantidadOperaciones()
    {
        return cantOps;
    }

    public Grafo solve()
	{
		Grafo grafo = instancia.dameGrafoOriginal().clone();
        incrementarOperaciones(instancia.getCantidadDeOperaciones());
        Grafo grafoTotal = instancia.dameGrafo().clone();
        incrementarOperaciones(instancia.getCantidadDeOperaciones());
        incrementarOperaciones(2);

        LinkedList<Nodo> nodosIV1 = new LinkedList<Nodo>(instancia.dameNodosIV1());
        incrementarOperaciones(instancia.getCantidadDeOperaciones());
        LinkedList<Nodo> nodosIV2 = new LinkedList<Nodo>(instancia.dameNodosIV2());
        incrementarOperaciones(instancia.getCantidadDeOperaciones());
        incrementarOperaciones(nodosIV1.size() + nodosIV2.size());

        boolean esV1MasCorta = nodosIV1.size() < nodosIV2.size();
        incrementarOperaciones(4);
        int longitudMasCorta = (esV1MasCorta ? nodosIV1.size() : nodosIV2.size());
        incrementarOperaciones(4);

        int longIV1 = nodosIV1.size();
        int longIV2 = nodosIV2.size();
        incrementarOperaciones(4);

        int i;
        for (i = 0 ; i < longitudMasCorta; i++)
        {
            Nodo nodoIV1 = grafoTotal.dameNodoConMayorGradoDeV1RestringidoA(nodosIV1);
            incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
            grafoTotal.sacarNodoDeV1(nodoIV1);
            incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
            nodosIV1.remove(nodoIV1);
            incrementarOperaciones(nodosIV1.size());
            LinkedList<Eje> adyacentesHastaAhora = instancia.dameEjesConAdyacentes(grafo, nodoIV1, true);
            incrementarOperaciones(grafo.getCantidadDeOperaciones());
            incrementarOperaciones(instancia.getCantidadDeOperaciones());

            int minCruces = Integer.MAX_VALUE;
            int posMinCruce = -1;
            incrementarOperaciones(2);

            // Chequeo la mejor posicion de V1 para poner el nodo elegido.
            for (int j = 0 ; j < grafo.dameNodosV1().size(); j++) {
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
                int cruces = grafo.cantidadCruces(nodoIV1, adyacentesHastaAhora, j, true);
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
                incrementarOperaciones(2);
                if (minCruces > cruces) {
                    minCruces = cruces;
                    posMinCruce = j;
                    incrementarOperaciones(2);
                }
            }

            grafo.agregarNodoYEjesEnV1(nodoIV1, adyacentesHastaAhora, posMinCruce);
            incrementarOperaciones(grafo.getCantidadDeOperaciones());
            Nodo nodoIV2 = grafoTotal.dameNodoConMayorGradoDeV2RestringidoA(nodosIV2);
            incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
            grafoTotal.sacarNodoDeV2(nodoIV2);
            incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
            nodosIV2.remove(nodoIV2);
            incrementarOperaciones(nodosIV2.size());

            adyacentesHastaAhora = instancia.dameEjesConAdyacentes(grafo, nodoIV2, false);
            incrementarOperaciones(grafo.getCantidadDeOperaciones());
            incrementarOperaciones(instancia.getCantidadDeOperaciones());

            minCruces = Integer.MAX_VALUE;
            posMinCruce = -1;
            incrementarOperaciones(3);

            for (int j = 0 ; j < grafo.dameNodosV2().size(); j++) {
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
                int cruces = grafo.cantidadCruces(nodoIV2, adyacentesHastaAhora, j, false);
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
                incrementarOperaciones(2);
                if (minCruces > cruces) {
                    minCruces = cruces;
                    posMinCruce = j;
                    incrementarOperaciones(2);
                }
            }

            grafo.agregarNodoYEjesEnV2(nodoIV2, adyacentesHastaAhora, posMinCruce);
            incrementarOperaciones(grafo.getCantidadDeOperaciones());
        }

        /*
          Nodos restantes (de v1 o de v2, dependiendo de cual es más corta).
          */

        for (; i < (esV1MasCorta ? longIV2 : longIV1); i ++)  {

            Nodo nodo;
            if (!esV1MasCorta)
            {
                nodo = grafoTotal.dameNodoConMayorGradoDeV1RestringidoA(nodosIV1);
                incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
                incrementarOperaciones(1);
                grafoTotal.sacarNodoDeV1(nodo);
                incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
                nodosIV1.remove(nodo);
                incrementarOperaciones(nodosIV1.size());
            }
            else
            {
                nodo = grafoTotal.dameNodoConMayorGradoDeV2RestringidoA(nodosIV2);
                incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
                grafoTotal.sacarNodoDeV2(nodo);
                incrementarOperaciones(grafoTotal.getCantidadDeOperaciones());
                nodosIV2.remove(nodo);
                incrementarOperaciones(nodosIV2.size());
            }

            //Nodo nodo = esV1MasCorta ? nodosIV2.get(i) : nodosIV1.get(i);
            LinkedList<Eje> adyacentesHastaAhora = instancia.dameEjesConAdyacentes(grafo, nodo, !esV1MasCorta);
            incrementarOperaciones(grafo.getCantidadDeOperaciones());
            incrementarOperaciones(instancia.getCantidadDeOperaciones());

            int minCruces = Integer.MAX_VALUE;
            int posMinCruce = -1;
            incrementarOperaciones(2);
            
            for (int j = 0 ; j < (esV1MasCorta ? grafo.dameNodosV2().size() : grafo.dameNodosV1().size()); j++) {
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
                int cruces = grafo.cantidadCruces(nodo, adyacentesHastaAhora, j, !esV1MasCorta);
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
                incrementarOperaciones(2);
                if (minCruces > cruces) {
                    minCruces = cruces;
                    posMinCruce = j;
                    incrementarOperaciones(2);
                }
            }

            if (esV1MasCorta) {
                grafo.agregarNodoYEjesEnV2(nodo, adyacentesHastaAhora, posMinCruce);
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
            } else {
                grafo.agregarNodoYEjesEnV1(nodo, adyacentesHastaAhora, posMinCruce);
                incrementarOperaciones(grafo.getCantidadDeOperaciones());
            }

        }
        return grafo;
	}
}