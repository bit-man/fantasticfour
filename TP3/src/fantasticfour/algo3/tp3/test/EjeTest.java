/**
 * 
 */
package fantasticfour.algo3.tp3.test;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp3.Eje;
import fantasticfour.algo3.tp3.Nodo;

/**
 * @author bit-man
 *
 */
public class EjeTest {


	private static final int	VALOR_2	= 4;
	private static final int	VALOR_1	= 3;

	Nodo n1;
	Nodo n2;
	Eje e;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		n1 = new Nodo(VALOR_1, false);
		n2 = new Nodo(VALOR_2, false);
		e = new Eje(n1,n2);
	}
	

	@Test
	public void testEqualsEje() {
		Nodo nn1 = new Nodo(VALOR_1, false);
		Nodo nn2 = new Nodo(VALOR_2, false);
		Eje ee = new Eje(nn1,nn2);
		assertTrue( ee.equals(e) );

		nn2 = new Nodo(VALOR_2 + 1, false);
		ee = new Eje(nn1,nn2);
		assertFalse( ee.equals(e) );
	}
	
	@Test
	public void testDameNodos() {
		assertTrue( e.dameDesde().equals(n1) );
		assertTrue( e.dameHasta().equals(n2) );
	}
	
	@Test
	public void testConstructorDeEje() {
		Eje e2 = new Eje(e);
		e = e2;
		testDameNodos();
		testEqualsEje();
	}
}
