package fantasticfour.algo3.tp2.ej3;

public class Const {
	public static final int	CANT_COMANDOS	= 4;
	public static final String COMANDO_SACAR = "sacar";
	public static final String COMANDO_PERTENECE = "pertenece";
	public static final String COMANDO_CARDINAL = "cardinal";
	public static final String COMANDO_AGREGAR = "agregar";

	public static final String TIPO_UNICAS = "unicas";
	public static final String TIPO_COMPARTIDAS = "compartidas";
}
