/**
 * 
 */
package fantasticfour.algo3.tp1.ej2;

import java.io.FileNotFoundException;

/**
 * @author bit-man
 *
 */
public class GenerarPeorEntrada {

	private static final int ERROR = 1;
	private static int valor = 1;
	
	/**
	 * @param args() 
	 */
	public static void main(String[] args) {
		if ( args.length != 2 ) {
			System.out.println("ERROR: Deben pasarse la cantidad de casos y el archivo a generar\n");
			System.out.println("java fantasticfour.algo3.tp1.ej2.GenerarPeorEntrada <casos> <archivo>\n");
			System.exit( ERROR );
		};
		
		int n = Integer.parseInt(args[0]);
		String nombreAchivo = args[1];
		EscribirEntrada entrada = null;
		
		try {
			entrada = new EscribirEntrada(nombreAchivo);
		} catch (FileNotFoundException e) {
			System.err.println("No se encontr� el archivo " + nombreAchivo);
			System.exit(ERROR);
		};
		
		for(int items = 1; items <= n; items = proxItem() ) {
			entrada.items(items);
			entrada.grabar();
		};

		entrada.cerrar();
			
	}
	
	private static int proxItem() {		
		valor += 1;
		return valor;
	}

}
