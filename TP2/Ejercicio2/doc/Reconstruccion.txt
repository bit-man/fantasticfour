﻿ Idea de Reconstrucción del Camino sin usar la Matriz de Decisión:
--------------------------------------------------------------------------------------------------

* Si la posición (i, j) de las matrices K1 y K0 es true en ambas, se puede tomar lo que sigue para cualquiera de las dos.



* Si la fila (i,j) de la matriz K1 está en true, esto es porque necesariamente la segunda parte del if original 
(el de la función de programación dinámica) dio true.

Esto es: f(i, j-1, 0) y existe tratado (i, j) ó f(i, j-1, 1) y existe tratado (j-1, j)

Para que la posición (i, j) valga true, alguna de las dos f anteriores tuvo que evaluar en true.
Notar que en ambos casos se chequea la columna anterior ( j - 1 ) de cada matriz, por lo que hay que ver esa posición.

Pero: Si f(i, j-1, 0) es true, no implica directamente que exista (i, j)
	  Si f(i, j-1, 1) es true, no implica directamente que exista (j-1, j) 
	  Ya que puede existir el camino (f), pero no la conexión.
	  
Entonces, se pueden dar los siguientes casos: 
	  Si f(i, j-1, 0) es true y si f(i, j-1, 1) es false, entonces SI O SI, existe (i, j) y sigo por ahí.
	  Si f(i, j-1, 0) es false y si f(i, j-1, 1) es true, entonces SI O SI, existe (j-1, j) y sigo por ahí.
	  Si f(i, j-1, 0) es true y si f(i, j-1, 1) es true, entonces tengo que chequear cual es la relacion que existe (puede ser cualquiera de las dos).
	  Este es el caso más choto.
	  NO se puede dar el caso en que ambas sean false, por que no habría camino :)

Voy a ver en la columna anterior y repito el procedimiento.



* Si la fila (i,j) de la matriz K0 está en true, esto es por que necesariamente la primera parte del if original 
(el de la función de programación dinámica) dio true.

Esto es: f(i + 1, j, 0) y existe tratado (i, i+1) ó f(i + 1, j, 1) y existe tratado (i, j)

Para que la posición (i, j) valga true, alguna de las dos f anteriores tuvo que evaluar en true.
Notar que en ambos casos se chequea la fila posterior ( i + 1 ) por lo que hay que ver esa posición.

Pero: Si f(i + 1, j, 0) es true, no implica directamente que exista (i, i + 1)
	  Si f(i + 1, j, 1) es true, no implica directamente que exista (i, j) 
	  Ya que puede existir el camino (f), pero no la conexión.

Entonces, se pueden dar los siguientes casos: 
	  Si f(i + 1, j, 0) es true y si f(i + 1, j, 1) es false, entonces SI O SI, existe (i, i + 1) y sigo por ahí.
	  Si f(i + 1, j, 0) es false y si f(i + 1, j, 1) es true, entonces SI O SI, existe (j-1, j) y sigo por ahí.
	  Si f(i + 1, j, 0) es true y si f(i + 1, j, 1) es true, entonces tengo que chequear cual es la relacion que existe 
	  (puede ser cualquiera de las dos).
	  Este es el caso más choto.
	  NO se puede dar el caso en que ambas sean false, por que no habría camino :)


Voy a ver en la fila posterior y repito el procedimiento.


En ambos casos notar que se selecciona cual componente pertenece al camino:
Si estoy en K1, será la segunda componente.
Si estoy en K0, será la primera componente.


Todo esto se hace hasta llegar a la diagonal (o generar el camino de longitud n - 1) . Ahí se corta.

------------------------------------------------------------------------------------------------------------------------------------------------------------

Parece que la cosa es así:

Cuando estoy en una matriz y me fijo como dije antes en las posiciones anteriores, si en ambas es true, 
estoy en problemas, ya que no puedo decidir que puente tenía.
Ahí tengo que chequear con las relaciones.
Si alguna de las dos es false, listo, voy por la que es true, por que se me garantiza que el puente a ese camino existe.
:D
