package fantasticfour.algo3.tp2.ej2;

import java.io.IOException;

public class Ej2Solver 
{
	private Ej2Input entrada;
	private Ej2Output salida;
	private Matriz k0;
	private Matriz k1;
	private Pueblos pueblos;
	
	private int cantOperaciones;
	
	public Ej2Solver(Ej2Input entrada, Ej2Output salida) 
	{
		this.entrada = entrada;
		this.salida = salida;
		this.cantOperaciones = 0;
	}
	
	public void solve() throws IOException	
	{
		while ((pueblos = entrada.dameProximaInstancia()) != null) {
			cantOperaciones = 0;
			boolean encontreCamino = false;
			
			for (int i = 0; i < pueblos.obtenerCantPueblos(); i++) {
				/* Prueba si existe un camino entre i y el pueblo anterior. */
				if (existeCaminoEntre(i, pueblos.puebloAnterior(i))) {
//					if (k0.obtenerValor(i, pueblos.puebloAnterior(i))) {
//						System.out.println("Termino en " + i);
//					} else {
//						System.out.println("Termino en " + pueblos.puebloAnterior(i));
//					}
//					System.out.println("K0\n" + k0.toString());
//					System.out.println("K1\n" + k1.toString());

					int[] camino = recuperarCamino(i, pueblos.puebloAnterior(i));
					encontreCamino = true;
						
					cantOperaciones++;
					
					salida.grabarResultado(true, camino);
					
//					for (int j = 0; j < camino.length; j++) {
//						System.out.print((camino[j] +1) + ", ");
//					}

					break;
				}
			}
			
			if (!encontreCamino) {
				salida.grabarResultado(false, null);
			}
			
			salida.grabarCantOperaciones(pueblos.obtenerCantPueblos()+pueblos.obtenerCantTratados(), cantOperaciones);
		}
	
		salida.cerrar();
		entrada.cerrar();
	}
	
	private int[] recuperarCamino(int i, int j) 
	{
		int[] camino = new int[pueblos.obtenerCantPueblos()];
		int pueblo;
		int cantPueblosVisitados = 1;
		
		cantOperaciones = cantOperaciones+5;
		// Evaluacion de la guarda
		// cantidad de operaciones en obtener y agregar en una matriz es 5
		
		if (k0.obtenerValor(i, j)) {
			// El camino termina en i.
			camino[0] = i;
			i = pueblos.puebloSiguiente(i);
			
			cantOperaciones = cantOperaciones+2;
			
		} else {
			// El camino termina en j.
			camino[0] = j;
			j = pueblos.puebloAnterior(j);
			
			cantOperaciones = cantOperaciones+2;
			
		}
		
		cantOperaciones = cantOperaciones+2;
		//Evaluacion de la guarda 
		
		while (cantPueblosVisitados < camino.length) {
			// guarda del if
			cantOperaciones = cantOperaciones+3;
			
			if (k0.obtenerValor(i, j) && k1.obtenerValor(i, j)) {
				// El camino puede terminar en cualquiera de los dos.
				
				//Evaluacion guarda del if
				cantOperaciones = cantOperaciones+3;
				
				if (pueblos.hayTratadoEntre(camino[cantPueblosVisitados - 1], i)) {
					pueblo = i;
					i = pueblos.puebloSiguiente(i); // Voy para i + 1.
					 
					// puebloSiguiente y puebloAntererior 4 opereciones cada una.
					// =, = y 4 de puebloSiguiente.
					cantOperaciones = cantOperaciones+6;
					
				} else {
					pueblo = j;
					j = pueblos.puebloAnterior(j); // Voy para j - 1.;
					
					cantOperaciones = cantOperaciones+6;
					
				}
				
				
			} else if (k0.obtenerValor(i, j)) {
				// El camino termina en i.
				
				pueblo = i;
				i = pueblos.puebloSiguiente(i); // Voy para i + 1.
				
				cantOperaciones = cantOperaciones+6;
				
			} else {
				// El camino termina en j.
				
				pueblo = j;
				j = pueblos.puebloAnterior(j); // Voy para j - 1.;
				
				cantOperaciones = cantOperaciones+6;
				
			}
			
			camino[cantPueblosVisitados] = pueblo;
			cantPueblosVisitados++;
			
			cantOperaciones = cantOperaciones+2;
			
			//Evaluacion guarda del while
			cantOperaciones = cantOperaciones+2;
			
		}
		
		return camino;
	}

	private boolean existeCaminoEntre(int i, int j) 
	{
		// Debo devolver el valor de la matriz (i, j).
		// Antes debo llenar las matrices de recorridos (k0 y k1)
		llenarMatrizPara(i, j);
		
		cantOperaciones = cantOperaciones+11;
		
		// 5 de cada obtenerValor y 1 del ||.
		
		return k0.obtenerValor(i, j) || k1.obtenerValor(i, j);
	}

	private void llenarMatrizPara(int inicio, int destino)  
	{
		int dimension = pueblos.obtenerCantPueblos();
		cantOperaciones = cantOperaciones+1;
		
		k0 = new Matriz(dimension);
		k1 = new Matriz(dimension);

		// Long 0:
		// Lleno casos base (en la diagonal). i = j => True.
		
		cantOperaciones = cantOperaciones+2;
		
		for(int i = 0; i < dimension; i++) {
			
			cantOperaciones = cantOperaciones+12;
			
			// 10 por los 2 agregar, 1 por ++ y otro por <
			
			k0.agregar(i, i);
			k1.agregar(i, i);
		}
		
		// Long 1: Tratados entre vecinos: Caso Base.
		// Llenar el resto de las matrices de a diagonales.
		int longitudCamino = 1;
		cantOperaciones = cantOperaciones+1;
		
	    //System.out.println("Distancia " + longitudCamino);
		int origen = pueblos.puebloAnteriorADistancia(inicio, 2);
		cantOperaciones = cantOperaciones+6;
		//pruebloAnteriorADistancia evaluado en 2 es 2*3 ya que 3 es la cantidad de operaciones de ejecutar solo una vez el while de pruebloAnteriorADistancia
		
		cantOperaciones = cantOperaciones + 3;
		
		for (int i = 0; i < dimension - longitudCamino; i++) {
			
			cantOperaciones = cantOperaciones+9;
			
			int puebloSiguiente = pueblos.puebloSiguiente(origen);
			
			// Evaluacion del if
			
			cantOperaciones = cantOperaciones+1;
			
			if (pueblos.hayTratadoEntre(origen, puebloSiguiente)) {
				
				cantOperaciones = cantOperaciones+11;
				
				k0.agregar(origen, puebloSiguiente);
				k1.agregar(origen, puebloSiguiente);
			}
			//System.out.println("Agrego dato de : [" + origen + ", " + puebloSiguiente + "]");
			
			cantOperaciones = cantOperaciones+6;
			
			origen = pueblos.puebloAnterior(origen);
		}

		//System.out.println();
		
		// Long 2 a n - 1
		// OJO! Tal vez no haya tantos pueblos en el lago!
		
		cantOperaciones = cantOperaciones+2;
		
		for (longitudCamino = 2; longitudCamino < dimension; longitudCamino++) {
			
			cantOperaciones = cantOperaciones+3;
			
			origen = pueblos.puebloAnteriorADistancia(destino, longitudCamino);
			
			// Si la ejecucion del whiles de puebloADistancia es de 3 operaciones y necesito uno camino de longitud longitudCamino
			// entonces cantidad de operaciones es 3*longitudCamino
			
			cantOperaciones = cantOperaciones+longitudCamino*3;
			
			
			int dest = destino;
			//System.out.println("Distancia " + longitudCamino);
			
			cantOperaciones = cantOperaciones+3;
			
			for (int i = 0; i < dimension - longitudCamino; i++) {
				
				cantOperaciones = cantOperaciones+7;
				
				boolean valueK0 = existeCaminoK0Entre(origen, dest);
				boolean valueK1 = existeCaminoK1Entre(origen, dest);
				
				cantOperaciones = cantOperaciones+10;
				
				k0.agregar(origen, dest, valueK0);
				k1.agregar(origen, dest, valueK1);

				//System.out.println("Agrego dato de : [" + origen + ", " + dest + "]");

				origen = pueblos.puebloAnterior(origen);
				dest = pueblos.puebloAnterior(dest);
				
				cantOperaciones = cantOperaciones+12;
				
			}
			//System.out.println();

		}
		
	}

	private boolean existeCaminoK0Entre(int origen, int dest) 
	{
		int puebloSiguiente = pueblos.puebloSiguiente(origen);
		
		cantOperaciones = cantOperaciones+21;
		
		return (k0.obtenerValor(puebloSiguiente, dest) && pueblos.hayTratadoEntre(origen, puebloSiguiente)) ||
					(k1.obtenerValor(puebloSiguiente, dest) && pueblos.hayTratadoEntre(origen, dest));
	}

	private boolean existeCaminoK1Entre(int origen, int dest) 
	{
		int puebloAnterior = pueblos.puebloAnterior(dest);
		
		cantOperaciones = cantOperaciones+21;
		
		return (k0.obtenerValor(origen, puebloAnterior) && pueblos.hayTratadoEntre(origen, dest)) ||
					(k1.obtenerValor(origen, puebloAnterior) && pueblos.hayTratadoEntre(puebloAnterior, dest));	
	}

}