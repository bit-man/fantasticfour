package fantasticfour.algo3.tp3.ej5;

import fantasticfour.algo3.tp3.Instancia;
import fantasticfour.algo3.tp3.Grafo;
import fantasticfour.algo3.tp3.ej4.Ej4Solver;

public class Ej5Solver
{
    private Instancia instancia;
    private long cantOps;
    /**
     * Generar un grafo con el generador de grafos.
     * Correrle goloso ramdomizado. Ejercicio4 con el true
     * Correrle busquedaLocal
     * 
	 */
	
    public Ej5Solver(Instancia instancia)
    {
        this.instancia = instancia;
        cantOps = 0;
    }

    protected void incrementarOperaciones(long cantidadDeOperaciones)
    {
        cantOps = cantOps + cantidadDeOperaciones;
    }

    public long cantidadOperaciones()
    {
        return cantOps;
    }

    public Grafo solve()
    {
        Grafo mejorGrafo = null;
        int mejorCruces = Integer.MAX_VALUE;

        int ciclos = 30;
        int ciclosSinMejora = 15;
        int hayCorte = 0;
        int iteracionActual = 0;
        incrementarOperaciones(8);

        incrementarOperaciones(3);
        Ej4Solver ej4Solver;
        Grafo grafo;
        
        while (hayCorte <= ciclosSinMejora && iteracionActual < ciclos)
        {
            ej4Solver = new Ej4Solver(instancia);
            grafo = ej4Solver.solve(true);
            incrementarOperaciones(ej4Solver.getCantidadDeOperaciones());
            int cantidadCruces = grafo.getCalcCantCruces();
            incrementarOperaciones(grafo.getCantidadDeOperaciones());
            incrementarOperaciones(1);
            if (mejorCruces > cantidadCruces)
            {
                mejorGrafo = grafo;
                mejorCruces = cantidadCruces;
                hayCorte = 0;
                incrementarOperaciones(3);
            }
            else {
                hayCorte++;
                incrementarOperaciones(1);
            }
            iteracionActual++;
            incrementarOperaciones(1);
        }
        return mejorGrafo == null ? instancia.dameGrafo() : mejorGrafo;
    }
}