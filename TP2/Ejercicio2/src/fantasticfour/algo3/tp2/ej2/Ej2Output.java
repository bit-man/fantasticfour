package fantasticfour.algo3.tp2.ej2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Ej2Output 
{	
	private FileWriter archivoSalida;
	private FileWriter statisticsFile;
	private String nombreArchivoSalida;	

	public Ej2Output(File archivoSalida) 
		throws IOException 
	{
		this.nombreArchivoSalida = archivoSalida.getAbsolutePath();
		this.archivoSalida = new FileWriter(archivoSalida);
		this.statisticsFile = new FileWriter(new File(nombreArchivoSalida + ".op"));
	}
	
	public void cerrar(){
		
		try {
			if (archivoSalida != null) {
				archivoSalida.close();
			}
		} catch (IOException e) {
			printCloseError(nombreArchivoSalida);
		}
		
		try {
			if (statisticsFile != null) {
				statisticsFile.close();
			}
		} catch (IOException e2) {
			printCloseError(nombreArchivoSalida + ".op");
			}
		
	}
		
		
		private void printCloseError(String nombreArchivoSalida) 
		{
			System.out.println("No se puede cerrar el archivo " + nombreArchivoSalida);
		}

		public void grabarResultado(boolean existeCamino , int[] resultados) 
			throws IOException 
		{
			if (existeCamino) {
				int i = 0;
				archivoSalida.write("" + (resultados[0] + 1));
				for (i = 1; i < resultados.length; i++) {
					archivoSalida.write(" " + (resultados[i] + 1));
				}
				archivoSalida.write("\n");
			} else {
				archivoSalida.write( "-1" + "\n");
			}
		}
		
		public void grabarCantOperaciones(int tamanio, int valor) 
			throws IOException 
		{
			statisticsFile.write(tamanio + "   " + valor + "   " + "\n");
		}
}


