package fantasticfour.algo3.tp3;

import java.util.HashMap;
import java.util.LinkedList;


public class Grafo
{
	private ListaAdyacencia listaAdyacencia;
    private LinkedList<Nodo> nodosV1;
    private LinkedList<Nodo> nodosV2;
    private LinkedList<Eje> ejes;
	private int calcCantCruces;
    private long cantidadOperaciones = 0;

    public Grafo(LinkedList<Nodo> nodosV1, LinkedList<Nodo> nodosV2, LinkedList<Eje> ejes)
    {
        this.nodosV1 = new LinkedList<Nodo>(nodosV1);
        this.nodosV2 = new LinkedList<Nodo>(nodosV2);
        this.ejes = new LinkedList<Eje>(ejes);
        this.listaAdyacencia = new ListaAdyacencia(this);
        cantidadOperaciones = nodosV1.size() + nodosV2.size() + ejes.size();
    }

    public LinkedList<Nodo> dameNodosV1()
    {
        return this.nodosV1;
    }

    public LinkedList<Nodo> dameNodosV2()
    {
        return this.nodosV2;
    }

    public void setNodosV1(LinkedList<Nodo> nodosV1)
    {
        this.nodosV1 = nodosV1;
    }

    public void setNodosV2(LinkedList<Nodo> nodosV2)
    {
        this.nodosV2 = nodosV2;
    }

    public LinkedList<Eje> dameEjes()
    {
        return this.ejes;
    }

    public LinkedList<Nodo> dameNodos()
    {
        LinkedList<Nodo> nodos = new LinkedList<Nodo>(dameNodosV1());
        cantidadOperaciones = cantidadOperaciones + nodosV1.size();
        nodos.addAll(dameNodosV2());
        cantidadOperaciones = cantidadOperaciones + nodosV2.size();
        return nodos;
    }

    public void agregarNodoYEjesEnV1(Nodo nodo, LinkedList<Eje> ejes, int pos)
    {
        dameNodosV1().add(pos, nodo);
        cantidadOperaciones += pos;
        for (Eje eje : ejes) {
            listaAdyacencia.agregar(nodo, eje.dameHasta());
            cantidadOperaciones = cantidadOperaciones + listaAdyacencia.getCantidadDeOperaciones();
        }
        this.ejes.addAll(ejes);
        cantidadOperaciones += ejes.size();
    }

    public void agregarNodoYEjesEnV2(Nodo nodo, LinkedList<Eje> ejes, int pos)
    {
        dameNodosV2().add(pos, nodo);
        cantidadOperaciones = cantidadOperaciones + 1;
        for (Eje eje : ejes) {
            listaAdyacencia.agregar(eje.dameDesde(), nodo);
            cantidadOperaciones = cantidadOperaciones + listaAdyacencia.getCantidadDeOperaciones();
        }
        this.ejes.addAll(ejes);
        cantidadOperaciones = cantidadOperaciones + ejes.size();
    }

    public int cantidadCruces()
    {
        int result = 0;
        HashMap<Integer, Integer> posiciones = damePosicionesActuales();
        
        LinkedList<Eje> ejes = new LinkedList<Eje>(this.ejes);
        cantidadOperaciones = cantidadOperaciones + ejes.size();
        int size = ejes.size();

        cantidadOperaciones = cantidadOperaciones + 1;
        for (int i = 0; i < size ; i++)
        {
            Eje eje1 = ejes.poll();
            cantidadOperaciones +=  2; // get y -
            int posicionNodo1Desde = posiciones.get(eje1.dameDesde().nodo - 1);
            cantidadOperaciones +=  2; // get y -
            int posicionNodo1Hasta = posiciones.get(eje1.dameHasta().nodo - 1);
            cantidadOperaciones = cantidadOperaciones + 1;
            for (Eje eje2 : ejes)
            {
                cantidadOperaciones +=  2; // get y -
                int posicionNodo2Desde = posiciones.get(eje2.dameDesde().nodo - 1);
                cantidadOperaciones +=  2; // get y -
                int posicionNodo2Hasta = posiciones.get(eje2.dameHasta().nodo - 1);


                cantidadOperaciones +=  7; // 7 comparaciones !!
                if ((posicionNodo1Desde < posicionNodo2Desde && posicionNodo1Hasta > posicionNodo2Hasta) ||
                    (posicionNodo1Desde > posicionNodo2Desde && posicionNodo1Hasta < posicionNodo2Hasta)) {
                    result++;
                    cantidadOperaciones += 1;
                }
            }
            cantidadOperaciones += 2; // i < size e i++
        }
        return result;
    }

	private HashMap<Integer, Integer> damePosicionesActuales() 
	{
		HashMap<Integer, Integer> posiciones = new HashMap<Integer, Integer>(this.dameNodos().size());
		cantidadOperaciones += this.dameNodos().size(); // genera un hash de n entradas

		cantidadOperaciones += 2; // i < .. y size
		for (int i = 0; i < this.dameNodosV1().size(); i++) {
			cantidadOperaciones += i;
        	Nodo nodoV1 = this.dameNodosV1().get(i);
            cantidadOperaciones = cantidadOperaciones + 2;
        	posiciones.put(nodoV1.nodo - 1, i);

    		cantidadOperaciones += 3; // i < ... , i++ y size
        }


		cantidadOperaciones += 2;
        for (int i = 0; i < this.dameNodosV2().size(); i++) {
			cantidadOperaciones += i;
        	Nodo nodoV2 = this.dameNodosV2().get(i);
        	posiciones.put(nodoV2.nodo - 1, i);
            cantidadOperaciones = cantidadOperaciones + 2;

    		cantidadOperaciones += 3; // i < ... e i++ y size()
        }

        return posiciones;
	}

    public int cantidadCruces(Nodo nodo, LinkedList<Eje> adyacentesHastaAhora, int pos, boolean estaEnV1)
    {
        Grafo g = new Grafo(this.dameNodosV1(), this.dameNodosV2(), ejes);
        if (estaEnV1) {
        	g.dameNodosV1().add(pos, nodo);
            cantidadOperaciones = cantidadOperaciones + 1;
        } else {
        	g.dameNodosV2().add(pos, nodo);
            cantidadOperaciones = cantidadOperaciones + 1;
        }
        g.dameEjes().addAll(adyacentesHastaAhora);
        cantidadOperaciones = cantidadOperaciones + adyacentesHastaAhora.size();

        return g.cantidadCruces();
    }
    
    public void subir(Nodo n, boolean estaEnV1) 
	{
		LinkedList<Nodo> listaNodos = estaEnV1 ? this.nodosV1 : this.nodosV2;
        cantidadOperaciones = cantidadOperaciones + 1;

        cantidadOperaciones = cantidadOperaciones + listaNodos.size();
        if (listaNodos.indexOf(n)==0)
		{
        	cantidadOperaciones += listaNodos.size() + 1;
			int ultPos = listaNodos.size()-1;
            Nodo nodo = listaNodos.get(ultPos);

        	cantidadOperaciones += listaNodos.size();
            listaNodos.set(ultPos, n);

        	cantidadOperaciones += 1;
            listaNodos.set(0, nodo);
        }
		else
		{
        	cantidadOperaciones +=  listaNodos.size() + 1;
			int pos = listaNodos.indexOf(n);
        	cantidadOperaciones +=  pos - 1;
            Nodo nodo = listaNodos.get(pos - 1);

        	cantidadOperaciones +=  2 * pos - 1;
            listaNodos.set(pos, nodo);
            listaNodos.set(pos - 1, n);
        }
		
	}
	
	public void subirEnV1(Nodo n)
	{
		cantidadOperaciones += nodosV1.size();
		assert this.nodosV1.contains(n);
		this.subir(n, true);
	}

	public void subirEnV2(Nodo n)
	{
		cantidadOperaciones += nodosV2.size();
		assert this.nodosV2.contains(n);
		this.subir(n, false);
	}
	
	/**
	 * @return La posición en que está el nodo n dentro del digrafo 
	 */
	public int getPosicion(Nodo n)
    {
        int index;
        cantidadOperaciones = cantidadOperaciones + nodosV1.size();
        if (this.nodosV1.contains(n))
        {
			index = this.nodosV1.indexOf(n);
            cantidadOperaciones = cantidadOperaciones + nodosV1.size();
        }
        else
        {
			index = this.nodosV2.indexOf(n);
            cantidadOperaciones = cantidadOperaciones + nodosV2.size();
        }
        return index;
    }

    public LinkedList<Nodo> dameAdyacentesA(Nodo nodo)
    {
        LinkedList<Nodo> adyacentes = this.listaAdyacencia.getEjesDe(nodo);
        cantidadOperaciones += listaAdyacencia.getCantidadDeOperaciones();
        return adyacentes;
    }

    public Grafo clone()
    {
        return new Grafo(this.nodosV1, this.nodosV2, this.ejes);
    }

    public String toString()
    {
       StringBuffer str = new StringBuffer("{V1 = [");
       for (Nodo nodoV1 : this.dameNodosV1()) {
           str.append(nodoV1.esOriginal() ? nodoV1 + "x" : nodoV1).append(", ");
       }
       str.append("], V2 = [");
       for (Nodo nodoV2 : this.dameNodosV2()) {
           str.append(nodoV2.esOriginal() ? nodoV2 + "x" : nodoV2).append(", ");
       }
       str.append("]}");

       return str.toString();
    }

	public void bajar(Nodo n, boolean estaEnV1) 
	{
		LinkedList<Nodo> listaNodos = estaEnV1 ? this.nodosV1 : this.nodosV2;
        cantidadOperaciones = cantidadOperaciones + 1;

        int i = listaNodos.size()-1;
        cantidadOperaciones = cantidadOperaciones + 1;

        cantidadOperaciones = cantidadOperaciones + listaNodos.size();
        if (listaNodos.indexOf(n)==i)
		{
        	cantidadOperaciones += 2;
			Nodo nodo = listaNodos.get(0);
            listaNodos.set(0, n);
            cantidadOperaciones += 1;
			listaNodos.set(i, nodo);
        }
		else
		{
	        cantidadOperaciones += listaNodos.size();
			int pos = listaNodos.indexOf(n);
	        cantidadOperaciones += pos + 1;
			Nodo nodo = listaNodos.get(pos + 1);
	        cantidadOperaciones += pos + 1;
			listaNodos.set(pos+1, n);
	        cantidadOperaciones += pos;
			listaNodos.set(pos , nodo);
        }
		
	}
	
	public void bajarEnV1(Nodo nodo) 
	{	
        cantidadOperaciones += nodosV1.size();
		assert this.nodosV1.contains(nodo);
		this.bajar(nodo, true);	
	}
	
	public void bajarEnV2(Nodo nodo) 
	{	
        cantidadOperaciones += nodosV2.size();
		assert this.nodosV2.contains(nodo);
		this.bajar(nodo, false);	
	}
	
	public void cambiarPosiciones(Grafo respuestaPivote) 
	{
		this.setPosicionesNodosV1(respuestaPivote.dameNodosV1());
		this.setPosicionesNodosV2(respuestaPivote.dameNodosV2());
	}

	public boolean sePuedeBajar(Nodo nodoActual, boolean estaEnV1) 
	{
		boolean result = false;
        cantidadOperaciones = cantidadOperaciones + 3;
        LinkedList<Nodo> listaNodos = estaEnV1 ? this.nodosV1 : this.nodosV2;
		
        int pos = this.getPosicion(nodoActual);

        cantidadOperaciones += 1;
		if (pos == -1) {
			System.err.println("Error! Nodo " + nodoActual + " dice no estar!!!");
			System.exit(1);
		}

        cantidadOperaciones = cantidadOperaciones + 3;
        if (pos == listaNodos.size() - 1) {
			return false; // Ya no lo puedo bajar más. (Estoy en la tumba!)
		}

		Nodo nodoSiguiente = listaNodos.get(pos + 1);
        cantidadOperaciones = cantidadOperaciones + pos + 2;

        cantidadOperaciones += 1;
        if (!nodoActual.esOriginal()) {
			result = true;
		} else {
	        cantidadOperaciones += 1;
			if(!nodoSiguiente.esOriginal()) {
				result = true;
			}
		}
        cantidadOperaciones = cantidadOperaciones + 2;
		return result;
	}
	
	public boolean sePuedeBajarEnV1(Nodo nodo) 
	{	
		return this.sePuedeBajar(nodo, true);
	}
	
	public boolean sePuedeBajarEnV2(Nodo nodo) 
	{
		return this.sePuedeBajar(nodo, false);
	}
	
	public boolean sePuedeSubir(Nodo nodoActual, boolean estaEnV1) 
	{
		boolean result = false;
        cantidadOperaciones += 1;
		LinkedList<Nodo> listaNodos = estaEnV1 ? this.nodosV1 : this.nodosV2;

        int pos = this.getPosicion(nodoActual);

        cantidadOperaciones += 1;
		if (pos == -1) {
			System.err.println("Error! Nodo " + nodoActual + " dice no estar!!!");
			System.exit(1);
		}

        cantidadOperaciones += 1;
		if (pos == 0) {
			return false; // Ya no lo puedo subir más. (Estoy en la cima!)
		}
		

        cantidadOperaciones += pos - 1;
		Nodo nodoAnterior = listaNodos.get(pos - 1);
		
		cantidadOperaciones += 1;
		if (!nodoActual.esOriginal()) {
			result = true;
		} else {
			cantidadOperaciones += 1;
			if(!nodoAnterior.esOriginal()) {
				result = true;
			}
		}
		return result;
	}
	
	public boolean sePuedeSubirEnV1(Nodo nodo) 
	{	
		assert this.nodosV1.contains(nodo);
		return this.sePuedeSubir(nodo, true);	
	}
	
	public boolean sePuedeSubirEnV2(Nodo nodo) 
	{
		assert this.nodosV2.contains(nodo);
		return this.sePuedeSubir(nodo, false);
	}

	public void setPosicionesNodosV1(LinkedList<Nodo> nodosV1) 
	{
		cantidadOperaciones += 1;
		for (int i = 0; i < nodosV1.size(); i++) {
			cantidadOperaciones += 2 * i;
			this.nodosV1.set(i, nodosV1.get(i));
			cantidadOperaciones += 2;
		}
    }
	
	public void setPosicionesNodosV2(LinkedList<Nodo> nodosV2) 
	{
		cantidadOperaciones += 1;
		for (int i = 0; i < nodosV2.size(); i++) {
			cantidadOperaciones += 2 * i;
			this.nodosV2.set(i, nodosV2.get(i));
			cantidadOperaciones += 2;
		}
    }

    public Nodo dameNodoConMayorGradoDeV1RestringidoA(LinkedList<Nodo> nodosIV1) 
	{
		Nodo nodo = nodosIV1.getFirst();
		int mayorCantGrado = 0;
        cantidadOperaciones = cantidadOperaciones + 1;

        for (Nodo nodoActual : nodosIV1)
        {
            int gradoNodoActual = this.dameAdyacentesA(nodoActual).size();
            cantidadOperaciones += 1;

            cantidadOperaciones = cantidadOperaciones + 1;
            if (mayorCantGrado < gradoNodoActual)
            {
                nodo = nodoActual;
                mayorCantGrado = gradoNodoActual;
            }
		}

        return nodo;
	}

    public Nodo dameNodoConMayorGradoDeV2RestringidoA(LinkedList<Nodo> nodosIV2)
	{
		Nodo nodo = nodosIV2.getFirst();
		int mayorCantGrado = 0;
        cantidadOperaciones = cantidadOperaciones + 1;
        
        for (Nodo nodoActual : nodosIV2)
        {
            int gradoNodoActual = this.dameAdyacentesA(nodoActual).size();
            cantidadOperaciones = cantidadOperaciones + 1;

            cantidadOperaciones = cantidadOperaciones + 1;
            if (mayorCantGrado < gradoNodoActual)
            {
                nodo = nodoActual;
                mayorCantGrado = gradoNodoActual;
            }
		}

        return nodo;
	}

    public void sacarNodoDeV1(Nodo nodo)
    {
        cantidadOperaciones += nodosV1.indexOf(nodo);
        this.nodosV1.remove(nodo);

        cantidadOperaciones += 1;
        for (int i = 0; i < this.ejes.size(); i++) {
        	cantidadOperaciones += i;
            Eje eje = ejes.get(i);
            Nodo nodoDesde = eje.dameDesde();

            if (nodo.equals(nodoDesde))
            {
                cantidadOperaciones += ejes.indexOf(eje);
                this.ejes.remove(eje);
            }
            

            cantidadOperaciones += 2;
        }
    }

    public void sacarNodoDeV2(Nodo nodo)
    {
    	cantidadOperaciones += nodosV2.indexOf(nodo);
        this.nodosV2.remove(nodo);

        cantidadOperaciones += 1;
        for (int i = 0; i < this.ejes.size(); i++) {
        	cantidadOperaciones += i;
            Eje eje = ejes.get(i);
            Nodo nodoHasta = eje.dameHasta();

            if (nodo.equals(nodoHasta))
            {
            	ejes.indexOf(eje);
                this.ejes.remove(eje);
            }
            cantidadOperaciones += 2;
        }
    }


    /*
	 * O(n^4)
	 */
	public Nodo dameNodoConMayorCantCrucesDeV1(boolean[] usados)
	{
		Nodo nodo = null;
		int mayorCantCruces = 0;

        for (Nodo nodoActual : this.dameNodosV1()) {
            cantidadOperaciones = cantidadOperaciones + 3;
            if (!usados[nodoActual.nodo - 1]) {
				int crucesNodoActual = this.cantCrucesDeNodo(nodoActual);

                cantidadOperaciones = cantidadOperaciones + 3;
                if (mayorCantCruces < crucesNodoActual && (this.sePuedeBajarEnV1(nodoActual) || this.sePuedeSubirEnV1(nodoActual))) {
					nodo = nodoActual;
					mayorCantCruces = crucesNodoActual;
                }
			}
		}
        cantidadOperaciones = cantidadOperaciones + 1;
        if (nodo != null) {
            cantidadOperaciones = cantidadOperaciones + 2;
			usados[nodo.nodo - 1] = true;
        }
        
        
        return nodo;
	}

	private int cantCrucesDeNodo(Nodo nodoActual) 
	{
        int cantCrucesDeNodo = 0;
        HashMap<Integer, Integer> posiciones = damePosicionesActuales();
        
        LinkedList<Nodo> nodosAdyacentes = this.dameAdyacentesA(nodoActual);
        
        cantidadOperaciones += 2;
        int posicionNodo1Desde = posiciones.get(nodoActual.nodo -1);

        for (Nodo nodoAdy : nodosAdyacentes) {
        	int posicionNodo1Hasta = dameNodosV2().indexOf(nodoAdy);
            cantidadOperaciones = cantidadOperaciones + dameNodosV2().indexOf(nodoAdy);
            
            for (Eje eje : this.dameEjes()) {
                int posicionNodo2Desde = posiciones.get(eje.dameDesde().nodo - 1);
                int posicionNodo2Hasta = posiciones.get(eje.dameHasta().nodo - 1);

                cantidadOperaciones = cantidadOperaciones + 4;

                cantidadOperaciones = cantidadOperaciones + 7;
                if ((posicionNodo1Desde < posicionNodo2Desde && posicionNodo1Hasta > posicionNodo2Hasta) ||
                    (posicionNodo1Desde > posicionNodo2Desde && posicionNodo1Hasta < posicionNodo2Hasta)) {
                    cantidadOperaciones = cantidadOperaciones + 1;
                    cantCrucesDeNodo++;
                }
           }
        }

        return cantCrucesDeNodo;
	}
	
	public void setCantidadDeCrucesCalculada(int mejorCantCruces) 
	{
		this.calcCantCruces = mejorCantCruces;
    }

	public int getCalcCantCruces() 
	{
		return calcCantCruces;
	}

    public int damePosEnV2De(Nodo nodo)
    {
        cantidadOperaciones = cantidadOperaciones + nodosV2.indexOf(nodo);
        return nodosV2.indexOf(nodo);
    }

    public int damePosEnV1De(Nodo nodo)
    {
        cantidadOperaciones = cantidadOperaciones + nodosV1.indexOf(nodo);
        return nodosV1.indexOf(nodo);
    }

    public void restaurarPosDe(Nodo nodoAdy, int posActual, int posOrig, boolean esV1)
    {
    	cantidadOperaciones += 1;
        LinkedList<Nodo> nodos = esV1 ? dameNodosV1() : dameNodosV2();
        cantidadOperaciones += 1;
        if (posActual != posOrig) {
        	cantidadOperaciones += nodos.indexOf(posActual) + posOrig;
            nodos.remove(posActual);
            nodos.add(posOrig, nodoAdy);
        }
    }

	public Nodo dameNodoDeEstosConMenorGrado(LinkedList<Nodo> nodos) 
	{
		int minGrado = Integer.MAX_VALUE;
		Nodo nodoMinGrado = null;

		for (Nodo nodo : nodos) {
			int size = this.listaAdyacencia.getEjesDe(nodo).size();
            cantidadOperaciones = cantidadOperaciones + listaAdyacencia.getCantidadDeOperaciones() + 2;
            if (minGrado > size) {
				nodoMinGrado = nodo;
				minGrado = size;
            }
		}
        return nodoMinGrado;
	}

    public long getCantidadDeOperaciones()
    {
        long ops = cantidadOperaciones;
        this.cantidadOperaciones = 0;
        return ops;
    }
}