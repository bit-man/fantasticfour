/**
 * 
 */
package fantasticfour.algo3.tp2.ej3.tests;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp2.ej3.RadixTree;

/**
 * @author bit-man
 *
 */
public class RadixTreeTest {

	/**
	 * 
	 */
	private static final String	CATEDRA_CHAU	= "chau";
	/**
	 * 
	 */
	private static final String	CATEDRA_HOLA	= "hola";
	private final String UNA_CADENA = "unacadena";
	private final String UNA_CONDENA = "unacondena";
	private final String UNA_CUERDA = "unacuerda";
	private final String UNA_CASA = "unacasa";
	private final String UNA_CADENITA = "unacadenita";
	private final String CADENA_INDEPENDIENTE = "cadenaindependiente";
    private final String VALOR_INEXISTENTE = "stringquenuncasevaausarenestaprueba";

	private RadixTree r = null;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		r = new RadixTree();
	}
	
	@Test
	public void VacioTest() {
		assertTrue( r.cardinal() == 0 );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
	}

	@Test
	public void AgregoUnaCadenaTest() {
		r.agregar( UNA_CADENA );
		
		assertTrue( r.cardinal() == 1 );
		assertTrue( r.pertenece( UNA_CADENA ) );

		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
	}

	@Test
	public void AgregoDosCadenasLibresDePrefijoTest() {
		r.agregar( UNA_CADENA );
		r.agregar( CADENA_INDEPENDIENTE );
		
		assertTrue( r.cardinal() == 2 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );

		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
	}

	@Test
	public void AgregoDosCadenasConPrefijoYLuegoUnaIndependienteTest() {
		r.agregar( UNA_CADENA );
		r.agregar( UNA_CONDENA );

		assertTrue( r.cardinal() == 2 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );

		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	

		r.agregar( CADENA_INDEPENDIENTE );
		assertTrue( r.cardinal() == 3 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );

		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
	}

	@Test
	public void Agrego3CadenasConElMismoPrefijoTest() {
		r.agregar( UNA_CADENA );
		r.agregar( UNA_CONDENA );
		r.agregar( UNA_CUERDA );

		assertTrue( r.cardinal() == 3 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertTrue( r.pertenece( UNA_CUERDA ) );
		
		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
	}
	
	@Test
	public void Agrego4CadenasConPrefijosVariosTest() {
		r.agregar( UNA_CADENA );
		r.agregar( UNA_CONDENA );
		r.agregar( UNA_CUERDA );
		r.agregar( UNA_CADENITA );

		assertTrue( r.cardinal() == 4 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertTrue( r.pertenece( UNA_CUERDA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );
		
		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
	}

	@Test
	public void AgregoTodasTest() {
		r.agregar( UNA_CADENA );
		r.agregar( UNA_CONDENA );
		r.agregar( UNA_CUERDA );
		r.agregar( UNA_CADENITA );
		r.agregar( UNA_CASA );
		r.agregar( CADENA_INDEPENDIENTE );

		assertTrue( r.cardinal() == 6 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertTrue( r.pertenece( UNA_CUERDA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );	
		assertTrue( r.pertenece( UNA_CASA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );
		
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
	}
	
	@Test
	public void AgregoUnaYLaBorroTest() {
		r.agregar( UNA_CADENA );
		
		assertTrue( r.cardinal() == 1 );
		assertTrue( r.pertenece( UNA_CADENA ) );

		r.sacar( UNA_CADENA );

		assertTrue( r.cardinal() == 0 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		
		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
	}
	
	@Test
	public void Agrego2YLasBorroTest() {
		r.agregar( UNA_CADENA );
		r.agregar( CADENA_INDEPENDIENTE );
		
		assertTrue( r.cardinal() == 2 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );

		r.sacar( UNA_CADENA );

		assertTrue( r.cardinal() == 1 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );

		r.sacar( CADENA_INDEPENDIENTE );

		assertTrue( r.cardinal() == 0 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		
		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
	}
	
	@Test
	public void Agrego3YBorroUnaTest() {
		r.agregar( UNA_CADENA );
		r.agregar( UNA_CADENITA );
		r.agregar( CADENA_INDEPENDIENTE );
		
		assertTrue( r.cardinal() == 3 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );

		r.sacar( UNA_CADENA );

		assertTrue( r.cardinal() == 2 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );

		r.sacar( CADENA_INDEPENDIENTE );

		assertTrue( r.cardinal() == 1 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );

		r.sacar( UNA_CADENITA );

		assertTrue( r.cardinal() == 0 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		
		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
	}
	
	@Test
	public void AgregoTodasYLasBorroDeAUnaTest() {
		r.agregar( UNA_CADENA );
		r.agregar( UNA_CONDENA );
		r.agregar( UNA_CUERDA );
		r.agregar( UNA_CADENITA );
		r.agregar( UNA_CASA );
		r.agregar( CADENA_INDEPENDIENTE );

		assertTrue( r.cardinal() == 6 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertTrue( r.pertenece( UNA_CUERDA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );	
		assertTrue( r.pertenece( UNA_CASA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		
		r.sacar( UNA_CUERDA );

		assertTrue( r.cardinal() == 5 );
		assertTrue( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );	
		assertTrue( r.pertenece( UNA_CASA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
		
		r.sacar( UNA_CADENA );

		assertTrue( r.cardinal() == 4 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );	
		assertTrue( r.pertenece( UNA_CASA ) );
		assertTrue( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );

		r.sacar( CADENA_INDEPENDIENTE );

		assertTrue( r.cardinal() == 3 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertTrue( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );	
		assertTrue( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );

		r.sacar( UNA_CONDENA );
		
		assertTrue( r.cardinal() == 2 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertTrue( r.pertenece( UNA_CADENITA ) );	
		assertTrue( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );

		r.sacar( UNA_CADENITA );
		
		assertTrue( r.cardinal() == 1 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
		assertTrue( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );

		r.sacar( UNA_CASA );
		
		assertTrue( r.cardinal() == 0 );
		assertFalse( r.pertenece( UNA_CADENA ) );
		assertFalse( r.pertenece( UNA_CONDENA ) );
		assertFalse( r.pertenece( UNA_CUERDA ) );
		assertFalse( r.pertenece( UNA_CADENITA ) );	
		assertFalse( r.pertenece( UNA_CASA ) );
		assertFalse( r.pertenece( CADENA_INDEPENDIENTE ) );
		assertFalse( r.pertenece( VALOR_INEXISTENTE ) );
	}

	@Test
	public void DeLACatedraTest() {
		r.agregar(CATEDRA_HOLA);
		assertTrue( r.pertenece(CATEDRA_HOLA));
		r.agregar(CATEDRA_CHAU);
		assertTrue( r.cardinal() == 2 );
		r.sacar(CATEDRA_HOLA);
		assertFalse( r.pertenece(CATEDRA_HOLA));
	}

}
