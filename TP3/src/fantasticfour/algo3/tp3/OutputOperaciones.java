/**
 * 
 */
package fantasticfour.algo3.tp3;

import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

public class OutputOperaciones {

	private static final String	EXTENSION_OUT	= ".csv";
	private String nombreArchivo;
    private File archivo;
    private List<Value> values;

    public OutputOperaciones( String nombreArchivo )
    {
        values = new ArrayList<Value>();
        this.nombreArchivo = nombreArchivo + EXTENSION_OUT;
    }

    public void setValues( long ejes, long operaciones, long mejora )
    {
        values.add(new Value(ejes, operaciones, mejora));
    }

    public void grabar()
        throws Exception
    {
        crearArchivo();
        FileWriter fileWriter = new FileWriter(archivo);
        for (Value value : values)
        {
            fileWriter.write(value.getEjes() + "," + value.getOperaciones() + "," + value.getMejora() + "\n");
        }
        fileWriter.flush();
        fileWriter.close();
    }

    private void crearArchivo()
        throws Exception
    {
        this.archivo = new File( this.nombreArchivo );
    	if (this.archivo.exists())
        {
            boolean deleted = this.archivo.delete();
            if (!deleted)
            {
                throw new Exception("No se pudo borrar el archivo " + this.archivo.getAbsolutePath());
            }
        }

        boolean created;
        try {
            created = this.archivo.createNewFile();
        }
        catch (IOException e) {
            throw new Exception("No se pudo crear el archivo " + this.archivo.getAbsolutePath(), e);
        }
        if (!created)
        {
            throw new Exception("No se pudo crear el archivo " + this.archivo.getAbsolutePath());
        }
    }

    private static class Value
    {
        private long ejes;
        private long operaciones;
        private long mejora;

        public Value(long ejes, long operaciones, long mejora)
        {
            this.ejes = ejes;
            this.operaciones = operaciones;
            this.mejora = mejora;
        }

        public long getEjes()
        {
            return this.ejes;
        }

        public long getOperaciones()
        {
            return this.operaciones;
        }

        public long getMejora()
        {
            return this.mejora;
        }
    }
}