/**
 * 
 */
package fantasticfour.algo3.tp1.ej2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Scanner;

/**
 * @author bit-man
 *
 * Lee desde un archivo, en forma secuencial, instancias a ser resueltas 
 * correspondientes al problema del Ejercicio 2, TP1 
 */
public class LectorInstancia {
	private static final String FIN_DE_ARCHIVO = "Fin";

	private Scanner s;
	
	private int n;		// Cant. de ítems
	private int P;		// Peso máximo
	private int[] v;	// Valor de cada ítem
	private int[] p;	// Peso de cada ítem
	
	public LectorInstancia( String archivo ) throws FileNotFoundException {
		 s = new Scanner(
				 new File( archivo )
			   );
		 inicializarVariables();
	}
	
	/**
	 * Lee el proximo caso del archivo, colocando los valores correspondientes en
	 * las variables n, P, v y p
	 * 
	 *  pre-condición : - debe haber más casos para leer => hayMasCasos() == true
	 *  				- la próxima línea a leer es la que tiene 'n' y 'P'
	 */
	public void leerCaso() {
		inicializarVariables();
		
		try {
			n = s.nextInt();
			P = s.nextInt();
			v = new int[n];
			p = new int[n];

			for(int i=0; i< n; i++) {
				v[i] = s.nextInt();
				p[i] = s.nextInt();
			};
		} catch (Exception e) {
			/*** 
			 *      Si da una excepción es porque no llegó al EOF o
			 * 		hubo un error => no hay más nada que hacer
			 */
		}
	}
	
	public boolean hayMasCasos() {
		boolean ret;

		try {
			String linea = s.next();
			ret = ! linea.equals(FIN_DE_ARCHIVO);
			// Esta última deja posicionado el archivo en el EOF o
			// en la línea siguiente a 'Caso'
		} catch (Exception e) {
			// Si da una excepción es porque o llegó al EOF o
			// hubo un error => no hay más nada que hacer
			ret = false;
		}
		
		return ret;
	}
	
	public int pesoTotal() {
		return P;
	}
	
	public int items() {
		return n;
	}

	public int[] valores() {
		return v;
	}
	
	public int[] pesos() {
		return p;
	}
	
	public void cerrar() throws IOException {
		s.close();
	}
	
	private void inicializarVariables() {
		n = 0;
		P = 0;
		v = null;
		p = null;
	}
}
