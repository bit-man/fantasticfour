/**
 * 
 */
package fantasticfour.algo3.tp3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * @author bit-man
 *
 */
public class InputGrabar {

	private static final String	ESPACIO	= " ";
	private static final String	FIN_DE_ARCHIVO	= "-1";

	private PrintWriter pw = null;
	private String nombreArchivo;
	private List<Nodo> nodosV1 = null;
	private List<Nodo> nodosV2 = null;
	private List<Eje> ejes = null;
	private List<Nodo> nodosIV1 = null;
	private List<Nodo> nodosIV2 = null;
	private List<Eje> ejesI = null; 
	private File f;

	/***
	 * Constructor que requiere el prefijo del archivo, generando un archivo del
	 * formato "prefijoXX.in" donde  XX es un nro. implementdo con un long
	 * @param prefijo : parte inicial del archivo
	 * @throws IOException : cuando hay un error en la operación de I/O del archivo
	 * @throws FileNotFoundException : cuando no hay más nombres de archivos disponibles
	 */
    public InputGrabar( String prefijo ) throws IOException {
    	File f = generarFile( prefijo );
    	initFile(f);
	    nuevaInstancia();
    }
    
	public InputGrabar( File f ) throws IOException {
		initFile(f);
	    nuevaInstancia();
    }
	
    /**
     * Inicializa el File pasado como par,ametro
	 * @param f
     * @throws IOException 
	 */
	private void initFile(File f) throws IOException {
		this.f = f;
    	f.createNewFile();
    	nombreArchivo = f.getName();
	    pw = new PrintWriter( f );
	}


    /**
     * Busca un nombre de archivo libre con el formato "prefijoXXX.in" 
	 * @param nombreArch
	 * @return File
	 */
	private File generarFile(String nombreArch) {
		nombreArchivo = nombreArch;
		return new File(nombreArchivo);
	}

	/***
	 * Genera una nueva instancia, pero NO GRABA la instancia actual en el archivo
	 */
	public void nuevaInstancia() {
    	nodosV1 = new LinkedList<Nodo>();
    	nodosV2 = new LinkedList<Nodo>();
    	ejes = new LinkedList<Eje>();
    	nodosIV1 = new LinkedList<Nodo>();
    	nodosIV2 = new LinkedList<Nodo>();
    	ejesI = new LinkedList<Eje>();
    }
    
	public void setNodosV1(LinkedList<Nodo> n) {
		nodosV1 = n;
	}
    
	public void setNodosV2(LinkedList<Nodo> n) {
		nodosV2 = n;
	}
	    
	public void setEjes(LinkedList<Eje> n) {
		ejes = n;
	}

	public void setNodosIV1(LinkedList<Nodo> n) {
		nodosIV1 = n;
	}
    
	public void setNodosIV2(LinkedList<Nodo> n) {
		nodosIV2 = n;
	}
	    
	public void setEjesI(LinkedList<Eje> n) {
		ejesI = n;
	}

	public void grabar() {
		grabarNodos(nodosV1);
		grabarNodos(nodosV2);
		grabarEjes(ejes);
		grabarNodos(nodosIV1);
		grabarNodos(nodosIV2);
		grabarEjes(ejesI);
	};
	
    /**
     * Graba la lista de ejes pasada como parámetro
	 * @param ejes2
	 */
	private void grabarEjes(List<Eje> ej) {
		pw.println( ej.size() );
		for( Eje e : ej )
			pw.println( e.dameDesde() + ESPACIO + e.dameHasta() );
	}

	/**
     * Graba la lista de nodos pasada como parámetro
	 * @param nodos
	 */
	private void grabarNodos(List<Nodo> nodos) {
		pw.println( nodos.size() );
		for( Nodo n : nodos )
			pw.println(n);
	}

	public void cerrar() {
    	pw.println(FIN_DE_ARCHIVO);
    	pw.close();
    }

    public final String getNombreArchivo() {
    	return nombreArchivo;
    }

    public final File getFile() {
    	return f;
    }
}
