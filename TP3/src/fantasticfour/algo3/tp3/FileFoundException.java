/**
 * 
 */
package fantasticfour.algo3.tp3;

/**
 * @author bit-man
 *
 */
public class FileFoundException extends Exception {
	private static final long	serialVersionUID	= 1384596569958590L;

	public FileFoundException( String msg ) {
		super(msg);
	}
}
