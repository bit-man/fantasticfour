package fantasticfour.algo3.tp3.ej4;

import fantasticfour.algo3.tp3.*;

import java.util.LinkedList;
import java.util.Random;

public class GolosoRandom
{
    Instancia instancia;
	private Random randomGenerator;
    long cantidadOperaciones = 0;

    public GolosoRandom(Instancia instancia)
    {
        this.instancia = instancia;
        randomGenerator = new Random(System.currentTimeMillis());
    }

    public long getCantidadDeOperaciones()
    {
        long ops = cantidadOperaciones;
        this.cantidadOperaciones = 0;
        return ops;
    }

	public Grafo resolverConGolosoRandom()
	{
		Grafo grafo = instancia.dameGrafoOriginal().clone();
        Grafo grafoTotal = instancia.dameGrafo().clone();
        
        LinkedList<Nodo> nodosIV1 = new LinkedList<Nodo>(instancia.dameNodosIV1());
        cantidadOperaciones = cantidadOperaciones + instancia.getCantidadDeOperaciones();
        LinkedList<Nodo> nodosIV2 = new LinkedList<Nodo>(instancia.dameNodosIV2());
        cantidadOperaciones = cantidadOperaciones + instancia.getCantidadDeOperaciones();
        cantidadOperaciones = cantidadOperaciones + nodosIV1.size() + nodosIV2.size();

        int sizeTotal = nodosIV1.size() + nodosIV2.size();
        cantidadOperaciones = cantidadOperaciones + 4;

        while (sizeTotal > 0)
        {
            LinkedList<Nodo> nodosActuales;
            boolean esIV1;

            int randomIVx = randomGenerator.nextInt(2);
            cantidadOperaciones = cantidadOperaciones + 1;

            cantidadOperaciones = cantidadOperaciones + 1;
            if  (randomIVx == 0) {
                cantidadOperaciones = cantidadOperaciones + nodosIV1.size() + 1;
                if (nodosIV1.size() != 0) {
                    nodosActuales = nodosIV1;
                    esIV1 = true;
                    cantidadOperaciones = cantidadOperaciones + 2;
                } else {
                    nodosActuales = nodosIV2;
                    esIV1 = false;
                    cantidadOperaciones = cantidadOperaciones + 2;
                }
            } else {
                cantidadOperaciones = cantidadOperaciones + nodosIV2.size() + 1;
                if (nodosIV2.size() != 0) {
                    nodosActuales = nodosIV2;
                    esIV1 = false;
                    cantidadOperaciones = cantidadOperaciones + 2;
                } else {
                    nodosActuales = nodosIV1;
                    esIV1 = true;
                    cantidadOperaciones = cantidadOperaciones + 2;
                }
            }

            Nodo nodoRandom;

            if (esIV1) {
                nodoRandom = grafoTotal.dameNodoConMayorGradoDeV1RestringidoA(nodosActuales);
                cantidadOperaciones = cantidadOperaciones + grafoTotal.getCantidadDeOperaciones();
            } else {
                nodoRandom = grafoTotal.dameNodoConMayorGradoDeV2RestringidoA(nodosActuales);
                cantidadOperaciones = cantidadOperaciones + grafoTotal.getCantidadDeOperaciones();
            }
            cantidadOperaciones = cantidadOperaciones + 2;

            LinkedList<Eje> adyacentesHastaAhora = instancia.dameEjesConAdyacentes(grafo, nodoRandom, esIV1);
            cantidadOperaciones = cantidadOperaciones + grafo.getCantidadDeOperaciones();
            cantidadOperaciones = cantidadOperaciones + instancia.getCantidadDeOperaciones();

            int minCruces = Integer.MAX_VALUE;
            int posMinCruce = -1;
            cantidadOperaciones = cantidadOperaciones + 3;

            for (int j = 0 ; j < (esIV1 ? grafo.dameNodosV1().size() : grafo.dameNodosV2().size()) ; j++) {
                cantidadOperaciones = cantidadOperaciones + grafo.getCantidadDeOperaciones();
                int cruces = grafo.cantidadCruces(nodoRandom, adyacentesHastaAhora, j, esIV1);
                cantidadOperaciones = cantidadOperaciones + grafo.getCantidadDeOperaciones();
                cantidadOperaciones = cantidadOperaciones + 2;
                if (minCruces > cruces) {
                    minCruces = cruces;
                    posMinCruce = j;
                    cantidadOperaciones = cantidadOperaciones + 2;
                }
            }

            if (esIV1)  {
                grafo.agregarNodoYEjesEnV1(nodoRandom, adyacentesHastaAhora, posMinCruce);
                cantidadOperaciones = cantidadOperaciones + grafo.getCantidadDeOperaciones();
            }
            else {
                grafo.agregarNodoYEjesEnV2(nodoRandom, adyacentesHastaAhora, posMinCruce);
                cantidadOperaciones = cantidadOperaciones + grafo.getCantidadDeOperaciones();
            }

            nodosActuales.remove(nodoRandom);
            cantidadOperaciones = cantidadOperaciones + nodosActuales.size();

            sizeTotal--;
        }
		return grafo;
	}
}