/**
 * 
 */
package fantasticfour.algo3.tp1.ej2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * @author bit-man
 *
 */
public class EscribirEntrada {
	private static final int PESO_CADA_ITEM = 1;
	private static final int VALOR_CADA_ITEM = 10;
	
	PrintWriter pw = null;
	private int items;
	
	public EscribirEntrada( String nombreArchivo ) throws FileNotFoundException {
		pw = new PrintWriter(
					new File( nombreArchivo )
				);
	}

	
	public void items(int n) {
		items = n;
	}
	
	public void grabar() {
		pw.println("Caso");
		
		int peso = items * PESO_CADA_ITEM;
		pw.println(items + " " + peso);
		
		for(int i=0; i < items; i++)
			pw.println( VALOR_CADA_ITEM + " " + PESO_CADA_ITEM);

	}
	
	public void cerrar() {
		pw.println("Fin");
		pw.close();
	}
}
