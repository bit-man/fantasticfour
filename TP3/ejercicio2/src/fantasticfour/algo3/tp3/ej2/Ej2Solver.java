package fantasticfour.algo3.tp3.ej2;

import fantasticfour.algo3.tp3.*;

import java.util.LinkedList;

public class Ej2Solver
{
    private Instancia instancia;
    static private long cantOps;

    public Ej2Solver(Instancia instancia)
    {
        this.instancia = instancia;
        cantOps = 0;
    }

    protected static void incrementarOperaciones(long cantidadDeOperaciones)
    {
        cantOps = cantOps + cantidadDeOperaciones;
    }

    public long cantidadOperaciones()
    {
        return cantOps;
    }

    public Grafo solve()
    {
        //comienzo//
        Grafo mejorGrafo = null;
        int mejorCruces = Integer.MAX_VALUE;
        final LinkedList<Eje> ejes = instancia.dameEjesTodos();
        incrementarOperaciones( instancia.getCantidadDeOperaciones() );
        final Grafo grafo = new Grafo( instancia.dameNodosV1Todos(), instancia.dameNodosV2Todos(), ejes);
        incrementarOperaciones( instancia.getCantidadDeOperaciones() );

        final GenerarNodosEnLinea generadorNodosV1 = new GenerarNodosEnLinea(instancia.dameNodosV1(), instancia.dameNodosIV1());
        incrementarOperaciones(instancia.getCantidadDeOperaciones());
        
        while (generadorNodosV1.hayMasLineas())
        {
            LinkedList<Nodo> nodosV1 = generadorNodosV1.otraLinea();
            final GenerarNodosEnLinea generadorNodosV2 = new GenerarNodosEnLinea(instancia.dameNodosV2(), instancia.dameNodosIV2());
            incrementarOperaciones(instancia.getCantidadDeOperaciones());
            while (generadorNodosV2.hayMasLineas())
            {
                //todo No rearmar grafo. Ver ejercicio4.
                LinkedList<Nodo> nodosV2 = generadorNodosV2.otraLinea();
                grafo.setNodosV1(nodosV1);
                grafo.setNodosV2(nodosV2);

                int crucesActual = grafo.cantidadCruces();
                incrementarOperaciones(grafo.getCantidadDeOperaciones());

                incrementarOperaciones(1);
                if (crucesActual < mejorCruces)
                {
                    mejorCruces = crucesActual;
                    mejorGrafo = grafo.clone();
                    incrementarOperaciones(grafo.getCantidadDeOperaciones());
                }
            }
        }
        
        return mejorGrafo;
    }
}
