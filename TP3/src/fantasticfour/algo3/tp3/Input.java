package fantasticfour.algo3.tp3;

import java.util.Scanner;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;

public class Input
{
    private Scanner scanner;
    private List<Instancia> instancias;
    public Input(File inputFile)
    {
        try {
            this.scanner = new Scanner(inputFile);
            instancias = new ArrayList<Instancia>();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            //ignorar. El chequeo de que si el file existe se hace antes.
        }
    }

    public List<Instancia> dameInstancias()
    {
        return instancias;
    }

    public void leerArchivo()
    {
        while (true)
        {
            //leer nodos V1
            LinkedList<Nodo> nodosV1 = new LinkedList<Nodo>();
            int cantidadNodosV1 = scanner.nextInt();
            if (cantidadNodosV1 == -1)
            {
                //fin instancia
                return;
            }
            for (int i = 0; i < cantidadNodosV1; i++)
            {
                Nodo nodo = new Nodo(scanner.nextInt(), true);
                nodosV1.add(nodo);
            }

            //leer nodos V2
            LinkedList<Nodo> nodosV2 = new LinkedList<Nodo>();
            int cantidadNodosV2 = scanner.nextInt();
            for (int i = 0; i < cantidadNodosV2; i++)
            {
                Nodo nodo = new Nodo(scanner.nextInt(), true);
                nodosV2.add(nodo);
            }

            //leer ejes de V1 a V2
            int cantidadAristas = scanner.nextInt();
            LinkedList<Eje> ejes = new LinkedList<Eje>();
            for (int i = 0; i < cantidadAristas; i++)
            {
                Nodo nodoDesde = new Nodo(scanner.nextInt(), true);
                Nodo nodoHasta = new Nodo(scanner.nextInt(), true);
                Eje eje;
                if (nodosV1.contains(nodoDesde))
                {
                    eje = new Eje(nodoDesde, nodoHasta);
                }
                else
                {
                    eje = new Eje(nodoHasta, nodoDesde);
                }
                ejes.add(eje);
            }

            //leer nodos IV1
            LinkedList<Nodo> nodosIV1 = new LinkedList<Nodo>();
            int cantidadNodosIV1 = scanner.nextInt();
            for (int i = 0; i < cantidadNodosIV1; i++)
            {
                Nodo nodo = new Nodo(scanner.nextInt(), false);
                nodosIV1.add(nodo);
            }

            //leer nodos IV2
            LinkedList<Nodo> nodosIV2 = new LinkedList<Nodo>();
            int cantidadNodosIV2 = scanner.nextInt();
            for (int i = 0; i < cantidadNodosIV2; i++)
            {
                Nodo nodo = new Nodo(scanner.nextInt(), false);
                nodosIV2.add(nodo);
            }

            //leer ejes de IV1 a IV2
            int cantidadAristasI = scanner.nextInt();
            LinkedList<Eje> ejesI = new LinkedList<Eje>();
            for (int i = 0; i < cantidadAristasI; i++)
            {
                int desde = scanner.nextInt();
                int hasta = scanner.nextInt();
                boolean esDesdeOriginal = false;
                boolean esHastaOriginal = false;

                for(Nodo nodoV1 : nodosV1) {
                    if (nodoV1.nodo == desde) {
                        esDesdeOriginal = true;
                        break;
                    }
                    if (nodoV1.nodo == hasta) {
                        esHastaOriginal = true;
                        break;
                    }
                }

                for (Nodo nodoV2 : nodosV2) {
                    if (nodoV2.nodo == desde) {
                        esDesdeOriginal = true;
                        break;
                    }

                    if (nodoV2.nodo == hasta) {
                        esHastaOriginal = true;
                        break;
                    }

                }

                Nodo nodoDesde = new Nodo(desde, esDesdeOriginal);
                Nodo nodoHasta = new Nodo(hasta, esHastaOriginal);

                Eje eje;
                if (nodosV1.contains(nodoDesde) || nodosIV1.contains(nodoDesde))
                {
                    eje = new Eje(nodoDesde, nodoHasta);
                }
                else
                {
                    eje = new Eje(nodoHasta, nodoDesde);
                }
                ejesI.add(eje);
            }

            instancias.add(new Instancia(nodosV1, nodosV2, ejes, nodosIV1, nodosIV2, ejesI));
        }
    }
}