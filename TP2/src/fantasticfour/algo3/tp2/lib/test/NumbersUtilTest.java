package fantasticfour.algo3.tp2.lib.test;

import static org.junit.Assert.*;

import org.junit.Test;

import fantasticfour.algo3.tp2.lib.Constantes;
import fantasticfour.algo3.tp2.lib.NumbersUtil;

public class NumbersUtilTest {

	private static final int UNO = 1;
	private static final int MAYOR_QUE_UNO = UNO + 1;
	private static final int MENOR_QUE_UNO = UNO - 1;

	@Test
	public void testMinimo() {
		assertTrue( NumbersUtil.minimo( UNO, MAYOR_QUE_UNO) == UNO);
		assertTrue( NumbersUtil.minimo( UNO, MENOR_QUE_UNO) == MENOR_QUE_UNO);
		assertTrue( NumbersUtil.minimo( MAYOR_QUE_UNO, UNO) == UNO);
		assertTrue( NumbersUtil.minimo( MENOR_QUE_UNO, UNO) == MENOR_QUE_UNO);
	}
	
	@Test
	public void testMaximo() {
		assertTrue( NumbersUtil.maximo( UNO, MAYOR_QUE_UNO) == MAYOR_QUE_UNO);
		assertTrue( NumbersUtil.maximo( UNO, MENOR_QUE_UNO) == UNO);
		assertTrue( NumbersUtil.maximo( MAYOR_QUE_UNO, UNO) == MAYOR_QUE_UNO);
		assertTrue( NumbersUtil.maximo( MENOR_QUE_UNO, UNO) == UNO);
	}
	
	@Test
	public void testLogaritmo() {
		assertTrue( NumbersUtil.logaritmo( (double) 8, Constantes.BASE_2) == 3 );
		assertTrue( NumbersUtil.logaritmo( (double) 10000, Constantes.BASE_10) == 4 );
	}

}
