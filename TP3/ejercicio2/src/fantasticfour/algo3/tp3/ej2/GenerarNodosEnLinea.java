/**
 * 
 */
package fantasticfour.algo3.tp3.ej2;

import fantasticfour.algo3.tp3.Nodo;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *  
 * Permite generar una línea de nodos que sea una combinación de los nodos tipo V (que
 * deben mantener la misma posición relativa) y los nodos tipo IV (que pueden estar en cualquier
 * posición)
 * <p>
 * Los nodos V se numeran de 0 a nodosV.size()-1 y los IV desde nodosV.size() hasta
 * nodosV.size() + nodosIV.size() - 1 
 * <p>
 * El algoritmo se basa en la ubicación de los nodosIV en los espacios ubicados entre los
 * nodosV
 * 
 * @author bit-man 
 */
public class GenerarNodosEnLinea {

//	private static final int CUALQUIER_VALOR = 1;
	
	private List<Nodo> nodosV;
	private List<Nodo> nodosIV;
	
	/*** 
	 * Cada dígito de nodos me dice en que posición debe ir ese nodo
	 */
	private GeneradorNodos genNodos;
	
	/**
	 * Cada dígito indica cuántos nodos debo colocarse en ese hueco.
	 */
	private DesparramadorDeNodos espacios;
	private int cantHuecos;
	
	/***
	 * Genera todas las combinaciones de nodos de una de las partes de un digrafo, siendo
	 * nodosV los nodos que deben mantener la posición relativa entre si y los nodosIV que
	 * pueden ocupar cualquier posición 
	 * @param nodosV : lista de los nodos que deben mantener la posición relativa entre si 
	 * @param nodosIV : lista de los nodos que pueden estar en cualquier posición
	 */
	public GenerarNodosEnLinea( List<Nodo> nodosV, List<Nodo> nodosIV ) {
		this.nodosV = nodosV;
		this.nodosIV = nodosIV;
        Ej2Solver.incrementarOperaciones(1);
		this.cantHuecos = nodosV.size() + 1;
        this.espacios = new DesparramadorDeNodos( cantHuecos, nodosIV.size() );
        nuevoGeneradorDeNodos();
    }

	private void nuevoGeneradorDeNodos() {
		this.genNodos = new GeneradorNodos( this.nodosIV.size() );
	}

	/***
	 * Devuelve una nueva combinación de nodos para el digrafo.
	 * <p>
	 * En caso que no haya más lineas esta función no debe llamarse, si
	 * esto es hecho puede devolver cualquier valor.
	 * <p>
	 * Ejemplo de uso :
	 * <p>
	 * <pre>
	 * 		GenerarNodosEnLinea n = new GenerarNodosEnLinea( nodosV, nodosIV )
	 * 
	 * 		while( n.hayMasNodos() ) {
	 *     		List<Nodo> l = n.otraLinea();
	 *     		// procesar la nueva línea de nodos ...
	 * 		}
	 * </pre>
	 * 
	 * @return List: una nueva combinación de nodos 
	 */
	public LinkedList<Nodo> otraLinea() {
		LinkedList<Nodo> ret = reconstruirLineaADevolver();
		
		this.genNodos.otroGrafo();
        Ej2Solver.incrementarOperaciones(1);
		if ( ! this.genNodos.hasyMasGrafos() ) {
			this.espacios.next();
			nuevoGeneradorDeNodos();
        }

		return ret;
	}

	private LinkedList<Nodo> reconstruirLineaADevolver() {
		LinkedList<Nodo> ret =  new LinkedList<Nodo>();
		Iterator<Nodo> itNodosV = this.nodosV.iterator();
		int jNodosIV = 0;
        Ej2Solver.incrementarOperaciones(1);
		for(int iHuecos = 0; iHuecos <  this.cantHuecos ; iHuecos++) {
			int nodosIVAgregados = agregarNodosIVAlHueco(iHuecos, jNodosIV, ret);
            Ej2Solver.incrementarOperaciones(1);
			jNodosIV += nodosIVAgregados;
			if ( itNodosV.hasNext() )
            {
               ret.add( itNodosV.next() );
               Ej2Solver.incrementarOperaciones(1);
            }
            Ej2Solver.incrementarOperaciones(2); // iHuecos < ... y iHuecos++
        }

		return ret;
	}

	private int agregarNodosIVAlHueco(int hueco, int nodoInicialIV, List<Nodo> l) {
		int nodosEnEsteHueco = 0;
		try {
			nodosEnEsteHueco = this.espacios.nodosEnElHueco(hueco);
            Ej2Solver.incrementarOperaciones(1);
            for(int i=0; i < nodosEnEsteHueco; i++)
            {
                l.add( dameNodoIVenLaPos( i + nodoInicialIV ) );
                Ej2Solver.incrementarOperaciones(2);
                
                Ej2Solver.incrementarOperaciones(2); // i < ... e i++
            }
        } catch (Exception e) {
			e.printStackTrace();
		}
			
		return nodosEnEsteHueco;
	}

	
	private Nodo dameNodoIVenLaPos(int pos) {
		Nodo ret = null;
		try {
            Ej2Solver.incrementarOperaciones(1);
			for(int i=0; i<this.genNodos.getDigitos(); i++)
            {
	            Ej2Solver.incrementarOperaciones(1);
                if(this.genNodos.getDigito(i) == pos)
                {
                    ret = this.nodosIV.get(i);
                }
                Ej2Solver.incrementarOperaciones(2); // i < ... e i++
            }
        } catch (Exception e) {
			e.printStackTrace();
			assert(false);
		}
		
		return ret;
	}

	/***
	 * Identifica si hay más combinaciones a extraer
	 * @return  boolean identificando si puede volver a llamarse al método otraLinea()
	 */
	public boolean hayMasLineas() {
        Ej2Solver.incrementarOperaciones(1);
		return ! espacios.noHayMasCombinaciones();
	}
}
