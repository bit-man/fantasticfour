package fantasticfour.algo3.tp1.ej1;

public class Ej1Solver 
{
	private Ej1Input ej1Input;
	private Ej1Output ej1Output;
	private long ops;
	
	public Ej1Solver(Ej1Input ej1Input, Ej1Output ej1Output) 
	{
		this.ej1Input = ej1Input;
		this.ej1Output = ej1Output;
	}

	public void solve() 
	{
		long value;
		while (ej1Input.hasMoreValues() 
				&& (value = ej1Input.getNextValue()) != 0) {
			SolverResult result = factorize(value);
			ej1Output.print(result);
		}
	}

	private SolverResult factorize(long originalValue) 
	{
		SolverResult result = null;
		long value = originalValue;
		long pot;
		long currentMaxPrime = originalValue;
		long currentMaxPot = 1;
		ops = 5;	// 5 asignaciones incluída la de la guarda.
		
		for (int i = 2; i <= value; i++) {
			addOps(2);	// Chequeo guarda e i++.
			if(isPrime(i)) {
				pot = 0;
				addOps(2);	// pot = 0 y chequeo guarda.
				while (value % i == 0) {
					value = value / i;
					pot++;
					addOps(6); // Ops anteriores y guarda.
				}
				
				addOps(1);	// Guarda if.
				if (pot >= currentMaxPot) {
					currentMaxPrime = i;
					currentMaxPot = pot;
					addOps(3); // Asignaciones anteriores y guarda.
				}
			}
		}

		// TODO: agregar la cantidad de operaciones.
		result = new SolverResult(originalValue, currentMaxPrime, currentMaxPot, getOps());
		
		return result;
	}

	private boolean isPrime(long number) 
	{
		addOps(3);	// Comparaciones en guarda. (== y OR)
		if (number == 2 || number == 3) {
			// Si el número es 2 ó 3 => es primo.
			return true;
		}

		addOps(7); // Comparaciones y operaciones en guarda (%, == y OR)
		if (number == 1 || number % 2 == 0 || number % 3 == 0) {
			// Es divisible por 3 ó es par ó 1 => no es primo.
			return false;
		}

		final double sqrtN = Math.sqrt(Long.valueOf(number).doubleValue());
		addOps(3); // (sqrt(N) + asignación + asignación en guarda)

		for (long i = 1; (6 * i + 1) <= sqrtN && (6 * i - 1) <= sqrtN; i++) {
			addOps(9);  // Guarda. (*, +, -, ++ y AND)
			
			// Optimizaciones 2:
			//  Salteo los multiplos de 2 y de 3, y chequeo hasta la raíz cuadrada de number.
			
			addOps(9); // (*, +, -, ++ y OR)
			if (number % (6 * i + 1) == 0 || number % (6 * i - 1) == 0) {
				return false;
			}
		}

		return true;
	}

	public void addOps(long opsToAdd)
	{
		this.ops = this.ops + opsToAdd;
	}

	private long getOps() 
	{
		return this.ops;
	}

	public static class SolverResult
	{
		long number;
		long maxPrime;
		long maxPot;
		long qtyOps;
		
		public SolverResult(long number, long maxPrime, long maxPot, long qtyOps) 
		{
			this.number = number;
			this.maxPrime = maxPrime;
			this.maxPot = maxPot;
			this.qtyOps = qtyOps;
		}

		public long getMaxPrime() 
		{
			return maxPrime;
		}

		public long getMaxPot() 
		{
			return maxPot;
		}

		public long getQtyOps() 
		{
			return qtyOps;
		}

		public long getNumber() 
		{
			return number;
		}
	}
}
