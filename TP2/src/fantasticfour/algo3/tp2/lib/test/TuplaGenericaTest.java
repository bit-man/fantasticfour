/**
 * 
 */
package fantasticfour.algo3.tp2.lib.test;

import static org.junit.Assert.*;

import org.junit.Test;

import fantasticfour.algo3.tp2.lib.TuplaGenerica;

/**
 * @author bit-man
 *
 */
public class TuplaGenericaTest {

	/**
	 * 
	 */
	private static final String	STRING_X	= "xxxx";
	private static final String	STRING_Y	= "yyyy";

	/**
	 * Test method for {@link fantasticfour.algo3.tp2.lib.TuplaGenerica#TuplaGenerica()}.
	 */
	@Test
	public void testTuplaGenerica() {
		TuplaGenerica t = new TuplaGenerica();
		assertNull( t.x );
		assertNull( t.y );
		
		t.y = STRING_Y;
		assertNull( t.x );
		assertTrue( STRING_Y.equals( (String) t.y) );

		t.x = STRING_X;
		assertTrue( STRING_Y.equals( (String) t.y) );
		assertTrue( STRING_X.equals( (String) t.x) );
		
	}

	/**
	 * Test method for {@link fantasticfour.algo3.tp2.lib.TuplaGenerica#TuplaGenerica(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testTuplaGenericaObjectObject() {
		TuplaGenerica t = new TuplaGenerica( STRING_X );
		assertTrue( t.x.equals(STRING_X) );
		assertNull( t.y );
		
	}

	/**
	 * Test method for {@link fantasticfour.algo3.tp2.lib.TuplaGenerica#TuplaGenerica(java.lang.Object)}.
	 */
	@Test
	public void testTuplaGenericaObject() {
		TuplaGenerica t = new TuplaGenerica( STRING_X, STRING_Y );
		assertTrue( t.x.equals(STRING_X) );
		assertTrue( t.y.equals(STRING_Y) );
	}

	@Test
	public void testTuplaGenericaSwap() {
		TuplaGenerica t = new TuplaGenerica( STRING_X, STRING_Y );
		t.swap();
		assertTrue( t.x.equals(STRING_Y) );
		assertTrue( t.y.equals(STRING_X) );
	}

}
