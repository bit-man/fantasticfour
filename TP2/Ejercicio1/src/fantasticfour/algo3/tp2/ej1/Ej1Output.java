package fantasticfour.algo3.tp2.ej1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

public class Ej1Output 
{	
	private FileWriter archivoSalida;
	private FileWriter statisticsFile;
	private String nombreArchivoSalida;	

	public Ej1Output(File archivoSalida) 
		throws IOException 
	{
		this.nombreArchivoSalida = archivoSalida.getAbsolutePath();
		this.archivoSalida = new FileWriter(archivoSalida);
		this.statisticsFile = new FileWriter(new File(nombreArchivoSalida + ".op"));
	}

	public void cerrar()
	{
		try {
			if (archivoSalida != null) {
				archivoSalida.close();
			}
		} catch (IOException e) {
			printCloseError(nombreArchivoSalida);
		}		
		try {
			if (statisticsFile != null) {
				statisticsFile.close();
			}
		} catch (IOException e2) {
			printCloseError(nombreArchivoSalida + ".op");
		}
	}

	private void printCloseError(String nombreArchivoSalida) 
	{
		System.out.println("No se puede cerrar el archivo " + nombreArchivoSalida);
	}

	public void grabarCantOperaciones(int tamanio, int valor) throws IOException 
	{
		statisticsFile.write(tamanio + "   " + valor + "   " + "\n");	
	}

	public void grabarResultado(Grafo grafo) 
		throws IOException 
	{
		// Graba la salida ordenada.
		TreeSet<Integer> salidaOrdenada = new TreeSet<Integer>(grafo.getListaAdyacencia().keySet());
		archivoSalida.write(String.valueOf(salidaOrdenada.size()));
		for (Integer i : salidaOrdenada){
			archivoSalida.write( " " + i);
		}
		archivoSalida.write("\n");
	}

	public void grabarNoResultado() 
		throws IOException 
	{
		archivoSalida.write("-1");
		archivoSalida.write( "\n" );
	}

}


