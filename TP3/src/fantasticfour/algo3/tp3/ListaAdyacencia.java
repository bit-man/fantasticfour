package fantasticfour.algo3.tp3;

import java.util.HashMap;
import java.util.LinkedList;

public class ListaAdyacencia
{
    private int cantNodos;
    private int cantidadOperaciones = 0;
    private HashMap<Nodo, LinkedList<Nodo>> listaAdyacencia;

    public ListaAdyacencia()
    {
        this.listaAdyacencia = new HashMap<Nodo, LinkedList<Nodo>>();
        this.cantNodos = 0;
    }

    public ListaAdyacencia(Grafo grafo)
    {
        this();
        for (Eje ejeActual : grafo.dameEjes())
        {
            agregar(ejeActual.dameDesde(), ejeActual.dameHasta());
        }
        agregarNodosAislados(grafo);
    }

    /**
     * Agrega los nodos del grafo que no están conectados
     * @param grafo
     */
    private void agregarNodosAislados(Grafo grafo)
    {
        int nodosTotales = grafo.dameNodosV2().size() + grafo.dameNodosV1().size();
        cantidadOperaciones = cantidadOperaciones + 3;
        
        cantidadOperaciones += 1;
        for (int i = 1; i <= nodosTotales; i++)
        {
            Nodo j = new Nodo(i, false);
            cantidadOperaciones += 1;
            if ( ! listaAdyacencia.containsKey(j) )
            {
                agregar(j);
            }
            cantidadOperaciones += 2;
        }
    }

    public void agregar(Nodo i, Nodo j)
    {
        agregarEnlaceDirigido(i, j);
        agregarEnlaceDirigido(j, i);
    }

    /**
     * Agrega el enlace que va desde i hacia j. Si se trata de un grafo dirigido
     * sólo se agrega en el sentido del enlace, pero en el caso de un grafo
     * no dirigido (como es este) es necesario agregarlo en ambos sentidos.
     * Funcionalmente hay un camino tanto de i hacia j como de j hacia i
     * @param i : Nodo origen
     * @param j : Nodo destino
     */
    private void agregarEnlaceDirigido(Nodo i, Nodo j)
    {
        // (i, j) no debe existir en el grafo, no se hacen chequeos.
        // Tomar la lista i del hashMap = O(1)
        // Agregar el nodo adelante de la lista = O(1)
        // En consecuencia, agregar es O(1).
        if (this.tieneNodo(i))
        {
            this.getListaAdyacencia().get(i).addFirst(j);
            cantidadOperaciones += 2;
        }
        else
        {
            LinkedList<Nodo> list = new LinkedList<Nodo>();
            list.addFirst(j);
            this.getListaAdyacencia().put(i, list);
            cantNodos++;
            cantidadOperaciones = cantidadOperaciones + 4;
        }
    }

    public void agregar(Nodo nodo)
    {
        // nodo es un nodo solitario.
        // Tomar la lista i del hashMap = O(1)
        // Agregar el nodo adelante de la lista = O(1)
        // En consecuencia, agregar es O(1).
        cantidadOperaciones = cantidadOperaciones + 2;

        if (this.getListaAdyacencia().get(nodo) == null)
        {
            this.getListaAdyacencia().put(nodo, new LinkedList<Nodo>());
            cantNodos++;
            cantidadOperaciones = cantidadOperaciones + 3;
        }
    }

    public LinkedList<Nodo> getEjesDe(Nodo nodo)
    {
        LinkedList<Nodo> ret;
        ret = this.getListaAdyacencia().get(nodo);
        cantidadOperaciones = cantidadOperaciones + 1;

        if (ret == null)
        {
            ret = new LinkedList<Nodo>();
            cantidadOperaciones = cantidadOperaciones + 1;
        }

        return ret;
    }

    public long getCantidadDeOperaciones()
    {
        long ops = cantidadOperaciones;
        this.cantidadOperaciones = 0;
        return ops;
    }

    public int getCantNodos()
    {
        return cantNodos;
    }

    public HashMap<Nodo, LinkedList<Nodo>> getListaAdyacencia()
    {
        return listaAdyacencia;
    }

    /*
     * Verifica si el grafo tiene un nodo determinado
     * Complejidad: O(1)
     */
    public boolean tieneNodo(Nodo nodo)
    {
        cantidadOperaciones = cantidadOperaciones + 2;
        return this.getListaAdyacencia().get(nodo) != null;
    }

    public String toString()
    {
        String output = "Grafo de " + this.getCantNodos() + " nodos: ";
        for (Nodo i : this.getListaAdyacencia().keySet())
        {
            LinkedList<Nodo> linkedList = this.getListaAdyacencia().get(i);
            if (linkedList.size() > 0)
            {
                for (Nodo j : linkedList)
                {
                    output += ("[" + i + ", " + j + "] ");
                }
            }
            else
                output += ("[" + i + "] ");
        }
        return output;
    }
}