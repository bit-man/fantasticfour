package fantasticfour.algo3.tp2.ej2;

public class Matriz 
{
	private boolean[][] fila;
	private int dimension;
	
	public Matriz(int dimension)
	{
		this.dimension = dimension;
		this.fila = new boolean[dimension][];
		for(int i = 0; i < dimension; i++)
			fila[i] = new boolean[dimension - i];
	}

	public void agregar(int pueblo1, int pueblo2, boolean value)
	{
		int min = Math.min(pueblo1, pueblo2);
		boolean[] columna = fila[min];
		columna[Math.max(pueblo1, pueblo2) - min] = value;
	}

	public void agregar(int pueblo1, int pueblo2)
	{
		this.agregar(pueblo1, pueblo2, true);
	}
	
	public boolean obtenerValor(int pueblo1, int pueblo2) 
	{
		int min = Math.min(pueblo1, pueblo2);
		boolean[] columna = fila[min];
		return columna[Math.max(pueblo1, pueblo2) - min];
	}
	
	public int dimension() 
	{
		return dimension;
	}
	
	public String toString() 
	{
		StringBuffer ret = new StringBuffer("");

		for(int p1=0; p1 < this.dimension(); p1++) {
			ret.append("[ ");
			for(int p2=0; p2 < this.dimension(); p2++)
				 ret.append(this.obtenerValor(p1, p2) ? "1 " : "0 ");
			ret.append("]\n");
		};

		return ret.toString();
	}

}
