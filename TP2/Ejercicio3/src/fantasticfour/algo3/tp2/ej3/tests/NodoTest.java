/**
 * 
 */
package fantasticfour.algo3.tp2.ej3.tests;


import static org.junit.Assert.*;

import org.junit.Test;

import fantasticfour.algo3.tp2.ej3.Nodo;

/**
 * @author bit-man
 *
 */
public class NodoTest {

	private static final String	VALOR_DEFAULT	= "primero";
	private static final String	SEGUNDO	= "segundo";
	

	@Test
	public void nodoSimpleTest() {
		Nodo n = new Nodo( VALOR_DEFAULT );

		assertTrue( n.padre == null );
		assertTrue( noTieneHijos(n) );
		assertTrue( n.valor.equals(VALOR_DEFAULT) );
	}
	

	@Test
	public void nodoSimpleSoloInstrucTest() {
		Nodo n = new Nodo( VALOR_DEFAULT );

		assertTrue( n.instruc() == 1 );
		assertTrue( n.instruc() == 0 ); // no se ejecut� ninguna instrucci�n desde el �ltimo llamado a instruc() 
	}
	
	@Test
	public void nodoSimpleConPadreTest() {
		Nodo padre = new Nodo( VALOR_DEFAULT );
		Nodo hijo = new Nodo( SEGUNDO );
		
		hijo.padre = padre;

		assertTrue( hijo.padre == padre );
		assertTrue( noTieneHijos(hijo) );
		assertTrue( noTieneHijos(padre) );
		
		padre.actualizarHijo( hijo, hijo );
		
		assertTrue( noTieneHijos(hijo) );
		assertFalse( noTieneHijos(padre) );
		assertTrue( padre.cantHijos() == 1 );
		assertTrue( padre.siguienteNodo( SEGUNDO ) == hijo );
		
		padre.borrarHijo(hijo);
		assertTrue( padre.cantHijos() == 0 );
		assertTrue( noTieneHijos(padre) );
	}


	/**
	 * @param n : Nodo
	 * @return
	 */
	private boolean noTieneHijos(Nodo n) {
		return n.cantHijos() == 0;
	}
}
