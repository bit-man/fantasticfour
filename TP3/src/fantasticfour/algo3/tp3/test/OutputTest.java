/**
 * 
 */
package fantasticfour.algo3.tp3.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import org.junit.Test;

import fantasticfour.algo3.tp3.FileFoundException;
import fantasticfour.algo3.tp3.Nodo;
import fantasticfour.algo3.tp3.Output;


/**
 * @author bit-man
 *
 */
public class OutputTest {

	/**
	 * 
	 */
	private static final String	FORMATO_TEST_MAS_COMPLEJO_1	= "1\n2 3 4\n3 5 6 7\n";
	/**
	 * 
	 */
	private static final String	FORMATO_TEST_NOMBRE	= "1\n0\n0\n";
	private static final String	EXTENSION_OUT	= ".out";
	private static final String	PREFIJO_TP3	= "/tmp/TP3";
	Output o;

	@Test
	public void testNombre() throws FileFoundException, IOException {
		File f = borrarArchivo(1);
		
		o = new Output(PREFIJO_TP3, 1);
		assertTrue( o.getNombreArchivo().equals(f.getAbsolutePath()));
		o.setKID(1);
		o.setIV1( new LinkedList<Nodo>() );
		o.setIV2( new LinkedList<Nodo>() );
		o.grabar();
		o.cerrar();
		
		assertTrue( tieneElFormatoCorrecto( FORMATO_TEST_NOMBRE ) );
		
		// Quiero generar un archivo que ya existe, debe dar error
		boolean dioExcepcion;
		try {
			o = new Output(PREFIJO_TP3, 1);
			dioExcepcion = false;
		} catch(FileFoundException e) {
			dioExcepcion = true;
		}
		assertTrue(dioExcepcion);
	}


	/**
	 * Borrar el archivo con el nro. de ejercicio pasado como parámetro
	 */
	private File borrarArchivo(int ejercicio) {
		File f = new File(PREFIJO_TP3 + ejercicio + EXTENSION_OUT);
		if(f.exists())
			f.delete();
		return f;
	}


	@Test
	public void testMasComplejo() throws FileFoundException, IOException {
		borrarArchivo(2);
		o = new Output(PREFIJO_TP3, 2);
		o.setKID(1);

		LinkedList<Nodo> n1 = new LinkedList<Nodo>();
		n1.add( new Nodo(3) );
		n1.add( new Nodo(4) );
		o.setIV1( n1 );

		LinkedList<Nodo> n2 = new LinkedList<Nodo>();
		n2.add( new Nodo(5) );
		n2.add( new Nodo(6) );
		n2.add( new Nodo(7) );
		o.setIV2( n2 );
		o.grabar();

		assertTrue( tieneElFormatoCorrecto( FORMATO_TEST_MAS_COMPLEJO_1 ) );
		
		o.nuevosResultados();
		o.setKID(1);
		o.setIV1( n1 );
		o.setIV2( n2 );
		o.grabar();

		assertTrue( tieneElFormatoCorrecto( FORMATO_TEST_MAS_COMPLEJO_1 
										  + FORMATO_TEST_MAS_COMPLEJO_1 ) );
		o.cerrar();
	}
	/**
	 * @param formatoTestArchivo
	 * @return
	 */
	private boolean tieneElFormatoCorrecto(String formato) {
		return( o.toString().equals(formato) );
	}

}
