package fantasticfour.algo3.tp1.ej1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Ej1Output 
{
	private FileWriter outputFile;
	private FileWriter statisticsFile;
	private String outputFileName;	

	public Ej1Output(File outputFile) 
		throws IOException 
	{
		this.outputFileName = outputFile.getAbsolutePath();
		this.outputFile = new FileWriter(outputFile);
		this.statisticsFile = new FileWriter(new File(outputFileName + ".op"));
	}

	public void close()
	{
		try {
			if (outputFile != null) {
				outputFile.close();
			}
		} catch (IOException e) {
			printCloseError(outputFileName);
		}
		
		try {
			if (statisticsFile != null) {
				statisticsFile.close();
			}
		} catch (IOException e2) {
			printCloseError(outputFileName + ".op");				
		}
	}

	private void printCloseError(String outputFileName) 
	{
		System.out.println("No se puede cerrar el archivo " + outputFileName);
	}

	public void print(Ej1Solver.SolverResult result) 
	{
		try {
			outputFile.write(result.getNumber() + " " + result.getMaxPrime() + " " + result.getMaxPot() + "\n");
			statisticsFile.write(result.getNumber() + " " + result.getQtyOps() + "\n");
		} catch (IOException e) {
			// TODO: Handle this exception properly.
			e.printStackTrace();
		}
	}	
	
}
