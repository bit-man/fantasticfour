/**
 * 
 */
package fantasticfour.algo3.tp3.test;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp3.ej2.GenerarNodosEnLinea;
import fantasticfour.algo3.tp3.Nodo;


/**
 * @author bit-man
 *
 */
public class GenerarNodosEnLineaTest {

	private GenerarNodosEnLinea n;
	private LinkedList<LinkedList<Nodo>> combinaciones;
	
	@Before
	public void setup() {
		this.combinaciones = new LinkedList<LinkedList<Nodo>>();
	}

	@Test
	public void testMinimo() {
		List<Nodo> nodosV = new LinkedList<Nodo>();
		nodosV.add( new Nodo(2) );
		List<Nodo> nodosIV = new LinkedList<Nodo>();
		nodosIV.add( new Nodo(3) );
		n = new GenerarNodosEnLinea(nodosV, nodosIV);

		assertTrue( n.hayMasLineas() );
		List<Nodo> linea = n.otraLinea();
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.get(1).equals(new Nodo(2)) );
		assertTrue( linea.get(0).equals(new Nodo(3)) );
		assertTrue( linea.size() == 2 );
		
		assertTrue( n.hayMasLineas() );
		linea = n.otraLinea();
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.get(1).equals(new Nodo(3)) );
		assertTrue( linea.get(0).equals(new Nodo(2)) );
		assertTrue( linea.size() == 2 );
		
		assertFalse( n.hayMasLineas() );
	}

	/**
	 * @param nodosV
	 * @param linea
	 * @return
	 */
	private boolean relacionEntreListasCorrecta( List<Nodo> nodosV,
												 List<Nodo> nodosIV,
												 List<Nodo> linea) {
		assertTrue( nodosSinNull( linea ) );
		assertTrue( (nodosIV.size() + nodosV.size()) == linea.size() );
		
		return losNodosVtienenLaMismaPosicionRelativa(nodosV, linea);
	}

	/**
	 * @param nodosV
	 * @param linea
	 * @return
	 */
	private boolean losNodosVtienenLaMismaPosicionRelativa(List<Nodo> nodosV,
														   List<Nodo> linea) {
		int found = 0;
		Iterator<Nodo> lineaIt = linea.iterator();
		Iterator<Nodo> nodosVit = nodosV.iterator();
		while( nodosVit.hasNext() && lineaIt.hasNext() ) {
			Nodo nodoV = nodosVit.next();
			
			assertTrue( nodoV != null );
			assertTrue( lineaIt != null );
			
			boolean foundThis = false;
			while( lineaIt.hasNext() && ! foundThis ) {
				Nodo n = lineaIt.next();
				assertTrue ( n != null );
				assertTrue( n.equals(nodoV) || ! n.equals(nodoV) );
				n.equals(nodoV);
				foundThis = n.equals(nodoV);
			}
			
			if (foundThis)
				found++;
		}
		// Se llega a este punto cuando al menos uno de los dos iteradores llega al final
				
		
		return ( found == nodosV.size() );
	}

	/***
	 * NO debe haber ningún elemento null en la lista
	 * @param lista
	 * @return
	 */
	private boolean nodosSinNull(List<Nodo> lista) {
		for( Nodo e: lista)
			if ( e == null)
				return false;
		
		return true;
	}

	@Test
	public void test2V1IV() {
		List<Nodo> nodosV = new LinkedList<Nodo>();
		nodosV.add( new Nodo(2) );
		nodosV.add( new Nodo(4) );
		List<Nodo> nodosIV = new LinkedList<Nodo>();
		nodosIV.add( new Nodo(3) );
		n = new GenerarNodosEnLinea(nodosV, nodosIV);

		assertTrue( n.hayMasLineas() );
		LinkedList<Nodo> linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(3) ) );
		assertTrue( linea.get(1).equals( new Nodo(2) ) );
		assertTrue( linea.get(2).equals( new Nodo(4) ) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(2) ) );
		assertTrue( linea.get(1).equals( new Nodo(3) ) );
		assertTrue( linea.get(2).equals( new Nodo(4) ) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(2) ) );
		assertTrue( linea.get(1).equals( new Nodo(4) ) );
		assertTrue( linea.get(2).equals( new Nodo(3) ) );
		
		assertFalse( n.hayMasLineas() );
	}
	
	@Test
	public void test1V2IV() {
		List<Nodo> nodosV = new LinkedList<Nodo>();
		nodosV.add( new Nodo(2) );
		List<Nodo> nodosIV = new LinkedList<Nodo>();
		nodosIV.add( new Nodo(3) );
		nodosIV.add( new Nodo(4) );
		n = new GenerarNodosEnLinea(nodosV, nodosIV);

		assertTrue( n.hayMasLineas() );
		LinkedList<Nodo> linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(3) ) );
		assertTrue( linea.get(1).equals( new Nodo(4) ) );
		assertTrue( linea.get(2).equals( new Nodo(2) ) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(4) ) );
		assertTrue( linea.get(1).equals( new Nodo(3) ) );
		assertTrue( linea.get(2).equals( new Nodo(2) ) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(3) ) );
		assertTrue( linea.get(1).equals( new Nodo(2) ) );
		assertTrue( linea.get(2).equals( new Nodo(4) ) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(4) ) );
		assertTrue( linea.get(1).equals( new Nodo(2) ) );
		assertTrue( linea.get(2).equals( new Nodo(3) ) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(2) ) );
		assertTrue( linea.get(1).equals( new Nodo(3) ) );
		assertTrue( linea.get(2).equals( new Nodo(4) ) );

		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 3 );
		assertFalse( lineaRepetida(linea) );
		assertTrue( linea.get(0).equals( new Nodo(2) ) );
		assertTrue( linea.get(1).equals( new Nodo(4) ) );
		assertTrue( linea.get(2).equals( new Nodo(3) ) );
		
		assertFalse( n.hayMasLineas() );
	}

	@Test
	public void test6V1IV() {
		List<Nodo> nodosV = new LinkedList<Nodo>();
		nodosV.add( new Nodo(1) );
		nodosV.add( new Nodo(2) );
		nodosV.add( new Nodo(4) );
		nodosV.add( new Nodo(5) );
		nodosV.add( new Nodo(6) );
		nodosV.add( new Nodo(7) );
		List<Nodo> nodosIV = new LinkedList<Nodo>();
		nodosIV.add( new Nodo(3) );
		n = new GenerarNodosEnLinea(nodosV, nodosIV);

		assertTrue( n.hayMasLineas() );
		LinkedList<Nodo> linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 7 );
		assertFalse( lineaRepetida(linea) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 7 );
		assertFalse( lineaRepetida(linea) );
		
		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 7 );
		assertFalse( lineaRepetida(linea) );

		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 7 );
		assertFalse( lineaRepetida(linea) );

		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 7 );
		assertFalse( lineaRepetida(linea) );

		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 7 );
		assertFalse( lineaRepetida(linea) );

		assertTrue( n.hayMasLineas() );
		linea = new LinkedList<Nodo>( n.otraLinea() );
		assertTrue( relacionEntreListasCorrecta( nodosV, nodosIV, linea ) );
		assertTrue( linea.size() == 7 );
		assertFalse( lineaRepetida(linea) );
		
		assertFalse( n.hayMasLineas() );
	}
	
	/**
	 * @param linea
	 * @return
	 */
	private boolean lineaRepetida(LinkedList<Nodo> linea) {
		boolean repetida = false;
		for( List<Nodo> e : this.combinaciones)
			repetida |= e.equals(linea);
		
		if (!repetida)
			this.combinaciones.add(linea);
		
		return repetida;
	}

}
