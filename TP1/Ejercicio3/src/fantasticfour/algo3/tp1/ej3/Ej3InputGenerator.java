package fantasticfour.algo3.tp1.ej3;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 */

/**
 * @author bit-man
 *
 */
public class Ej3InputGenerator 
{
	public static void main(String args[]) 
		throws IOException 
	{
		FileWriter writer = new FileWriter(new File(args[0]));
		int size = Integer.valueOf(args[1]);
		
		for (int i=1; i < size; i++) {
			writer.write(i + "\n");
			for (int j=0; j<i/2; j++) {
				writer.write("1 ");
			}
			for(int j=i/2 +1; j<i ; j++) {
				writer.write("2 ");
			}
			writer.write("1\n");
		}
		writer.write("0\n");
		
		writer.close();
	}
}
