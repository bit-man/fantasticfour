/**
 * 
 */
package fantasticfour.algo3.tp1.ej2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * @author bit-man
 *
 * Graba en un archivo los resultados de las instancias resueltas del Ejercicio 2, TP1
 */
public class EscribirResultados {
	
	private PrintWriter pw = null;
	
	private int valorLista = 0;
	private int cantElementos = 0;
	private int[] indice = null;
	
	public EscribirResultados( String archivo ) throws FileNotFoundException {
		pw = new PrintWriter( 
				new File( archivo ) 
			);
		limpiarValores();
	};
	
	public void valorLista( int valor ) {
		valorLista = valor;
	};
	
	public void cantElementos( int valor ) {
		cantElementos = valor;
	};
	
	public void indices( int[] valor ) {
		indice = valor;
	};
	
	public void nuevosResultados() {
		limpiarValores();
	};
	
	public void grabar() {
		pw.printf("%d %d", valorLista, cantElementos);
		
		for( int i = 0; i < cantElementos; i++)
			pw.printf(" %d", indice[i]);
		
		pw.println();
	};
	
	public void cerrar() {
		pw.close();
	}
	
	private void limpiarValores() {
		valorLista = 0;
		cantElementos = 0;
		indice = null;
	};
}
