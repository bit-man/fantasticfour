/**
 * 
 */
package fantasticfour.algo3.tp2.lib.test;

import static org.junit.Assert.*;

import org.junit.Test;

import fantasticfour.algo3.tp2.lib.StringUtil;

/**
 * @author bit-man
 *
 */
public class StringUtilTest {

	private static final String	ABCD	= "abcd";
	private static final String	ABC		= "abc";
	private static final String	ABCE	= "abce";
	private static final String	WXYZ	= "wxyz";
	private static final String	VACIO	= "";

	/**
	 * Test method for {@link fantasticfour.algo3.tp2.lib.StringUtil#indicePrimerCharDistinto(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testIndicePrimerCharDistinto() {
		assertTrue( StringUtil.indicePrimerCharDistinto( ABCD, ABCE) == 3);
		assertTrue( StringUtil.indicePrimerCharDistinto( ABC,  ABCE) == 3);

		assertTrue( StringUtil.indicePrimerCharDistinto( WXYZ, ABC) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( WXYZ, ABCD) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( WXYZ, ABCE) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( ABC,  WXYZ) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( ABCD, WXYZ) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( ABCE, WXYZ) == 0);

		assertTrue( StringUtil.indicePrimerCharDistinto( VACIO, ABC) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( VACIO, ABCD) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( VACIO, ABCE) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( ABC,  VACIO) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( ABCD, VACIO) == 0);
		assertTrue( StringUtil.indicePrimerCharDistinto( ABCE, VACIO) == 0);
	}

}
