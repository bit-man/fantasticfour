package fantasticfour.algo3.tp2.ej1;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

public class Grafo 
{
	private Integer cantNodos;
	private HashMap<Integer, LinkedList<Integer>> listaAdyacencia;
	
	private int nump;
	private int cantidadDeOperaciones;
	
	private Vector<Grafo> componentesFuertementeConexas;
	private int[] vectorDePertenencia;

	public Grafo() 
	{
		this.listaAdyacencia = new HashMap<Integer, LinkedList<Integer>>();
		this.cantNodos = 0;
	}
	
	public void agregar(int i, int j)
	{
		// (i, j) no debe existir en el grafo, no se hacen chequeos.
		// Tomar la lista i del hashMap = O(1)
		// Agregar el nodo adelante de la lista = O(1)
		// En consecuencia, agregar es O(1).
		if (this.tieneNodo(i)) {
			this.getListaAdyacencia().get(i).addFirst(j);
			agregarOps(2);
		} else {
			LinkedList<Integer> list = new LinkedList<Integer>();
			list.addFirst(j);
			this.getListaAdyacencia().put(i, list);
			cantNodos++;
			agregarOps(4);	
		}
	}

	private void agregarOps(int i) 
	{
		this.cantidadDeOperaciones += i;
	}

	public void agregar(int nodo)
	{
		// nodo es un nodo solitario.
		// Tomar la lista i del hashMap = O(1)
		// Agregar el nodo adelante de la lista = O(1)
		// En consecuencia, agregar es O(1).
		agregarOps(2);
		if (this.getListaAdyacencia().get(nodo) == null) {
			this.getListaAdyacencia().put(nodo, new LinkedList<Integer>());
			cantNodos++;
			agregarOps(3);
		}
	}	
	
	public LinkedList<Integer> getEjesDe(int nodo)
	{
		agregarOps(1);
		return this.getListaAdyacencia().get(nodo);
	}
	
	public Grafo obtenerTraspuesto()
	{
		// Este obtiene el traspuesto en orden O(n+m) 
		// donde n = cant de nodos y m = cant de ejes
		Grafo traspuesto = new Grafo();

		Set<Integer> nodos = this.getListaAdyacencia().keySet(); // O(n)		
		for (int i : nodos) { // n veces

			LinkedList<Integer> linkedList = this.getListaAdyacencia().get(i);
			agregarOps(3);
			if (linkedList.size() == 0) {
				traspuesto.agregar(i);
				agregarOps(1);
			}
			for (int j : linkedList) { // m veces				
				traspuesto.agregar(j, i);
				agregarOps(3);
				if(!traspuesto.tieneNodo(i)) {
					traspuesto.agregar(i);
					agregarOps(1);
				}
				
			}
		}		
		return traspuesto;
	}
	
	private void separarComponentesFuertementeConexas()
	{
		cantidadDeOperaciones = 0;	// Init.

		// Utiliza el algoritmo de Kosaraju-Shahir. (O(n+m))		
		int[] postNum = this.dfs();		
//		System.out.print("\nPostNum = [");
//		for (int i = 0; i < postNum.length; i++) {
//			System.out.print(postNum[i] + ", ");
//		}
//		System.out.print("]\n");
		Grafo traspuesto = this.obtenerTraspuesto();
		componentesFuertementeConexas = traspuesto.dfs2(postNum);
		vectorDePertenencia = traspuesto.getVectorDePertenencia();
		agregarOps(4);	// Asignaciones.
	}
	
	private int[] dfs()
	{
		// Se inicializa el arreglo de visitados en false
		boolean[] fueVisitado = new boolean[this.getCantNodos()];
		int[] postNum = new int[this.getCantNodos()];
		this.nump = 0;
		agregarOps(3);
		
		agregarOps(2);
		for (int nodo = 1; nodo <= this.getCantNodos() ; nodo++) {
			agregarOps(3);
			if (!fueVisitado[nodo - 1]) {
				dfsSobreNodo(nodo, fueVisitado, postNum);
			}
			agregarOps(3);	// ++ y comparación de la guarda.
		}
		
		return postNum;
	}
	
	private void dfsSobreNodo(int nodo, boolean[] fueVisitado, int[] postNum)
	{
		fueVisitado[nodo - 1] = true;
		
		LinkedList<Integer> adyacentesNodo = this.getListaAdyacencia().get(nodo);
		
		agregarOps(5);	// Asignaciones, resta y guarda.
		
		if (adyacentesNodo != null) {
			agregarOps(2);
			for(int adyacenteANodo : adyacentesNodo) {
				agregarOps(2);
				if (!fueVisitado[adyacenteANodo - 1]) {
					dfsSobreNodo(adyacenteANodo, fueVisitado, postNum);
				}
				agregarOps(3);
			}
		}
		
		postNum[nodo - 1] = ++nump;
		agregarOps(4);
	}
	
	private Vector<Grafo> dfs2(int[] postNum)
	{
		Vector<Grafo> bosque = new Vector<Grafo>();
		
		int nodo = mayorNodoDe(postNum); // O(n)
		int nroCompConexa = 0;
		this.vectorDePertenencia = new int[this.getCantNodos()];
		agregarOps(6);
		while (postNum[nodo - 1] != NODO_YA_VISITADO) {
			Grafo subGrafoActual = new Grafo();
			dfsSobreNodo2(nodo, postNum, subGrafoActual, nroCompConexa);
			bosque.add(subGrafoActual);			
			nroCompConexa++;
			nodo = mayorNodoDe(postNum);
			
			agregarOps(bosque.size() + 6);
		}
		
		return bosque;
	}

	private void dfsSobreNodo2(int nodo, int[] postNum, Grafo grafo, int nroCompConexa)
	{
		postNum[nodo - 1] = NODO_YA_VISITADO;
		
		LinkedList<Integer> adyacentesNodo = this.getListaAdyacencia().get(nodo);
		
		agregarOps(4);
		if (adyacentesNodo != null) {
			agregarOps(2);
			for(int adyacenteANodo : adyacentesNodo) {
				agregarOps(2);
				if (postNum[adyacenteANodo - 1] != NODO_YA_VISITADO) {
					grafo.agregar(nodo, adyacenteANodo);
					
					this.vectorDePertenencia[nodo - 1] = nroCompConexa;					
					
					dfsSobreNodo2(adyacenteANodo, postNum, grafo, nroCompConexa);

					agregarOps(2);
				}
				agregarOps(3);
			}
		}
		
		agregarOps(1);
		if (!grafo.tieneNodo(nodo)) {
			grafo.agregar(nodo);
			this.vectorDePertenencia[nodo - 1] = nroCompConexa;
			agregarOps(2);
		}
	}

	/*
	 * Verifica si el grafo tiene un nodo determinado
	 * Complejidad: O(1)
	 */
	public boolean tieneNodo(int nodo) 
	{
		agregarOps(2);
		return this.getListaAdyacencia().get(nodo) != null;
	}

	/*
	 * Busca el nodo de valor máximo del arreglo postNum.
	 * Complejidad: O(n)
	 */
	private int mayorNodoDe(int[] postNum) 
	{
		int maxVal = 0; 
		int maxPos = 0;

		agregarOps(4);
		for (int i = 0; i < postNum.length; i++) {
			agregarOps(1);
			if (maxVal < postNum[i]) {
				maxVal = postNum[i];
				maxPos = i;
				agregarOps(2);
			}
			agregarOps(3);
		}

		agregarOps(1);
		return maxPos + 1;	// Ajuste de posición.
	}

	public int getCantNodos() 
	{
		return cantNodos;
	}

	public HashMap<Integer, LinkedList<Integer>> getListaAdyacencia() 
	{
		return listaAdyacencia;
	}

	public String toString()
	{
		String output = "Grafo de " + this.getCantNodos() + " nodos: ";
		for (int i : this.getListaAdyacencia().keySet()) {
			LinkedList<Integer> linkedList = this.getListaAdyacencia().get(i);
			if (linkedList.size() > 0) {
				for (int j : linkedList) {
					output += "[" + i + ", " + j + "] ";
				}
			} else {
				output += "[" + i + "] ";
			}
		}
		return output;
	}

	/*
	 * Devuelve las SCC de este grafo.
	 */
	public Vector<Grafo> getComponentesFuertementeConexas() 
	{
		this.separarComponentesFuertementeConexas();
		return componentesFuertementeConexas;
	}

	/*
	 * Devuelve el vector de Pertenecia de este grafo.
	 * Para cada nodo (i.e. posición del arreglo) posee el número de SCC a la que pertenece el mismo.
	 * Por ejemplo vectorDePertenencia[3] = a que SCC pertenece el nodo 3.
	 * NOTA: Esto devuelve un valor != null solo si es llamado despues de getComponentesFuertementeConexas()
	 */
	public int[] getVectorDePertenencia() 
	{
		return vectorDePertenencia;
	}
	
	public int getCantidadDeOperaciones()
	{
		return cantidadDeOperaciones;
	}
	
	private static int NODO_YA_VISITADO = -1;
}
