package fantasticfour.algo3.tp2.ej2;

import java.io.File;

import fantasticfour.algo3.tp2.ej2.Ej2Input;
import fantasticfour.algo3.tp2.ej2.Ej2Output;
import fantasticfour.algo3.tp2.ej2.Ej2Solver;

public class Ejercicio2 {
	public static void main(String[] args)
	{
		if (args.length != 2) {
			printHeader();
			System.out.println("Error: Forma de uso: Ejercicio3 <archInput.in> <archOutput.out>");
			System.exit(1);
		}
		
		File inputFile = new File(args[0]);
		
		if (!inputFile.isFile()) {
			printHeader();
			System.out.println("Error: El archivo " + args[0] + " no existe o es un directorio");
			System.exit(1);
		}
		
		File outputFile = new File(args[1]);

		Ej2Input ej2Input = null;
		Ej2Output ej2Output = null;
		try {
			ej2Input = new Ej2Input(inputFile); 
			ej2Output = new Ej2Output(outputFile);
			Ej2Solver solver = new Ej2Solver(ej2Input, ej2Output);
			
			solver.solve();
			
		} catch (Exception e) {
			printHeader();
			System.out.println("Ocurrió un error durante la ejecución.");
			e.printStackTrace();
			System.exit(1);
		} finally {
			if (ej2Input != null) {
				ej2Input.cerrar();
			}
			
			if (ej2Output != null) {
				ej2Output.cerrar();
			}
		}
	}

	private static void printHeader() 
	{
		System.out.println("Algoritmos III - TP2 - Ej2.");
		System.out.println("Grupo XXX");
	}


}
