/**
 * 
 */
package fantasticfour.algo3.tp1.ej2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

/**
 * @author bit-man
 *
 */
public class Backtracking {

	private int[] peso = null;
	private int[] valor = null;
	private int P = 0; 
	
	private boolean resuelto = false;
	
	private Stack<Integer> solucion = null;
	private int valorSolucion = 0;
	private Stack<Integer> solParcial = null;
	private int pesoSolParcial = 0;
	private int valorSolParcial = 0;
	private LinkedList<Integer> disponible = null;
	
	private long nInstruc = 0;
	
	public Backtracking( int P, int[] pesos, int[] valores ) {
		assert( pesos.length == valores.length);
		
		this.P = P;
		this.peso = pesos;
		this.valor = valores;
		
		resuelto = false;
		solucion = new Stack<Integer>();
		valorSolucion = 0;
		
		solParcial = new Stack<Integer>();
		pesoSolParcial = 0;
		valorSolParcial = 0;
		
		disponible = new LinkedList<Integer>();
		disponibilizarItemsDesdeHasta( 0, valores.length - 1 );
		
		nInstruc = 0;
	}
	
	/***
	 * Resuelve usando backtracking la instancia indicada en el constructor
	 */
	public void resolver() {
		assert( ! resuelto );
		resuelto = true;
		resolver( copiar(disponible) );
	};
	
	private void resolver( LinkedList<Integer> disponible ) {
		
		boolean soyElUltimoItem = true;

		nInstruc += 2; // tama�o de la lista + comparaci�n del while()
		while( disponible.size() != 0 ) {
			int actual = disponible.removeFirst();
			nInstruc += 1;  // borrar el primero
			if( esKPrometedor(actual, pesoSolParcial ) ) { // 3
				soyElUltimoItem = false;

				solParcial.push(actual);
				pesoSolParcial += peso[actual];
				valorSolParcial += valor[actual];
				nInstruc += 5;  // push() + suma * 2 + acceso array * 2

				resolver( copiar(disponible) ); // 1 de cada iteraci�n

				solParcial.pop();
				pesoSolParcial -= peso[actual];
				valorSolParcial -= valor[actual];
				nInstruc += 5;  //  pop() + resta * 2 + acceso array * 2 
			}
			nInstruc += 2;  // size() + comparaci�n del while()
		}

		if ( soyElUltimoItem )
			if ( esMejorLaSolucionParcial() ) { // 1
				solucion = copiar( solParcial ); // 1 de cada iteraci�n
				valorSolucion =  valorSolParcial;
			}
		
	}
	
	/***
	 * @param elem
	 * @return Indica si el agregado del elemento actual a la soluci�n actual
	 * 		   es una soluci�n viable
	 */
	private boolean esKPrometedor( int elem, int pesoSolParcial ) {
		nInstruc += 3 ;  // suma + acceso array + comparacion <=
		return ( pesoSolParcial + peso[elem] <= P );
	}
	
	/***
	 * 
	 * @return
	 */
	private boolean esMejorLaSolucionParcial() {
		nInstruc += 1; // comparaci�n
		return ( valorSolParcial > valorSolucion );
	}
	
	/***
	 * Devuelve la soluci�n encontrada luego de ejecutar resolver
	 */
	public int[] solucion() {
		assert(resuelto);

		int[] ret = new int[ solucion.size() ];
		
		Iterator<Integer> it = solucion.iterator();
		for ( int i = 0; it.hasNext(); i++ )
			ret[i] = it.next() + 1; 
		    // los arrays de Java son base 0, pero en el problema la base es '1'
		
		return ret;
	}
	

	
	/***
	 * Devuelve una copia de la LinkedList<Integer> pasada como par�metro
	 */
	private LinkedList<Integer> copiar( LinkedList<Integer> a ) {
		nInstruc += a.size();
		return new LinkedList<Integer>(a);
	}
	
	/***
	 * Devuelve una copia de la Stack<Integer> pasada como par�metro
	 */
	private Stack<Integer> copiar( Stack<Integer> s ) {
		Stack<Integer> ns = new Stack<Integer>();
		nInstruc++;
		
		Iterator<Integer> it = s.iterator();
		nInstruc ++; // generaci�n del iterador

		nInstruc ++; // hasNext() del while
		while( it.hasNext() ) {
			ns.push( it.next() );
			nInstruc += 2; // next() + push
			nInstruc ++; // hasNext() del while
		}

		return ns; 
	}
	
	
	/***
	 * Agrega a la lista de disponible todos los items que est�n ubicados 
	 * entre los valores 'desde' y 'hasta' inclusive. 
	 */
	private void disponibilizarItemsDesdeHasta( int desde, int hasta ) {

		for( int i = desde; i <= hasta; i++)
			disponible.addLast( i );

	}

	/**
	 * @return Devuelve la cantidad de instrucciones
	 */
	public long nInstruc() {
		return nInstruc;
	}

}
