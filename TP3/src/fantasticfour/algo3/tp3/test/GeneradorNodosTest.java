/**
 *
 */
package fantasticfour.algo3.tp3.test;

import java.util.LinkedList;
import java.util.List;

import fantasticfour.algo3.tp3.ej2.GeneradorNodos;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


/**
 * @author bit-man
 *
 */
public class GeneradorNodosTest
{
	private List<String> l;
	
	@Before
	public void setup() {
		l = new LinkedList<String>();
	}

    /**
     * Test method for {@link fantasticfour.algo3.tp3.ej2.GeneradorNodos#reset()}.
     * @throws Exception
     */
    @Test
    public void testReset() throws Exception
    {
        GeneradorNodos g = new GeneradorNodos(4);
        g.otroGrafo();
        g.reset();
        testReset(g);
    }

    private void testReset(GeneradorNodos g) throws Exception
    {
        for (int i = 0; i < g.getDigitos(); i++)
            assertTrue(g.getDigito(i) == i);
    }

    /**
     * Test method for {@link fantasticfour.algo3.tp3.ej2.GeneradorNodos#GeneradorGrafos(int)}.
     * @throws Exception
     */
    @Test
    public void testGeneradorGrafos() throws Exception
    {
        GeneradorNodos g = new GeneradorNodos(2);
        testReset(g);
    }

    /**
     * @throws Exception *
     *
     */
    @Test
    public void testIncSinOverflow() throws Exception
    {
        GeneradorNodos g = new GeneradorNodos(4);
        assertTrue( noSeRepiteLaSecuencia(g) );
        testSinOverflow4Digitos(g);
    }

    /**
     * @param g
     * @throws Exception
     */
    private void testSinOverflow4Digitos(GeneradorNodos g)
        throws Exception
    {
        g.otroGrafo();
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 2);
        assertTrue(g.getDigito(3) == 3);
        assertTrue( noSeRepiteLaSecuencia(g) );
        

        g.otroGrafo();
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 1);
        assertTrue(g.getDigito(3) == 3);
        assertTrue( noSeRepiteLaSecuencia(g) );

        g.otroGrafo();
        assertTrue(g.getDigito(0) == 3);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 1);
        assertTrue(g.getDigito(3) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );
    }

    @Test
    public void testIncConOverflow2Digitos() throws Exception
    {
        GeneradorNodos g = new GeneradorNodos(2);
        
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );

        g.otroGrafo();
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );

        // Sólo detecto la condición de overflow una vez que incremento
        // el contador, y no antes
        g.otroGrafo();
        assertFalse( g.hasyMasGrafos() );
        /***
         * En realidad no me importa si se repite o no la secuencia porque a
         * partir de que hay overflow (o no hay más grafos) es inválido querer
         * seguir extrayendo grafos, sólo una nota de color
         * (si más adelante falla el testing, sacar este assert o mejor analizar
         * cómo quedó la secuencia para ver si vuelve al estado inicial :-P )
         */
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 1);
        assertFalse( noSeRepiteLaSecuencia(g) );
    }
    

    @Test
    public void testIncConOverflow3Digitos() throws Exception
    {
        GeneradorNodos g = new GeneradorNodos(3);
        
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );
        
        g.otroGrafo();
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );
        
        g.otroGrafo();
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );
        
        g.otroGrafo();
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );
        
        g.otroGrafo();
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );
        
        g.otroGrafo();
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );
        
        g.otroGrafo();
        assertFalse( g.hasyMasGrafos() );
        /***
         * En realidad no me importa si se repite o no la secuencia porque a
         * partir de que hay overflow (o no hay más grafos) es inválido querer
         * seguir extrayendo grafos, lo que pasa que como internamente es un contador
         * todas las posibilidades se agotan al hacer overflow !!
         * (si más adelante falla el testing, sacar este assert o mejor analizar
         * cómo quedó la secuencia para ver si vuelve al estado inicial :-P )
         */
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 2);
        assertFalse( noSeRepiteLaSecuencia(g) );
    }
    
    @Test
    public void testIncConOverflow() throws Exception
    {
        GeneradorNodos g = new GeneradorNodos(4);
        assertTrue( noSeRepiteLaSecuencia(g) );
        testSinOverflow4Digitos(g);

        // Primer overflow
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 1);
        assertTrue(g.getDigito(3) == 3);
        assertTrue( noSeRepiteLaSecuencia(g) );

        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 0);
        assertTrue(g.getDigito(3) == 3);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 0);
        assertTrue(g.getDigito(3) == 3);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 3);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 0);
        assertTrue(g.getDigito(3) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        // Segundo overflow
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 3);
        assertTrue(g.getDigito(2) == 1);
        assertTrue(g.getDigito(3) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );

        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 3);
        assertTrue(g.getDigito(2) == 0);
        assertTrue(g.getDigito(3) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );

        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 3);
        assertTrue(g.getDigito(2) == 0);
        assertTrue(g.getDigito(3) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );

        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 3);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 0);
        assertTrue(g.getDigito(3) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        // Tercer overflow
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 3);
        assertTrue(g.getDigito(3) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 3);
        assertTrue(g.getDigito(3) == 2);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 3);
        assertTrue(g.getDigito(3) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 3);
        assertTrue(g.getDigito(1) == 0);
        assertTrue(g.getDigito(2) == 2);
        assertTrue(g.getDigito(3) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        // Cuarto overflow
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 3);
        assertTrue(g.getDigito(3) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 3);
        assertTrue(g.getDigito(3) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 3);
        assertTrue(g.getDigito(3) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 3);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 2);
        assertTrue(g.getDigito(3) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );

        // Quinto overflow
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 3);
        assertTrue(g.getDigito(2) == 2);
        assertTrue(g.getDigito(3) == 1);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 1);
        assertTrue(g.getDigito(1) == 3);
        assertTrue(g.getDigito(2) == 2);
        assertTrue(g.getDigito(3) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        
        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 2);
        assertTrue(g.getDigito(1) == 3);
        assertTrue(g.getDigito(2) == 1);
        assertTrue(g.getDigito(3) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );

        g.otroGrafo();
        assertTrue( g.hasyMasGrafos() );
        assertTrue(g.getDigito(0) == 3);
        assertTrue(g.getDigito(1) == 2);
        assertTrue(g.getDigito(2) == 1);
        assertTrue(g.getDigito(3) == 0);
        assertTrue( noSeRepiteLaSecuencia(g) );
        assertTrue( g.hasyMasGrafos() );
        
        g.otroGrafo();
        assertFalse( g.hasyMasGrafos() );
        /***
         * En realidad no me importa si se repite o no la secuencia porque a
         * partir de que hay overflow (o no hay más grafos) es inválido querer
         * seguir extrayendo grafos, lo que pasa que como internamente es un contador
         * todas las posibilidades se agotan al hacer overflow !!
         * (si más adelante falla el testing, sacar este assert o mejor analizar
         * cómo quedó la secuencia para ver si vuelve al estado inicial :-P )
         */
        assertTrue(g.getDigito(0) == 0);
        assertTrue(g.getDigito(1) == 1);
        assertTrue(g.getDigito(2) == 2);
        assertTrue(g.getDigito(3) == 3);
        assertFalse( noSeRepiteLaSecuencia(g) );
    }
    

	/**
	 * @param string
	 * @return
	 * @throws Exception 
	 */
	private boolean noSeRepiteLaSecuencia(GeneradorNodos g) throws Exception {
		String ultimaSecuencia = new String( "" );
		for(int i=0; i < g.getDigitos(); i++)
			ultimaSecuencia += g.getDigito(i);
		
		boolean ret = existeEnLaLista( ultimaSecuencia );
		l.add( ultimaSecuencia );
		return !ret;
	}

	/**
	 * @param ultimaSecuencia
	 * @return
	 */
	private boolean existeEnLaLista(String ultimaSecuencia) {
		boolean ret = false;
		
		for( String estaSecuencia : l )
			if ( estaSecuencia.equals(ultimaSecuencia)) {
				ret = true;
				break;
			}
		
		return ret;
	}
    
}
