package fantasticfour.algo3.tp3.ej4;

import java.util.LinkedList;

import fantasticfour.algo3.tp3.Grafo;
import fantasticfour.algo3.tp3.Instancia;
import fantasticfour.algo3.tp3.Nodo;
import fantasticfour.algo3.tp3.ej3.Ej3Solver;

public class Ej4Solver 
{	
	private Instancia instancia;
	private int mejorCantCruces = Integer.MAX_VALUE;
    private long cantOps;

    public Ej4Solver(Instancia instancia)
    {
        this.instancia = instancia;
        cantOps = 0;
    }

    protected void incrementarOperaciones(long cantidadDeOperaciones)
    {
        cantOps = cantOps + cantidadDeOperaciones;
    }

    public long getCantidadDeOperaciones()
    {
        return cantOps;
    }
	
	public Grafo solve(boolean ejecutarRandom)
	{
		Grafo grafoRet = resolverConBusquedaLocal(ejecutarRandom);
		grafoRet.setCantidadDeCrucesCalculada(mejorCantCruces);
		return grafoRet;
	}

	private Grafo resolverConBusquedaLocal(boolean ejecutarRandom) 
	{
        Grafo respuestaPivote;
        if (ejecutarRandom) {
            respuestaPivote = new GolosoRandom(this.instancia).resolverConGolosoRandom();
        } else {
            respuestaPivote = new Ej3Solver(this.instancia).solve();
        }

        Grafo mejorRespuesta = respuestaPivote.clone();
		
		mejorCantCruces = mejorRespuesta.cantidadCruces();
        incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
        int mejorCantCrucesAnterior = 0;
        
        incrementarOperaciones(2);
        boolean[] usados = new boolean[mejorRespuesta.dameNodos().size()];
        incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
        incrementarOperaciones(1);
		while(mejorCantCrucesAnterior != mejorCantCruces)
        {
			mejorCantCrucesAnterior = mejorCantCruces;
			
			Nodo nodo = mejorRespuesta.dameNodoConMayorCantCrucesDeV1(usados);
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
            incrementarOperaciones(1);
            if (nodo == null) {
				break;	// Todos los nodos estan usados...
			}
			
			LinkedList<Nodo> adyacentesANodo = respuestaPivote.dameAdyacentesA(nodo);
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            incrementarOperaciones(2);
            if(adyacentesANodo.isEmpty()) {
				// Si no tengo adyacentes, es un nodo solitario. No me modifica nada moverlo.
				continue;
			}

			// Ahora tomamos el nodo de menor grado entre los adyacentes...
			Nodo nodoAdy = mejorRespuesta.dameNodoDeEstosConMenorGrado(adyacentesANodo);
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());

            // Que pasa si toma siempre el mismo adyacente para distintos nodos
			// Me estaría cambiando la posición del mismo, que me perjudicaría la cant de nodos del anterior que moví.
			
			// Copio la lista de nodos de V1. (Así mantengo las pos. originales)
			int posOrigNodo = respuestaPivote.damePosEnV1De(nodo);
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());

            // Chequeo hacer los intercambios en V2 con la posición original del nodo de V1.
            int posOrigNodoAdy = respuestaPivote.damePosEnV2De(nodoAdy);
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            incrementarOperaciones(3);
            intercambiarEnV2(respuestaPivote, mejorRespuesta, nodoAdy);
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            respuestaPivote.restaurarPosDe(nodoAdy, respuestaPivote.damePosEnV2De(nodoAdy), posOrigNodoAdy, false);
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());

            while(respuestaPivote.sePuedeBajarEnV1(nodo)) {
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                posOrigNodoAdy = respuestaPivote.damePosEnV2De(nodoAdy);
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                respuestaPivote.bajarEnV1(nodo);
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                intercambiarEnV2(respuestaPivote, mejorRespuesta, nodoAdy);
                incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                respuestaPivote.restaurarPosDe(nodoAdy, respuestaPivote.damePosEnV2De(nodoAdy), posOrigNodoAdy, false);
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            }

			// Restauro posiciones previas a bajar nodo.
            respuestaPivote.restaurarPosDe(nodo, respuestaPivote.damePosEnV1De(nodo), posOrigNodo, true);
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());

            while(respuestaPivote.sePuedeSubirEnV1(nodo)) {
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                posOrigNodoAdy = respuestaPivote.damePosEnV2De(nodoAdy);
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                respuestaPivote.subirEnV1(nodo);
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                intercambiarEnV2(respuestaPivote, mejorRespuesta, nodoAdy);
                incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
                respuestaPivote.restaurarPosDe(nodoAdy, respuestaPivote.damePosEnV2De(nodoAdy), posOrigNodoAdy, false);
                incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            }
			
			// Piso la respPivote con la mejor respuesta que tengo.
			respuestaPivote.setPosicionesNodosV1(mejorRespuesta.dameNodosV1());
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            respuestaPivote.setPosicionesNodosV2(mejorRespuesta.dameNodosV2());
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
        }
        return mejorRespuesta;
	}

	private void intercambiarEnV2(Grafo respuestaPivote, Grafo mejorRespuesta, Nodo nodoAdy)
	{
		// Copio la lista de nodos de V2. (Así mantengo las pos. originales)
		//LinkedList<Nodo> nodosV2Originales = new LinkedList<Nodo>(respuestaPivote.dameNodosV2());
        int posOrig = respuestaPivote.damePosEnV2De(nodoAdy);
        incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
        incrementarOperaciones(1);
        chequearCruces(respuestaPivote, mejorRespuesta);
        incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
        incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
		
        while(respuestaPivote.sePuedeBajarEnV2(nodoAdy)) {
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            respuestaPivote.bajarEnV2(nodoAdy);
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            chequearCruces(respuestaPivote, mejorRespuesta);
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
        }

        // Restauro posiciones previas a bajar nodo.
		//respuestaPivote.setPosicionesNodosV2(nodosV2Originales);
        respuestaPivote.restaurarPosDe(nodoAdy, respuestaPivote.damePosEnV2De(nodoAdy), posOrig, false);
        incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());

        while(respuestaPivote.sePuedeSubirEnV2(nodoAdy)) {
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            respuestaPivote.subirEnV2(nodoAdy);
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            chequearCruces(respuestaPivote, mejorRespuesta);
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
        }
	}

	private void chequearCruces(Grafo respuestaPivote, Grafo mejorRespuesta) 
	{
		int cantCrucesAct = respuestaPivote.cantidadCruces();
        incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
        incrementarOperaciones(2);
        if(mejorCantCruces > cantCrucesAct) {
			mejorCantCruces = cantCrucesAct;
			mejorRespuesta.cambiarPosiciones(respuestaPivote);
            incrementarOperaciones(mejorRespuesta.getCantidadDeOperaciones());
            incrementarOperaciones(respuestaPivote.getCantidadDeOperaciones());
            incrementarOperaciones(1);
        }
	}
}
