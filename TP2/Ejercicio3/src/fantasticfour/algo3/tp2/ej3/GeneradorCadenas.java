/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import fantasticfour.algo3.tp2.lib.Constantes;
import fantasticfour.algo3.tp2.ej3.Const;

/**
 * @author bit-man
 *
 */
public class GeneradorCadenas {

	private static final String	CARACTER_DE_RELLENO	= "a";
	/**
	 * 
	 */
	private static PrintWriter archivo;
	private static int longCadena;
	private static int cantCadenas;
	private static int	cantBloquesCompartidos;

	/**
	 * Clase para generar un archivo de entrada que genera cadenas de la longitud
	 * y en la cantidad especificadas, con la cantidad de prefijos especificados
	 * que permite grabarlas, consultarlas, preguntar por el cardinal y luego
	 * borrarlas de un Radix Tree
	 *  
	 * @param args : - Nombre de archivo
	 * 				 - longitud de la cadena
	 * 				 - cant. de cadenas
	 * 				 - prefijos
	 */
	public static void main(String[] args) {
		if( args.length < 3 ) {
			ayuda();
			System.exit( Constantes.SALIR_CON_ERROR );
		}
		
		try {
			archivo = new PrintWriter( new File (args[0]) );
			String tipo = args[1];

			longCadena = Integer.parseInt(args[2]);

			if ( tipo.equals(Const.TIPO_COMPARTIDAS) && args.length > 3 ) {
				cantCadenas = Integer.parseInt(args[3]);
				cantBloquesCompartidos = Integer.parseInt(args[4]);
			}
			
			generarCadenas( tipo );
			
		} catch (FileNotFoundException e) {
			System.out.println("No puedo generar el archivo " + args[0]);
			System.out.println( e.getMessage() );
		} catch( Exception e ) {
			System.out.println("Se ha producido un error !!! :-(");
			System.out.println( e.getMessage() );
			e.printStackTrace();
		}
	}

	private static void generarCadenas(String tipo) throws Exception {

		if ( tipo.equals(Const.TIPO_UNICAS) )
			generarCadenasUnicas();
		else if ( tipo.equals(Const.TIPO_COMPARTIDAS) )
			generarCadenasCompartidas();
		else 
			throw new Exception("No existe el tipo de cadena " + tipo);
	}

	/**
	 * 
	 */
	private static void generarCadenasCompartidas() {
		int tamBloque = longCadena / cantBloquesCompartidos;
		String primerBloque = generarCadenaLong(tamBloque);
		String[] ret = new String[1];
		ret[0] = primerBloque;

		for( int bloque = 1; bloque < cantBloquesCompartidos; bloque++) {
			int dim = obtenerDimension();
			String[] charDistintos = 
				generarCadenaConCharsDistintos(dim, tamBloque);
			String[] aux = new String[ ret.length ];
			System.arraycopy(ret, 0, aux, 0, ret.length);
			ret = new String[ aux.length * charDistintos.length ];
			
			for( int i=0; i < aux.length; i++)
				for( int j=0; j < charDistintos.length; j++)
					ret[ (j * aux.length) + i ] = aux[i] + charDistintos[j];
					
			cantCadenas /= Constantes.CANT_MAX_LETRAS;
		}
		

		archivo.println(Const.CANT_COMANDOS * ret.length);
		
		for( int i=0; i < ret.length; i++) {
			grabaComandoAgregar( ret[i] );
			grabarComandoCardinal();
			grabarComandoPertence( ret[i] );
		}
		
		for( int i=0; i < ret.length; i++)
			grabarComandoSacar( ret[i] );
		

		archivo.println( Constantes.FIN_DE_ARCHIVO );
		archivo.close();

	}

	/**
	 * @return
	 */
	private static int obtenerDimension() {
		int ret;
		if ( cantCadenas < Constantes.CANT_MAX_LETRAS)
			ret = cantCadenas;
		else
			ret = Constantes.CANT_MAX_LETRAS;
		return ret;
	}


	/**
	 * @param charDist
	 * @return
	 */
	private static String[] generarCadenaConCharsDistintos(int charDist, int longitud) {
		assert( charDist <= Constantes.CANT_MAX_LETRAS);
		
		String pre = generarCadenaLong( longitud - 1 );
		String[] ret = new String[charDist];
		for( int i = 0; i < ret.length; i++)
			ret[i] = Constantes.todosLosChars[i] + pre;

		return ret;
	}

	/**
	 * @throws Exception 
	 * 
	 */
	private static void generarCadenasUnicas() {
		archivo.println(Const.CANT_COMANDOS * longCadena);
		
		for( int i = 1; i <= longCadena; i++) {
			String s = generarCadenaLong(i);
			grabarComandos(s);
		}

		archivo.println( Constantes.FIN_DE_ARCHIVO );
		archivo.close();
	}

	/**
	 * @param s : String que precede a los comandos
	 */
	private static void grabarComandos(String s) {
		grabaComandoAgregar(s);
		grabarComandoCardinal();
		grabarComandoPertence(s);
		grabarComandoSacar(s);
	}

	/**
	 * @param s
	 */
	private static void grabarComandoSacar(String s) {
		archivo.println( Const.COMANDO_SACAR + " " + s );
	}

	/**
	 * @param s
	 */
	private static void grabarComandoPertence(String s) {
		archivo.println( Const.COMANDO_PERTENECE +" " + s );
	}

	/**
	 * 
	 */
	private static void grabarComandoCardinal() {
		archivo.println( Const.COMANDO_CARDINAL );
	}

	/**
	 * @param s
	 */
	private static void grabaComandoAgregar(String s) {
		archivo.println( Const.COMANDO_AGREGAR + " " + s );
	}

	private static String generarCadenaLong(int longitud) {
		String s = new String("");
		
		for( int i = 0; i < longitud; i ++)
			s += CARACTER_DE_RELLENO;
		
		return s;
	}

	/**
	 * 
	 */
	private static void ayuda() {
		System.out.println("java fantasticfour.algo3.tp2.ej3.GeneradorCadenas\n"
					     + "	   archivo_de_salida long_cadena cant_cadenas segmentos");
		
	}
	

}
