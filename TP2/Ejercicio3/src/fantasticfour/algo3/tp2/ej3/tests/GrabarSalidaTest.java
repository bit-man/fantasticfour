/**
 * 
 */
package fantasticfour.algo3.tp2.ej3.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp2.ej3.GrabarSalida;

/**
 * @author bit-man
 *
 */
public class GrabarSalidaTest {
	
	private static final String ARCHIVO = "/home/bit-man/eclipseWorkspace/TP2/Ejercicio3/test/GrabarSalidaTest.out";
	private static final String ARCHIVO_TEST = "/home/bit-man/eclipseWorkspace/TP2/Ejercicio3/test/GrabarSalidaTest.testigo.out";

	private GrabarSalida salida = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		salida = new GrabarSalida( ARCHIVO );
	}

	/**
	 * Test method for {@link fantasticfour.algo3.tp2.ej3.GrabarSalida}.
	 */
	@Test
	public void testGrabarSalida() throws FileNotFoundException, IOException {
		salida.cadenaPertenece(false);
		salida.cadenaPertenece(true);
		salida.cantElementos(2);
		salida.grabar();
		salida.cerrar();
		
		grabarArchivoTestigo( ARCHIVO_TEST );
		assertTrue( archivosIguales( ARCHIVO_TEST, ARCHIVO ) );
	}

	private boolean archivosIguales(String archivo1, String archivo2) throws FileNotFoundException, IOException {
		FileReader f1 = new FileReader( new File(archivo1));
		FileReader f2 = new FileReader( new File(archivo2));
		boolean sonIguales = true;
		
		while( f1.ready() && f2.ready() && sonIguales )
			if ( f1.read() != f2.read() )
				sonIguales = false;
		
		f1.close();
		f2.close();
		return sonIguales;
	}

	private void grabarArchivoTestigo(String archivoTest) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter( 
                new File( archivoTest ) 
        );
		
        pw.println("0 1 2");
        pw.close();
	}

}
