/**
 *
 */
package fantasticfour.algo3.tp3.test;

import fantasticfour.algo3.tp3.ej2.Contador;
import static org.junit.Assert.*;

import org.junit.Test;


/**
 * @author bit-man
 *
 */
public class ContadorTest
{
	private static final int	VALOR_DE_RESET	= 1;
	private static final int	BASE_12	= 12;
	private static final int	DOS_DIGITOS	= 2;
    private static final int DEFAULT_BASE_10 = 10;
    private static final int MAX_DIGITOS = 6;

    private void testBasicBase10(int digitos)
    {
        testBasic(digitos, DEFAULT_BASE_10);
    }

    private void testBasic(int digitos, int base)
    {
        Contador c = new Contador(digitos, base);
        assertTrue(c.getDigitos() == digitos );
        c.reset();
        assertTrue( c.getContador() == 0);
        assertFalse( c.dioOverflow() );

        for (int i = 1;
                i < (int) java.lang.Math.pow((double) base, (double) digitos);
                i++)
        {
            c.inc();
            assertTrue(c.getContador() == i);
        }
    }

    @Test
    public void testIncrementalBase10()
    {
        for (int i = 1; i <= MAX_DIGITOS; i++)
            testBasicBase10(i);
    }

    @Test
    public void testObtenerDigitoMayorQueElUltimo()
    {
        for (int i = 1; i <= MAX_DIGITOS; i++)
        {
            Contador c = new Contador(i);

            try
            {
                c.getDigito(i);
                fail("No debería poder obtener un dígito mayor al último");
            }
            catch (Exception e)
            {
                // Si da una excepción está bien
            }
        }
    }

    @Test
    public void testObtenerDigitoObtenible() throws Exception
    {
        for (int i = 1; i <= MAX_DIGITOS; i++)
        {
            Contador c = new Contador(i);

            for (int j = 0; j < i; j++)
                c.getDigito(j);
        }
    }
    
    
    @Test
    public void testReset() throws Exception {
    	Contador c = new Contador(DOS_DIGITOS);
    	assertFalse( c.dioOverflow() );
    	assertTrue( c.getContador() == 0);
    	c.inc();
    	assertTrue( c.getContador() == 1);
    	assertFalse( c.dioOverflow() );
    	c.reset();
    	assertTrue( c.getContador() == 0);
    	assertFalse( c.dioOverflow() );
    	
    	c.reset(VALOR_DE_RESET);
    	assertTrue( c.getContador() == (DEFAULT_BASE_10 * VALOR_DE_RESET) + VALOR_DE_RESET );
    	assertFalse( c.dioOverflow() );
    	c.inc();
    	assertTrue( c.getContador() == (DEFAULT_BASE_10 * VALOR_DE_RESET) + VALOR_DE_RESET + 1 );
    	assertFalse( c.dioOverflow() );
    	
    	c.reset(new int[]{VALOR_DE_RESET,VALOR_DE_RESET} );
    	assertTrue( c.getContador() == (DEFAULT_BASE_10 * VALOR_DE_RESET) + VALOR_DE_RESET );
    	assertFalse( c.dioOverflow() );
    	c.inc();
    	assertTrue( c.getContador() == (DEFAULT_BASE_10 * VALOR_DE_RESET) + VALOR_DE_RESET + 1 );
    	assertFalse( c.dioOverflow() );
    }

    @Test
    public void testBase12() throws Exception {
    	Contador c = new Contador(DOS_DIGITOS,BASE_12);
    	assertFalse( c.dioOverflow() );
    	
    	for(int i=1; i <= 11; i++) {
    		c.inc();
    		assertFalse( c.dioOverflow() );
    	}
    	
    	assertTrue( c.getDigito(0) == BASE_12 - 1 );
    	assertTrue( c.getDigito(1) == 0 );
    	
    	c.inc();
    	assertTrue( c.getDigito(0) == 0 );
    	assertTrue( c.getDigito(1) == 1 );
    	assertFalse( c.dioOverflow() );
    }
    
    @Test
    public void testSetIncDigito() throws Exception {
    	Contador c = new Contador(DOS_DIGITOS);
    	assertFalse( c.dioOverflow() );
    	c.setDigito(0, 9);
    	assertFalse( c.dioOverflow() );
    	assertTrue(c.getDigito(0) == 9);
    	assertTrue(c.getDigito(1) == 0);

    	assertFalse( c.incDigito(1) ); // pasar de 0 a 1 no genera carry
    	assertTrue(c.getDigito(1) == 1);
    	assertFalse( c.dioOverflow() );
    	
    	assertTrue( c.incDigito(0) );       // pasar de 0 a 1 si genera carry
    	assertTrue( c.getDigito(1) == 1 );  // ... y no incrementa al siguiente
    	assertFalse( c.dioOverflow() );
    }
    
    @Test
    public void testOverflow() {
    	Contador c = new Contador(DOS_DIGITOS);
    	assertFalse( c.dioOverflow() );
    	c.setDigito(0, 9);
    	assertFalse( c.dioOverflow() );
    	c.setDigito(1, 9);
    	assertFalse( c.dioOverflow() );
    	
    	c.inc();
    	assertTrue( c.dioOverflow() );
    }

    @Test
    public void testDecDigito() throws Exception {
    	Contador c = new Contador(DOS_DIGITOS);
    	
    	c.setDigito(0, VALOR_DE_RESET);
    	
    	assertFalse( c.decDigito(0) );
    	assertFalse( c.dioOverflow() );
    	assertTrue( c.getDigito(0) == 0 );
    	
    	assertTrue( c.decDigito(0) );
    	assertTrue( c.getDigito(0) == DEFAULT_BASE_10 - 1 );
    }
    

    @Test
    public void testDec() throws Exception {
    	Contador c = new Contador(DOS_DIGITOS);
    	c.dec();
    	assertTrue( c.dioOverflow() );
    	assertTrue( c.getDigito(0) == DEFAULT_BASE_10 - 1 );
    	assertTrue( c.getDigito(1) == DEFAULT_BASE_10 - 1 );
    	
    	c.reset(VALOR_DE_RESET);
    	assertFalse( c.dioOverflow() );
    	assertTrue( c.getDigito(0) == 1 );
    	assertTrue( c.getDigito(1) == 1 );

    	c.dec();
    	assertFalse( c.dioOverflow() );
    	assertTrue( c.getDigito(0) == 0 );
    	assertTrue( c.getDigito(1) == 1 );

    	c.dec();
    	assertFalse( c.dioOverflow() );
    	assertTrue( c.getDigito(0) == 9 );
    	assertTrue( c.getDigito(1) == 0 );
    }
}
