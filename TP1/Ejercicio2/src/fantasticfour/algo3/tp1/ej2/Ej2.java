/**
 * 
 */
package fantasticfour.algo3.tp1.ej2;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author bit-man
 *
 */
public class Ej2 {
	private static final String SUFIJO_SALIDA = ".out";
	private static final String SUFIJO_ENTRADA = ".in";
	private static final String SUFIJO_OPERACIONES = ".op";

	private String archivoEntrada;
	private String archivoSalida;
	private String archivoOperaciones;
	
	public Ej2( String prefijo ) {
		archivoEntrada = prefijo + SUFIJO_ENTRADA;
		archivoSalida = prefijo + SUFIJO_SALIDA;
		archivoOperaciones = prefijo + SUFIJO_OPERACIONES;
	}
	
	public void resolver() {
		try {
			LectorInstancia inst = new LectorInstancia( archivoEntrada );
			EscribirResultados res = new EscribirResultados( archivoSalida ); 
			EscribirOperaciones op = new EscribirOperaciones( archivoOperaciones );

			while( inst.hayMasCasos() ) {
				System.out.println("Nuevo caso");
				inst.leerCaso();
				
				System.out.println("Peso max : " + inst.pesoTotal() );
				System.out.println("Cant. de items : " + inst.items() );
				mostrarArray("Valores : ", inst.valores() );
				mostrarArray("Pesos   : ", inst.pesos() );
				
				Backtracking b = new Backtracking( inst.pesoTotal(), inst.pesos(), inst.valores() );
				b.resolver();
				int[] solucion = b.solucion();

				res.nuevosResultados();
				res.cantElementos( solucion.length );
				res.indices( solucion );
				res.valorLista( sumarValores( solucion, inst.valores() ) );
				res.grabar();

				op.grabar( b.nInstruc() );
				
				System.out.println("-----------------------------------------------");
				
			}
			
			op.cerrar();
			res.cerrar();
			inst.cerrar();
		} catch (FileNotFoundException e) {
			System.err.println("No se encuentra el archivo '"+ archivoEntrada +"' o '"
					          + archivoSalida +"'");
		} catch (IOException e) {
			System.err.println("Error al cerrar el archivo '"+ archivoEntrada +"'");
		}	
	}
	


	/***
	 * 
	 * @param indice
	 * @param valor
	 * @return
	 */
	private int sumarValores( int[] indice, int[] valor ) {
		int suma = 0;
		
		for( int i = 0; i < indice.length; i++) {
			// En el problema el primer elemento es el '1'
			// pero en el array es el '0'
			suma += valor[ indice[i] - 1];
		}
		
		return suma;
	}

	private void mostrarArray( String msg,  int[] a ) {
		System.out.print( msg );
		for( int i=0; i < a.length; i++ )
			System.out.print( " " + a[i] );
		System.out.println();
	}
}
