package fantasticfour.algo3.tp1.ej3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


class Salida {
	private FileWriter archivoSalida;
	private FileWriter statisticsFile;
	private String nombreArchivoSalida;	

	public Salida(File archivoSalida) 
		throws IOException 
	{
		this.nombreArchivoSalida = archivoSalida.getAbsolutePath();
		this.archivoSalida = new FileWriter(archivoSalida);
		this.statisticsFile = new FileWriter(new File(nombreArchivoSalida + ".op"));
	}

	public void cerrar()
	{
		try {
			if (archivoSalida != null) {
				archivoSalida.close();
			}
		} catch (IOException e) {
			printCloseError(nombreArchivoSalida);
		}
		
		try {
			if (statisticsFile != null) {
				statisticsFile.close();
			}
		} catch (IOException e2) {
			printCloseError(nombreArchivoSalida + ".op");				
		}
	}

	private void printCloseError(String nombreArchivoSalida) 
	{
		System.out.println("No se puede cerrar el archivo " + nombreArchivoSalida);
	}

	public void grabarResultado(int result) throws IOException {
		
			archivoSalida.write(result + "\n");
		
	}
	public void grabarCantOperaciones(int tamanio, int valor) throws IOException {
		
		statisticsFile.write(tamanio + "   " + valor + "   " + (tamanio - valor) + "\n");
	
}


}
