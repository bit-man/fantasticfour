/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;

import java.io.FileNotFoundException;


/**
 * @author bit-man
 *
 */
public class Ej3Solver {

	LectorInstancia entrada = null; 
	GrabarSalida salida = null; 
	EscribirOperaciones operaciones = null;
	
	public Ej3Solver( String archivoIn, String archivoOut, String archivoOp) 
												throws FileNotFoundException {
		entrada = new LectorInstancia( archivoIn );
		salida = new GrabarSalida( archivoOut );
		operaciones = new EscribirOperaciones( archivoOp );
		
	}
	
	public void resolver() throws Exception {

		while( entrada.hayMasInstrucciones() ) {

			RadixTree r = new RadixTree();
			salida.nuevosResultados();

			while ( entrada.hayMasInstrucciones() ) {
				entrada.siguienteOperacion();
				String oper = entrada.operacion();
				ejecutarInstruciones(oper, r);
				operaciones.grabar( r.instruc() );
			}
			
			salida.grabar();
			entrada.siguienteInstancia();
		}
	}

	/**
	 * @param oper : operación a ejecutar
	 * @param r: Radix Tree sobre el cual ejecutar las operaciones
	 * @throws Exception 
	 */
	private void ejecutarInstruciones(String oper, RadixTree r) throws Exception {
		
		if ( oper.equals(Const.COMANDO_AGREGAR) )
			r.agregar( entrada.operando() );
		else if ( oper.equals(Const.COMANDO_PERTENECE) )
			salida.cadenaPertenece( r.pertenece( entrada.operando() ) );
		else if ( oper.equals(Const.COMANDO_CARDINAL) )
			salida.cantElementos( r.cardinal() );
		else if ( oper.equals(Const.COMANDO_SACAR) )
			r.sacar( entrada.operando() );
		else 
			throw new Exception("No existe la operación " + oper);
	}
	
	public void finalizar() {
		entrada.cerrar();
		salida.cerrar();
		operaciones.cerrar();
	}
}
