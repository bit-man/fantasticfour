/**
 * 
 */
package fantasticfour.algo3.tp2.ej3.tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp2.ej3.LectorInstancia;

/**
 * @author bit-man
 *
 */
public class LectorInstanciaTest {

	private static final int CANT_INSTRUCCIONES = 6;
	private static final String ARCHIVO_INEXISTENTE = "noExisteNiAGanchos";
	private static final String ARCHIVO_EXISTE = "/home/bit-man/eclipseWorkspace/TP2/Ejercicio3/test/Tp2Ej3.in";
	
	LectorInstancia r = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUpBefore() throws FileNotFoundException {
		r = new LectorInstancia( ARCHIVO_EXISTE );
		assertTrue( r != null );
	}

	@After
	public void tearDownAfter() {
		r.cerrar();
	}
	
	@Test(expected= FileNotFoundException.class)
	public void testArchivoNoExiste() throws FileNotFoundException {
		r = new LectorInstancia( ARCHIVO_INEXISTENTE );
		fail("Debio tirar una excepci�n porque el archivo no existe");
	}
	
	/**
	 * Test method for {@link fantasticfour.algo3.tp2.ej3.LectorInstancia#LectorInstancia(java.lang.String)}.
	 */
	@Test
	public void testHayMasInstrucciones() {		
		assertTrue( r.hayMasInstrucciones() );
	}

	/**
	 * Test method for {@link fantasticfour.algo3.tp2.ej3.LectorInstancia#hayMasInstrucciones()}.
	 */
	@Test
	public void testRecorrerArchivo() {
		assertTrue( r.hayMasInstrucciones() );
		
		int lineas = 0;
		while( r.hayMasInstrucciones() ) {
			r.siguienteOperacion();
			lineas++;
		}
		
		assertTrue( lineas == CANT_INSTRUCCIONES );
		assertTrue( ! r.hayMasInstrucciones() );
	}
	
	/**
	 * Test method for {@link fantasticfour.algo3.tp2.ej3.LectorInstancia#siguienteInstancia()}.
	 */
	@Test
	public void testOperadoresYOperandos() {
		String[] operacion = new String[] { "agregar", "pertenece", "agregar", "cardinal", "sacar", "pertenece" };
		String[] operando  = new String[] { "hola", "hola", "chau", null, "hola", "hola" };
		assertTrue( r.hayMasInstrucciones() );
		
		int linea = 0;
		while( r.hayMasInstrucciones() ) {
			r.siguienteOperacion();
			System.out.println(operacion[linea]);
			assertTrue( r.operacion().equalsIgnoreCase( operacion[linea] ) );
			
			if (operando[linea] != null )
				assertTrue( r.operando().equalsIgnoreCase( operando[linea]) );

			linea++;
		}
		
		assertTrue( ! r.hayMasInstrucciones() );
	}

}
