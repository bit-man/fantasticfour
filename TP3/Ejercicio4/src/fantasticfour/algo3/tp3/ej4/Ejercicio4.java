package fantasticfour.algo3.tp3.ej4;


import fantasticfour.algo3.tp3.*;

import java.io.File;
import java.util.List;

public class Ejercicio4
{
    public static void main(String[] args)
    {
        if (args.length != 2)
        {
			System.out.println("Error: Forma de uso: fantasticfour.algo3.tp3.ej4.Ejercicio4 <archInput.in> <archInput.out>");
			System.exit(1);
		}

        final String archivoEntradaNombre = args[0];
        final File archivoEntrada = new File(archivoEntradaNombre);

		if (!archivoEntrada.isFile()) {
			System.out.println("Error: El archivo " + archivoEntradaNombre + " no existe o es un directorio");
			System.exit(1);
		}

        final Input input = new Input(archivoEntrada);
        input.leerArchivo();
        List<Instancia> instancias = input.dameInstancias();

        final String archivoSalidaNombre = args[1];
        try
        {
            Output output = new Output(archivoSalidaNombre);
            OutputOperaciones operaciones = new OutputOperaciones(archivoSalidaNombre);
            
            for (Instancia instancia : instancias)
            {
                int cantidadCrucesOriginal = instancia.dameGrafo().cantidadCruces();
                Ej4Solver ej4Solver = new Ej4Solver(instancia);
                Grafo grafo = ej4Solver.solve(false);
                output.setIV1(grafo.dameNodosV1());
                output.setIV2(grafo.dameNodosV2());
                output.setKID(grafo.cantidadCruces());
                output.grabar();
                output.nuevosResultados();

                operaciones.setValues(grafo.dameEjes().size(), ej4Solver.getCantidadDeOperaciones(), cantidadCrucesOriginal - grafo.cantidadCruces());
            }
            operaciones.grabar();
            output.cerrar();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}