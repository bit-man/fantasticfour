/**
 * 
 */
package fantasticfour.algo3.tp3.test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp3.ListaAdyacencia;
import fantasticfour.algo3.tp3.Nodo;


/**
 * @author bit-man
 *
 */
public class ListaAdyacenciaTest {
	
	private static final HashMap<Nodo, LinkedList<Nodo>> LISTA_DE_ADYACENCIA_VACIA = new HashMap<Nodo, LinkedList<Nodo>>();
	private static final LinkedList<Nodo> LISTA_DE_NODOS_VACIA = new LinkedList<Nodo>();
	private static final int VALOR_MUY_ALTO = 999;
	private ListaAdyacencia l;
	private Nodo nodoQueNoExiste;
	
	@Before
	public void setUp() throws Exception {
		nodoQueNoExiste = new Nodo(VALOR_MUY_ALTO, false);
		l = new ListaAdyacencia();
	}
	
	@Test
	public void testVacia() throws Exception {
		assertTrue( l.getCantNodos() == 0 );
		assertFalse( l.tieneNodo(nodoQueNoExiste) );
		assertTrue( l.getEjesDe(nodoQueNoExiste).equals( LISTA_DE_NODOS_VACIA ) );
		assertTrue( l.getListaAdyacencia().equals( LISTA_DE_ADYACENCIA_VACIA ) );
	}

	@Test
	public void testUnNodoSolo() throws Exception {
		Nodo n = new Nodo(1, false);
		l.agregar(n);
		assertTrue( l.getCantNodos() == 1 );
		assertFalse( l.tieneNodo(nodoQueNoExiste) );
		assertTrue( l.tieneNodo(n) );
		assertTrue( l.getEjesDe(nodoQueNoExiste).equals( LISTA_DE_NODOS_VACIA ) );
		assertTrue( l.getEjesDe(n).equals( LISTA_DE_NODOS_VACIA ) );
		assertFalse( l.getListaAdyacencia().equals( LISTA_DE_ADYACENCIA_VACIA ) );
	}

	@Test
	public void testDosNodos() throws Exception {
		Nodo n1 = new Nodo(1, false);
		Nodo n2 = new Nodo(2, false);
		l.agregar(n1,n2);
		assertTrue( l.getCantNodos() == 2 );
		assertFalse( l.tieneNodo(nodoQueNoExiste) );
		assertTrue( l.tieneNodo(n1) );
		assertTrue( l.tieneNodo(n2) );
		assertFalse( l.getEjesDe(n2).equals( LISTA_DE_NODOS_VACIA ) );		
		assertTrue( l.getEjesDe(nodoQueNoExiste).equals( LISTA_DE_NODOS_VACIA ) );
		assertFalse( l.getEjesDe(n1).equals( LISTA_DE_NODOS_VACIA ) );
		assertFalse( l.getListaAdyacencia().equals( LISTA_DE_ADYACENCIA_VACIA ) );

		LinkedList<Nodo> vecinosNodo1 = new LinkedList<Nodo>();
		vecinosNodo1.add(n2);
		assertTrue( l.getEjesDe(n1).equals( vecinosNodo1 ) );
		
		LinkedList<Nodo> vecinosNodo2 = new LinkedList<Nodo>();
		vecinosNodo2.add(n1);
		assertTrue( l.getEjesDe(n2).equals( vecinosNodo2 ) );
		
		HashMap<Nodo, LinkedList<Nodo>> lAdy = new HashMap<Nodo, LinkedList<Nodo>>();
		lAdy.put(n1, vecinosNodo1);
		lAdy.put(n2, vecinosNodo2);
		assertTrue( l.getListaAdyacencia().equals( lAdy ) );		
	}
	

	@Test
	public void testTresNodos() throws Exception {
		Nodo n1 = new Nodo(1, false);
		Nodo n2 = new Nodo(2, false);
		Nodo n3 = new Nodo(3, false);
		l.agregar(n1,n2);
		l.agregar(n1,n3);

		assertTrue( l.tieneNodo(n1) );
		assertTrue( l.tieneNodo(n3) );
		assertTrue( l.tieneNodo(n2) );
		assertTrue( l.getCantNodos() == 3 );
		assertFalse( l.tieneNodo(nodoQueNoExiste) );
		assertTrue( l.tieneNodo(n2) );
		assertFalse( l.getEjesDe(n2).equals( LISTA_DE_NODOS_VACIA ) );

		assertTrue( l.getEjesDe(nodoQueNoExiste).equals( LISTA_DE_NODOS_VACIA ) );
		assertFalse( l.getEjesDe(n1).equals( LISTA_DE_NODOS_VACIA ) );
		assertFalse( l.getListaAdyacencia().equals( LISTA_DE_ADYACENCIA_VACIA ) );
		
		/***
		 * TODO Aquí vuelve a fallar el test porque los adyacentes no están ordenados
		 * 		según el orden en que van siendo agregados (se hace un addFirst() en
		 * 		lugar de un add()
		 * 		Discutirlo primero y codificarlo después :-) 
		 */
		LinkedList<Nodo> vecinosNodo1 = new LinkedList<Nodo>();
		vecinosNodo1.add(n2);
		vecinosNodo1.add(n3);
		assertTrue( l.getEjesDe(n1).equals( vecinosNodo1 ) );
		LinkedList<Nodo> vecinosNodo2 = new LinkedList<Nodo>();
		vecinosNodo1.add(n1);
		vecinosNodo1.add(n3);
		assertTrue( l.getEjesDe(n2).equals( vecinosNodo2 ) );
		LinkedList<Nodo> vecinosNodo3 = new LinkedList<Nodo>();
		vecinosNodo1.add(n1);
		vecinosNodo1.add(n2);
		assertTrue( l.getEjesDe(n1).equals( vecinosNodo3 ) );
		/*** Fin ToDo **/
		
		HashMap<Nodo, LinkedList<Nodo>> lAdy = new HashMap<Nodo, LinkedList<Nodo>>();
		lAdy.put(n1, vecinosNodo1);

		/***
		 * TODO discutir si al agregar un eje este debe verse desde ambos nodos
		 * 		IMHO sí, pero mejor discutirlo primero y codificarlo después :-) 
		 */
		lAdy.put(n2, vecinosNodo2);
		lAdy.put(n3, vecinosNodo3);
		/*** Fin ToDo **/
		
		assertTrue( l.getListaAdyacencia().equals( lAdy ) );		
	}
	
	@Test
	public void testSimple() throws Exception {
		GrafoTest g = new GrafoTest();
		ListaAdyacencia lAdy = new ListaAdyacencia( g.setUpSimple().dameGrafo() );
		assertTrue( lAdy.getCantNodos() == 8 );
		
		LinkedList<Nodo> lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[2] );
		assertTrue( lAdy.getEjesDe( g.n[0] ).equals( lNodo ) );

		lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[2] );
		lNodo.add( g.n[3] );
		assertTrue( lAdy.getEjesDe( g.n[1] ).equals( lNodo ) );

		lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[0] );
		lNodo.add( g.n[1] );
		assertTrue( lAdy.getEjesDe( g.n[2] ).equals( lNodo ) );

		lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[1] );
		assertTrue( lAdy.getEjesDe( g.n[3] ).equals( lNodo ) );
		
		lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[7] );
		assertTrue( lAdy.getEjesDe( g.n[4] ).equals( lNodo ) );
		
		lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[4] );
		assertTrue( lAdy.getEjesDe( g.n[7] ).equals( lNodo ) );
		
		lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[6] );
		assertTrue( lAdy.getEjesDe( g.n[5] ).equals( lNodo ) );
		
		lNodo = new LinkedList<Nodo>();
		lNodo.add( g.n[5] );
		assertTrue( lAdy.getEjesDe( g.n[6] ).equals( lNodo ) );
		
		// Todos los nodos tienen un adyacente
		for( int i=0; i < g.n.length; i++)
			assertTrue( lAdy.getListaAdyacencia().containsKey( g.n[i] ) );
	}

}
