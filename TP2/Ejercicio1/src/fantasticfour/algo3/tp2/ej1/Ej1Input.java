package fantasticfour.algo3.tp2.ej1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Ej1Input 
{
	private Scanner s;
	private int jugadores;
	private int cantPartidosArreglados;
	private Grafo partidosArreglados; 

	public Ej1Input(File archivoEntrada) 
		throws FileNotFoundException 
	{
		s = new Scanner(archivoEntrada);
   	}

	private void crearGrafoDePartidosArreglados(int cantidad, int cantPartidosArreglados) 
	{
		partidosArreglados = new Grafo();
		for(int i = 0; i < cantPartidosArreglados; i++) {
			partidosArreglados.agregar(s.nextInt(), s.nextInt());
		}

		// Agrego los nodos sin ejes.
		if (partidosArreglados.getListaAdyacencia().keySet().size() != jugadores) {
			for (int i = 1; i <= jugadores; i++) {
				if (!partidosArreglados.tieneNodo(i)) {
					partidosArreglados.agregar(i);
				}
			}
		}
	} 

	public int getCantPartidosArreglados()
	{
		return cantPartidosArreglados;
	}
	
	public int getJugadores()
	{
		return jugadores;
	}
	
	public Grafo getProximaInstancia()
	{
		jugadores = s.nextInt();
        cantPartidosArreglados = s.nextInt();

        if (jugadores == 0 && cantPartidosArreglados == 0) {
        	partidosArreglados = null;
        } else {
        	crearGrafoDePartidosArreglados(jugadores, cantPartidosArreglados);
        }

        return partidosArreglados;
	}
	
	public void cerrar() 
	{
		s.close();
	}
}
