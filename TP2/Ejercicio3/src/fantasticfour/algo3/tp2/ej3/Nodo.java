/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;

import java.util.Collection;
import java.util.TreeMap;

import fantasticfour.algo3.tp2.lib.Constantes;
import fantasticfour.algo3.tp2.lib.NumbersUtil;

/**
 * @author bit-man
 *
 */
public class Nodo {
	
 

	public String valor;
	public Nodo padre = null;
	
	private TreeMap<Character,Nodo> siguiente = null;
	private int nInstruc = 0;
	
	public Nodo( String s ) {
		valor = s;
		padre = null;
		siguiente = new TreeMap<Character,Nodo>();
		nInstruc = 1;
	}

	public void actualizarHijo( Nodo hijo, Nodo valor ) {
		siguiente.put( hijo.valor.charAt(0), valor );
		nInstruc += 1 + instrucGetPutDel();
	}

	// ToDo falta hacer el testing
	public void borrarHijo( Nodo hijo ) {
		siguiente.remove( hijo.valor.charAt(0) );
		nInstruc += 1 + instrucGetPutDel();
	}
	
	public Nodo siguienteNodo( String s ){
		nInstruc += 1 + instrucGetPutDel();
	    return siguiente.get( s.charAt(0) );
	}
	
	public boolean esHoja() {
		nInstruc += 1;
		return siguiente.isEmpty();
	}
	
	public int cantHijos() {
		nInstruc += 1;
		return siguiente.size();
	}

	public Nodo dameElUnicoHijo()  {                                           // O(1)
	    assert( siguiente.size() == 1 );
		nInstruc += 2;  // c�mo existe una sola key, entonces seguro que la encuentra en O(1)
	    return siguiente.get( siguiente.lastKey() );
	}
	
	/**
	 * 
	 * @return Devuelve la cantidad de instrucciones ejecutadas desde la �ltima
	 *         vez que se consult� por la cantidad de instrucciones ejecutadas. 
	 */

	public int instruc() {
		int ret = nInstruc;
		nInstruc = 0;
		return ret;
	}
	
	public Collection<Nodo> hijos() {
		return siguiente.values();
	}

	private int instrucGetPutDel() {
		if (siguiente.size() == 0)
			return 0;
		else
			return (int) Math.ceil( NumbersUtil.logaritmo( (double) siguiente.size(), Constantes.BASE_2) );
	} 
}
