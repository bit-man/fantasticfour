/**
 * 
 */
package fantasticfour.algo3.tp3.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import org.junit.Test;

import fantasticfour.algo3.tp3.Eje;
import fantasticfour.algo3.tp3.InputGrabar;
import fantasticfour.algo3.tp3.Nodo;


/**
 * @author bit-man
 *
 */
public class InputGrabarTest {
	private static final int	NODO_DEFAULT_VALUE	= 3;
	private static final String	FILE_NINGUNA_INSTANCIA	= "-1\n";
	private static final String	FILE_NODOS_Y_EJES_VACIOS	= "0\n0\n0\n0\n0\n0\n-1\n";
	private static final String	FILE_NODOS_Y_EJES_SIMPLE_SIN_FIN	= 
													"1\n3\n1\n3\n1\n3 3\n1\n3\n1\n3\n1\n3 3\n";	
	private static final String	FILE_NODOS_Y_EJES_SIMPLE	= 
								FILE_NODOS_Y_EJES_SIMPLE_SIN_FIN + "-1\n";
	
	private static final String	DEFAULT_PREFIX	= "test";
	private static final String	TEMP_FOLDER	= "/tmp";

	@Test
	public void sinInstancias() throws FileNotFoundException, IOException {
		String prefijo = TEMP_FOLDER + "/" + DEFAULT_PREFIX;
		InputGrabar input = new InputGrabar(prefijo);
		File f = input.getFile();
		assertTrue( input.getNombreArchivo().startsWith(DEFAULT_PREFIX) );
		input.cerrar();
		
		assertTrue( tieneElFormatoCorrecto( f.getAbsolutePath(),FILE_NINGUNA_INSTANCIA) );
	}
	
	@Test
	public void nodosYejesVacios() throws FileNotFoundException, IOException {
		String prefijo = TEMP_FOLDER + "/" + DEFAULT_PREFIX;
		InputGrabar input = new InputGrabar(prefijo);
		File f = input.getFile();
		
		LinkedList<Nodo> nodos = new LinkedList<Nodo>();
		LinkedList<Eje> ejes = new LinkedList<Eje>();
		input.setNodosV1(nodos);
		input.setNodosV2(nodos);
		input.setEjes(ejes);
		input.setNodosIV1(nodos);
		input.setNodosIV2(nodos);
		input.setEjesI(ejes);
		input.grabar();
		
		input.cerrar();
		
		assertTrue( tieneElFormatoCorrecto( f.getAbsolutePath(),FILE_NODOS_Y_EJES_VACIOS) );
	}

	@Test
	public void nodosYejesSimple() throws FileNotFoundException, IOException {
		String prefijo = TEMP_FOLDER + "/" + DEFAULT_PREFIX;
		InputGrabar input = new InputGrabar(prefijo);
		File f = input.getFile();
		
		LinkedList<Nodo> nodos = new LinkedList<Nodo>();
		nodos.add( new Nodo(NODO_DEFAULT_VALUE) );
		LinkedList<Eje> ejes = new LinkedList<Eje>();
		ejes.add( new Eje( 
						new Nodo(NODO_DEFAULT_VALUE), 
						new Nodo(NODO_DEFAULT_VALUE) 
						)
		);
		input.setNodosV1(nodos);
		input.setNodosV2(nodos);
		input.setEjes(ejes);
		input.setNodosIV1(nodos);
		input.setNodosIV2(nodos);
		input.setEjesI(ejes);
		input.grabar();
		
		input.cerrar();
		
		assertTrue( tieneElFormatoCorrecto( f.getAbsolutePath(),FILE_NODOS_Y_EJES_SIMPLE) );
	}

	@Test
	public void nodosYejesSimple2() throws FileNotFoundException, IOException {
		String prefijo = TEMP_FOLDER + "/" + DEFAULT_PREFIX;
		InputGrabar input = new InputGrabar(prefijo);
		File f = input.getFile();
		
		LinkedList<Nodo> nodos = new LinkedList<Nodo>();
		nodos.add( new Nodo(NODO_DEFAULT_VALUE) );
		LinkedList<Eje> ejes = new LinkedList<Eje>();
		ejes.add( new Eje( 
						new Nodo(NODO_DEFAULT_VALUE), 
						new Nodo(NODO_DEFAULT_VALUE) 
						)
		);
		input.setNodosV1(nodos);
		input.setNodosV2(nodos);
		input.setEjes(ejes);
		input.setNodosIV1(nodos);
		input.setNodosIV2(nodos);
		input.setEjesI(ejes);
		input.grabar();
		

		input.setNodosV1(nodos);
		input.setNodosV2(nodos);
		input.setEjes(ejes);
		input.setNodosIV1(nodos);
		input.setNodosIV2(nodos);
		input.setEjesI(ejes);
		input.grabar();
		
		input.cerrar();
		
		assertTrue( tieneElFormatoCorrecto( f.getAbsolutePath(),
				FILE_NODOS_Y_EJES_SIMPLE_SIN_FIN + FILE_NODOS_Y_EJES_SIMPLE) );
	}
	
	public boolean tieneElFormatoCorrecto(String filePath, String formato) throws IOException {
		FileReader fr = new FileReader( filePath );
		char[] cbuf = new char[formato.length()]; 
		int charsRead = fr.read(cbuf);
		
		if (charsRead != cbuf.length)
			return false;
		
		for(int i=0; i < cbuf.length; i++)
			if ( cbuf[i] != formato.charAt(i) )
				return false;
		
		return true;
	}
}
