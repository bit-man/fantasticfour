/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * @author bit-man
 *
 */

// ToDo : clase de test !!!
public class EscribirOperaciones {
        
        private PrintWriter pw = null;
        
        public EscribirOperaciones( String archivo ) throws FileNotFoundException {
                pw = new PrintWriter( 
                                new File( archivo ) 
                        );
        }
        
        public void grabar( long n ) {
                pw.println(n);
        }
        
        public void cerrar() {
                pw.close();
        }
}
