package fantasticfour.algo3.tp3.test;


import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp3.Grafo;
import fantasticfour.algo3.tp3.Instancia;
import fantasticfour.algo3.tp3.MatrizAdyacencia;

import fantasticfour.algo3.tp3.test.GrafoTest;

public class MatrizAdyacenciaTest {

	private Instancia i;
	private Grafo g;
	private GrafoTest gt;
	
	@Before
	public void setUp() throws Exception {
		 gt = new GrafoTest();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSimple() {
		i = gt.setUpSimple();
		g = i.dameGrafoOriginal();
		MatrizAdyacencia m = new MatrizAdyacencia(g);
		boolean[][] miMatrizAdyacencia = new boolean[][]{ {true, false}, 
														  {true, true} } ;
		comparaMatrizBoolean(m, miMatrizAdyacencia);
		
		//g = i.dameGrafo();
		m = new MatrizAdyacencia(i);
		miMatrizAdyacencia = new boolean[][]{ {true,  false, false, false}, 
				                              {true,  true,  false, false},
				                              {false, false, false, true},
				                              {false, false, true,  false} } ;
		comparaMatrizBoolean(m, miMatrizAdyacencia);
	}
	
	@Test
	public void testMenosSimple() {
		i = gt.setupMenosSimple();
		g = i.dameGrafoOriginal();
		MatrizAdyacencia m = new MatrizAdyacencia(g);
		boolean[][] miMatrizAdyacencia = new boolean[][]{ {false, false, false}, 
														  {false, false, false},
														  {false, false, true}, 
														  {true,  false, false},} ;
		comparaMatrizBoolean(m, miMatrizAdyacencia);
		
		//g = i.dameGrafo();
		m = new MatrizAdyacencia(i);
		miMatrizAdyacencia = new boolean[][]{ {true,  false, false, false}, 
				                              {true,  true,  false, false},
				                              {false, false, false, true},
				                              {false, false, true,  false} } ;
		comparaMatrizBoolean(m, miMatrizAdyacencia);

	}

	/**
	 * @param m
	 * @param miMatrizAdyacencia
	 */
	private void comparaMatrizBoolean(MatrizAdyacencia m, boolean[][] miMatrizAdyacencia) {
		for( int i=0; i < miMatrizAdyacencia.length; i++)
			assertTrue( Arrays.equals(m.getMatriz()[i], miMatrizAdyacencia[i] ) );
	}
}
