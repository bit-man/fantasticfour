package fantasticfour.algo3.tp1.ej3;

import java.io.IOException;

class Ej3Solver {
	private Entrada entrada;
	
	private Salida salida;
	
	private	int moda;
	// Variable que guardará el valor de la moda del arreglo en caso de existir.
	
	private int cantOperaciones;
	/*
	 * Variable que guardará las cantidad de operaciones realizadas para resolver todos los casos del archivo de entrada.
	 */
	
	public Ej3Solver(Entrada entrada, Salida salida) 
	{
		this.entrada = entrada;
		this.salida = salida;
		this.moda = 0;
		this.cantOperaciones = 0;
	}

	public void solve() throws IOException 
	{
		cantOperaciones = 0;
		/*
		 *Se coloca en cero la cantidad de operaciones antes de comenzar a resolver el problema.
		 */
		
		
		int value;
		
		while (entrada.hayMasValores()
				&& (value = entrada.dameProxValor()) != 0) {
			/*
			 * Chequeo si hay más valores en el archivo de entrada.
			 * Si hay y es cero, termina el programa.
			 * Sino, se busca la moda.
			 */
			
			// cantOperaciones: hayMasValores, &&, =, dameProxValor() , ! y == : 6 operaciones
			
			
			int[] array = new int[value]; 
			/*
			 * Se crea un arreglo vacío del tamaño del valor levantado del archivo de entrada.
			 */
			// una asignación: 1 operación
			
			armarArreglo(array , value);
			/*
			 * Se llena el arreglo vacío con los elementos levantados de la 
			 * siguiente línea del archivo de entrada.
			 */
			
			
			boolean existeModa = false;
			// una asignación: 1 operación
			
			existeModa = existeModa(array , 0 , value-1);
			
			/*
			 * Se guarda el valor boolean devuelto por el método existeModa
			 * en la variable existeModa.
			 */
			
			//resta : 1 operación
			
			int result = 0;
			//asignación : 1 operación
			
			if (existeModa) {
				result = this.dameModa();
				
				/*
				 * Si existeModa es true se guarda el valor de la moda en la 
				 * variable result para luego ser impresa en el archivo de salida. 
				 */
			//asignación = 1 Operación
				
			}
			
			salida.grabarResultado(result);	
			
			
			
			cantOperaciones = cantOperaciones + 11;			
			
			
			salida.grabarCantOperaciones(value, cantOperaciones);
			
			cantOperaciones = 0;
			/*
			 * Total de operaciones cada vez que se ejecuta el while
			 * sin contar las operaciones que se hacen en los métodos
			 * a los cuales se llaman en esta método es de 
			 *	11 operaciones. 
			 */
			
		
			
		}
		
	}

	private void armarArreglo (int[] arreglo , int n){
		
		
		
		/*
		 * Se un arreglo de n posiciones con los elementos levantados del 
		 * archivo de entrada.
		 */
		
		cantOperaciones = cantOperaciones + 2; //primer ejecucion de la guarda. =, <
		
		for (int i=0 ; i < n ; i++ ) {
			
			//asignación, < y ++: 3 operaciones
			
			arreglo[i] = entrada.dameProxValor();

			
			//asignación y dameProxValor: 2 operaciones 
			
			//cada vez que se ejecuta el for es de 5 operaciones 
			
			//cantOperaciones = cantOperaciones + 5;
		}
	}
	
	public int dameModa(){
		return this.moda;
	}
	
	
	private boolean existeModa(int[] arreglo , int prim , int ult) 
	{		
		int j;
		moda = arreglo[prim];
		// Seteo el elemento con el valor del primer elemento del arreglo.
		// una asignación: 1 operación
		
		cantOperaciones = cantOperaciones + 1;
		
		// casos base
		if (ult < prim)
			return false;
		
		// < : 1 operación
		
		cantOperaciones = cantOperaciones + 1;
		
		if (ult == prim)
			return true;
		
		/*
		 *Si ult==prim quiere decir que el arreglo posee un solo elemento,
		 *por lo tanto, existe moda y ese elemento es la moda.
		 */
		// == : 1 operación
		
		cantOperaciones = cantOperaciones + 1;
				
		if (prim + 1 == ult) {
			
		/*
		 * Si prim+1==ultimo quiere decir que el arreglo tiene solo 2 elementos. 
		 */
			
			moda = arreglo[ult];
			
			/* Seteo la moda con el valor del ultimo elemento.
			 * También podría haberlo hecho con el primer.
			 */
			
			cantOperaciones = cantOperaciones + 4;
			
		//	cantOperaciones = cantOperaciones +1 ;
					
			return (arreglo[prim] == arreglo[ult]);
			
		/*
		 * Comparo el primer elemento con el último y si son iguales,
		 * entonces existe moda y el valor es el valor del primer o ultimo elemento.
		 * Se escogió el último.
		 */
			
			
		// +, ==, =, ==: 4 operaciones
		
			
		}
	
		// caso general
		
		j = prim;
		
		/*
		 * En ambos ciclos que continuan, ya se si el arreglo tiene longitud
		 * par o impar, se arma un nuevo arreglo a partir de la primera posición.
		 * Comparo los 2 primeros elementos, si coinciden, coloco alguno de ellos 
		 * en la primera posición, sino, comparo los dos siguientes.
		 * Si esto son iguales coloco este elemento en la 1° o la 2° posición
		 * según corresponda dependiendo si los dos primeros eran o no iguales.
		 * Continuo haciendo esto hasta que pueda seguir tomando elementos de a
		 * dos en el arreglo. 
		 */
		
		// arreglo con longitud par
		
		if ((ult - prim + 1) % 2 == 0) {
			
			
		cantOperaciones = cantOperaciones + 3;  //=,+ y <= : primera evaluación de la guarda
			
			for (int i = prim + 1; i <= ult; i += 2)
				if (arreglo[i - 1] == arreglo[i]) {
					arreglo[j] = arreglo[i];
					j++;
					
					cantOperaciones = cantOperaciones + 1;
				}
			/*
			 * -, +, ==, =, +, <=, +=, -, ==, =, ++, -:12 operaciones
			 */
			
			cantOperaciones = cantOperaciones + 12;
			
			return existeModa(arreglo, prim, j - 1);
		}
		
		// arreglo con longitud impar
		
		else {
			
			//cantOperaciones =cantOperaciones + 2;  //= y <= : primera evaluación de la guarda
			
			for (int i = prim; i < ult; i += 2)
				if (arreglo[i] == arreglo[i + 1]) {
					arreglo[j] = arreglo[i];
					j++;
				
			//		cantOperaciones = cantOperaciones+1;
			/*
			 * =, <, +=, ==, +, =, ++:  7 operaciones
			 */
					
			cantOperaciones = cantOperaciones + 7;
					
				}
			if (!existeModa(arreglo, prim, j - 1))
				moda = arreglo[ult];
			/*
			 * Si no existe moda desde el primero a j-1 entonces
			 * la moda es el último elemento.
			 */
			//!, -, =: 3 operaciones
			
			cantOperaciones = cantOperaciones + 3;
			
			
			
			return true;
		}
	}

}
