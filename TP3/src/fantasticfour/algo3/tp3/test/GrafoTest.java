/**
 * 
 */
package fantasticfour.algo3.tp3.test;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

import org.junit.Test;

import fantasticfour.algo3.tp3.Eje;
import fantasticfour.algo3.tp3.Nodo;
import fantasticfour.algo3.tp3.Instancia;

/**
 * @author bit-man
 *
 */
public class GrafoTest {

	private static final int CRUCES_TEST_MARTI_ESTRUCH_ORDENADO = 4;
	private static final int CRUCES_TEST_MARTI_ESTRUCH = 6;
	private static final int CRUCES_TEST_MENOS_SIMPLE = 9;
	private static final int CRUCES_TEST_SIMPLE = 1;
	/**
	 * 
	 */
	private static final int	NODOS_V1_SIMPLE	= 4;	// Cant. de ejes V1 + IV1
	private static final int	NODOS_V2_SIMPLE	= 4;	// Cant. de ejes V2 + IV2
	private static final int	EJES_V1_SIMPLE	= 3;	// Cant. de ejes V1 + V2
	private static final int	EJES_V2_SIMPLE	= 2;	// Cant. de ejes IV1 + IV2

	private static final int	NODOS_V1_MENOS_SIMPLE	= 6;
	private static final int	NODOS_V2_MENOS_SIMPLE	= 6;
	private static final int	EJES_V1_MENOS_SIMPLE	= 2;
	private static final int	EJES_V2_MENOS_SIMPLE	= 4;

	private static final int	NODOS_V1_MARTI_ESTRUCH	= 5;
	private static final int	NODOS_V2_MARTI_ESTRUCH	= 5;
	private static final int	EJES_V1_MARTI_ESTRUCH	= 5;
	private static final int	EJES_V2_MARTI_ESTRUCH	= 4;
	
	private static final int	NODOS_V1_MARTI_ESTRUCH_ORDENADO	= NODOS_V1_MARTI_ESTRUCH;
	private static final int	NODOS_V2_MARTI_ESTRUCH_ORDENADO	= NODOS_V2_MARTI_ESTRUCH;
	private static final int	EJES_V1_MARTI_ESTRUCH_ORDENADO	= EJES_V1_MARTI_ESTRUCH;
	private static final int	EJES_V2_MARTI_ESTRUCH_ORDENADO	= EJES_V2_MARTI_ESTRUCH;
	
	public Nodo[] n;
	public Eje[] v1, v2;
	public LinkedList<Nodo> nodosV1, nodosV2, nodosIV1, nodosIV2;
	public LinkedList<Eje> ejes, ejesI;
	Instancia instancia;
	/**
	 * @throws java.lang.Exception
	 */

	public Instancia setUpSimple() {
		setupNodos(NODOS_V1_SIMPLE + NODOS_V2_SIMPLE);

		nodosV1 = new LinkedList<Nodo>();
		nodosV1.add(n[0]);
		nodosV1.add(n[1]);
		nodosV2 = new LinkedList<Nodo>();
		nodosV2.add(n[2]);
		nodosV2.add(n[3]);
		nodosIV1 = new LinkedList<Nodo>();
		nodosIV1.add(n[4]);
		nodosIV1.add(n[6]);
		nodosIV2 = new LinkedList<Nodo>();
		nodosIV2.add(n[5]);
		nodosIV2.add(n[7]);
		
		v1 = new Eje[ EJES_V1_SIMPLE ];
		v1[0] = new Eje(n[0],n[2]);
		v1[1] = new Eje(n[1],n[2]);
		v1[2] = new Eje(n[1],n[3]);

		v2 = new Eje[ EJES_V2_SIMPLE ];
		v2[0] = new Eje(n[4],n[7]);
		v2[1] = new Eje(n[6],n[5]);

		ejes = new LinkedList<Eje>();
		ejes.add(v1[0]); 
		ejes.add(v1[1]); 
		ejes.add(v1[2]);
		ejesI = new LinkedList<Eje>();
		ejesI.add(v2[0]); 
		ejesI.add(v2[1]);
		
		return new Instancia( this.nodosV1, this.nodosV2, this.ejes,
					   this.nodosIV1, this.nodosIV2, this.ejesI);
	}

	/**
	 * Genera un array con la cant. de nodos indicada, y lo coloca en el 
	 * array de nodos 'n'
	 * @param nNodos cant. de nodos a generar
	 */
	private void setupNodos(int nNodos) {
		n = new Nodo[nNodos];
		for( int i=0; i< nNodos; i++)
			n[i] = new Nodo(i, false);
	}
	
	@Test
	public void testSimple() throws Exception {
		instancia = setUpSimple();
		testGraph(CRUCES_TEST_SIMPLE);
	}

	/**
	 * Hace el test de correctitud del Grafo 'g'
	 * @param Cant. de cruces que tiene el grafo
	 */
	private void testGraph( int cruces ) throws Exception{
		assertTrue( instancia.dameNodosIV1().equals(nodosIV1));
		assertTrue( instancia.dameNodosIV2().equals(nodosIV2));
		assertTrue( instancia.dameNodosV1().equals(nodosV1));
		assertTrue( instancia.dameNodosV2().equals(nodosV2));
		assertTrue( instancia.dameEjes().equals(ejes));
		assertTrue( instancia.dameEjesI().equals(ejesI));
		

		List<Nodo> nTodos = new ArrayList<Nodo>(nodosV1);
		nTodos.addAll(nodosIV1);
		assertTrue(instancia.dameNodosV1Todos().equals(nTodos));

		nTodos = new ArrayList<Nodo>(nodosV2);
		nTodos.addAll(nodosIV2);
		assertTrue(instancia.dameNodosV2Todos().equals(nTodos));
		
		List<Eje> nTodosE = new ArrayList<Eje>(ejes);
		nTodosE.addAll(ejesI);
		assertTrue( instancia.dameEjesTodos().equals(nTodosE) );
		
		assertTrue( instancia.dameGrafo().cantidadCruces() == cruces );

		int pos = 0;
		for( Nodo n : nodosV1)
			assertTrue( instancia.dameGrafo().getPosicion(n) == pos++ );

		pos = 0;
		for( Nodo n : nodosV2)
			assertTrue( instancia.dameGrafo().getPosicion(n) == pos++ );
	}


	public Instancia setupMenosSimple() {
		setupNodos(NODOS_V1_MENOS_SIMPLE + NODOS_V2_MENOS_SIMPLE);

		nodosV1 = new LinkedList<Nodo>();
		nodosV1.add(n[1]);
		nodosV1.add(n[2]);
		nodosV1.add(n[0]);
		nodosV1.add(n[3]);
		nodosV2 = new LinkedList<Nodo>();
		nodosV2.add(n[5]);
		nodosV2.add(n[4]);
		nodosV2.add(n[6]);
		nodosIV1 = new LinkedList<Nodo>();
		nodosIV1.add(n[7]);
		nodosIV1.add(n[8]);
		nodosIV2 = new LinkedList<Nodo>();
		nodosIV2.add(n[9]);
		nodosIV2.add(n[10]);
		nodosV1.add(n[11]);
		
		v1 = new Eje[ EJES_V1_MENOS_SIMPLE ];
		v1[0] = new Eje(n[0],n[6]);
		v1[1] = new Eje(n[3],n[5]);

		v2 = new Eje[ EJES_V2_MENOS_SIMPLE ];
		v2[0] = new Eje(n[1],n[9]);
		v2[1] = new Eje(n[7],n[4]);
		v2[2] = new Eje(n[7],n[10]);
		v2[3] = new Eje(n[8],n[5]);

		ejes = new LinkedList<Eje>();
		for(int i=0; i < EJES_V1_MENOS_SIMPLE; i++)
			ejes.add(v1[i]);

		ejesI = new LinkedList<Eje>();
		for(int i=0; i < EJES_V2_MENOS_SIMPLE; i++)
			ejesI.add(v2[i]);
		
		return new Instancia( this.nodosV1,  this.nodosV2,  this.ejes,
					   		  this.nodosIV1, this.nodosIV2, this.ejesI);
	}

	@Test
	public void testMenosSimple() throws Exception {
		instancia = setupMenosSimple();
		testGraph( CRUCES_TEST_MENOS_SIMPLE );
	}


	public Instancia setupMartiEstruch() {
		setupNodos(NODOS_V1_MARTI_ESTRUCH + NODOS_V2_MARTI_ESTRUCH);

		nodosV1 = new LinkedList<Nodo>();
		nodosV1.add(n[2]);
		nodosV1.add(n[0]);
		nodosV1.add(n[1]);
		nodosV2 = new LinkedList<Nodo>();
		nodosV2.add(n[4]);
		nodosV2.add(n[3]);
		nodosV2.add(n[5]);
		nodosIV1 = new LinkedList<Nodo>();
		nodosIV1.add(n[6]);
		nodosIV1.add(n[7]);
		nodosIV2 = new LinkedList<Nodo>();
		nodosIV2.add(n[8]);
		nodosIV2.add(n[9]);
		
		v1 = new Eje[ EJES_V1_MARTI_ESTRUCH ];
		v1[0] = new Eje(n[2],n[4]);
		v1[1] = new Eje(n[2],n[3]);
		v1[2] = new Eje(n[0],n[4]);
		v1[3] = new Eje(n[1],n[3]);
		v1[4] = new Eje(n[1],n[5]);

		v2 = new Eje[ EJES_V2_MARTI_ESTRUCH ];
		v2[0] = new Eje(n[6],n[3]);
		v2[1] = new Eje(n[6],n[9]);
		v2[2] = new Eje(n[7],n[8]);
		v2[3] = new Eje(n[8],n[0]);

		ejes = new LinkedList<Eje>();
		for(int i=0; i < EJES_V1_MARTI_ESTRUCH; i ++)
			ejes.add(v1[i]);

		ejesI = new LinkedList<Eje>();
		for(int i=0; i < EJES_V2_MARTI_ESTRUCH; i ++)
			ejes.add(v2[i]);
		
		return new Instancia( this.nodosV1, this.nodosV2, this.ejes,
					   this.nodosIV1, this.nodosIV2, this.ejesI);
	}

	public Instancia setupMartiEstruchOrdenado() {
		setupNodos(NODOS_V1_MARTI_ESTRUCH_ORDENADO + NODOS_V2_MARTI_ESTRUCH_ORDENADO);

		nodosV1 = new LinkedList<Nodo>();
		nodosV1.add(n[7]);
		nodosV1.add(n[2]);
		nodosV1.add(n[0]);
		nodosV1.add(n[6]);
		nodosV1.add(n[1]);
		nodosV2 = new LinkedList<Nodo>();
		nodosV2.add(n[8]);
		nodosV2.add(n[4]);
		nodosV2.add(n[3]);
		nodosV2.add(n[9]);
		nodosV2.add(n[5]);
		nodosIV1 = new LinkedList<Nodo>();
		nodosIV2 = new LinkedList<Nodo>();
		
		v1 = new Eje[ EJES_V1_MARTI_ESTRUCH_ORDENADO ];
		v1[0] = new Eje(n[2],n[4]);
		v1[1] = new Eje(n[2],n[3]);
		v1[2] = new Eje(n[0],n[4]);
		v1[3] = new Eje(n[1],n[3]);
		v1[4] = new Eje(n[1],n[5]);

		v2 = new Eje[ EJES_V2_MARTI_ESTRUCH_ORDENADO ];
		v2[0] = new Eje(n[6],n[3]);
		v2[1] = new Eje(n[6],n[9]);
		v2[2] = new Eje(n[7],n[8]);
		v2[3] = new Eje(n[8],n[0]);

		ejes = new LinkedList<Eje>();
		for(int i=0; i < EJES_V1_MARTI_ESTRUCH_ORDENADO; i ++)
			ejes.add(v1[i]);

		ejesI = new LinkedList<Eje>();
		for(int i=0; i < EJES_V2_MARTI_ESTRUCH_ORDENADO; i ++)
			ejes.add(v2[i]);
		
		return new Instancia(this.nodosV1, this.nodosV2, this.ejes,
					   this.nodosIV1, this.nodosIV2, this.ejesI);
	}

	@Test
	public void testMartiEstruch() throws Exception {
		instancia = setupMartiEstruch();
		testGraph( CRUCES_TEST_MARTI_ESTRUCH );
	}
	

	@Test
	public void testMartiEstruchOrdenado() throws Exception {
		instancia = setupMartiEstruchOrdenado();
		testGraph( CRUCES_TEST_MARTI_ESTRUCH_ORDENADO );
	}

}
