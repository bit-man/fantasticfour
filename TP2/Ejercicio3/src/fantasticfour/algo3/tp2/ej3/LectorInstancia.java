/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author bit-man
 *
 */
public class LectorInstancia {

	private Scanner s;
	
	private int lineas;
	private String oper;
	private String datum;

	public LectorInstancia(String archivo ) throws FileNotFoundException {
        s = new Scanner(
                new File( archivo )
          );
        inicializarVariables();
        obtenerCantLineas();
	}

	public boolean hayMasInstrucciones() {
		return ( lineas != 0 );
	}
	
	public void siguienteOperacion() {
		oper = s.next();
		
		if ( ! oper.equalsIgnoreCase("cardinal") )
			datum = s.next();

		lineas--;
	}
	
	public void siguienteInstancia() {
        inicializarVariables();
        obtenerCantLineas();
	}
	
	public String operacion() {
		return oper;
	}
	
	public String operando() {
		return datum;
	}
	
	public void cerrar() {
		s.close();
	}

	private void inicializarVariables() {
		lineas = 0;
		oper = null;
		datum = null;
	}
	
	private void obtenerCantLineas() {
		lineas = s.nextInt();
	}
}
