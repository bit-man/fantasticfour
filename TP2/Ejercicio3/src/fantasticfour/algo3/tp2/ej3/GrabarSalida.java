/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author bit-man
 *
 */
public class GrabarSalida {

	private PrintWriter pw = null;
    private LinkedList<Integer> resultados = null;

    public GrabarSalida( String archivo ) throws FileNotFoundException {
            pw = new PrintWriter( 
                            new File( archivo ) 
                    );
            nuevosResultados();
    }
    
    public void cadenaPertenece( boolean pertenece ) {

    	if ( pertenece )
    		resultados.add(1);
    	else
    		resultados.add(0);
    	
    };

    public void cantElementos( int n ) {
    	resultados.add(n);
    }
    
    public void grabar() {
           Iterator<Integer> it = resultados.iterator();
           boolean firstItem = true;
           
           while( it.hasNext() ) {
        	   if (firstItem) {
            	   pw.print( it.next() );
            	   firstItem = false;
        	   } else
            	   pw.print( " " + it.next() );
           };
           
           pw.println();
    }
    
    public void nuevosResultados() {
           resultados = new LinkedList<Integer>();
    }
    
    public void cerrar() {
            pw.close();
    }

}
