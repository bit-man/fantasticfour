/**
 * 
 */
package fantasticfour.algo3.tp2.lib;

/**
 * @author bit-man
 *
 */
public class Constantes {
	public static final int SALIR_CON_ERROR = -1;
	public static final int CANT_MAX_LETRAS = 26;
	
	// M�xima altura del �rbol que contiene a lo sumo 26 chars : ceil( log(26) )
	public static final int LOG_MAX_CANT_CHARS = 5; 
	
	public static final String SUFIJO_IN = ".in";
	public static final String SUFIJO_OUT = ".out";
	public static final String SUFIJO_P = ".op";
	
	public static final char[] todosLosChars = new char[] 
	                         { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',  
	                           'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	public static final String FIN_DE_ARCHIVO = "0"; 
	public static final String STRING_VACIO = ""; 

	public static final int	BASE_2	= 2;
	public static final int	BASE_10	= 10;

}
