package fantasticfour.algo3.tp3.ej2;


/**
 * @author bit-man
 *
 * Contador con dígitos y base parametrizables.
 */
public class Contador
{
    private static final int DEFAULT_RESET_VALUE = 0;
    private static final int BASE_10 = 10;
    private int[] contador;
    private int base;
    private boolean overflow;

    /***
     * Crea un contador con la cantidad de dígitos especificada y de base 10 
     * @param digitos
     */
    public Contador(int digitos)
    {
        this(digitos, BASE_10);
    }

    /***
     * Crea un contador con la cantidad de dígitos y base especificados 
     * @param digitos : cant. de dígitos
     * @param base : base del contador
     */

    public Contador(int digitos, int base)
    {
    	Ej2Solver.incrementarOperaciones(digitos);
        contador = new int[digitos];
        this.base = base;
        this.overflow = false;
        reset();
    }

    /***
     * Devuelve el valor del contador.
     * @return
     */
    public long getContador()
    {
        long ret = 0;
        long peso = 1;

        for (int aContador : contador) {
            ret += (aContador * peso);
            peso *= this.base;
            Ej2Solver.incrementarOperaciones(3);
        }

        return ret;
    }

    /***
     * Pone el contador a cero
     */
    public void reset()
    {
        reset(DEFAULT_RESET_VALUE);
    }

    /***
     * Coloca en cada elemento del contador el valor del parámetro
     * @valor inicializador del contador
     */
    public void reset(int valor) {
    	Ej2Solver.incrementarOperaciones(1); //  comparación
        for (int i = 0; i < contador.length; i++) {
            contador[i] = valor;
            Ej2Solver.incrementarOperaciones(3); // acceso contador[i],  comparación e i++
        }
        this.overflow = false;
    }
    
    /***
     * Coloca en cada elemento del contador el valor del parámetro.
     * Si cada elemento de valor[] es igual entonces es más fácil, y ocupa menos
     * memoria, utilizar reset(valor) ya que no es necesario crear el array
     * que sirve de parámetro de entrada a este método
     * 
     * @valor inicializador del contador
     */
    public void reset(int[] valor) {
    	System.arraycopy(valor, 0, contador, 0, contador.length);
        Ej2Solver.incrementarOperaciones(contador.length);
        this.overflow = false;
    }
    

    /***
     * Decrementa el valor del contador. Si el contador ya estaba en cero
     * entonces al decrementarlo vuelve a su valor máximo.
     */
    public void dec() {
        boolean fin = false;
        int i;

        Ej2Solver.incrementarOperaciones(3); // comparación, !fin y && 
        for (i = 0; ((i < contador.length) && !fin); i++)
        {
            Ej2Solver.incrementarOperaciones(2);
            boolean carry = decDigito(i);
            fin = ! carry;

            Ej2Solver.incrementarOperaciones(4); // comparación, !fin, && e i++
        }
        
        this.overflow = (i >= contador.length) && !fin;
        Ej2Solver.incrementarOperaciones(3);    	
    }
    
    /***
     * Decrementa en une el dígito indicado en el parametro 'i'
     * colocańdolo en su valor máximo si este es mmenor que cero (overflow)
     * Devuelve el valor de la condición de overflow luego de 
     * incrementado el dígito 
     * @param i : dígito a incrementar
     * @return indicación de oveflow (o carry)
     */
    public boolean decDigito(int i) {
        Ej2Solver.incrementarOperaciones(2);
    	contador[i]--;

    	// Se usa mayor o igual porque con setDigito() puede colocarse un valor
    	// mayor que el de la base
        Ej2Solver.incrementarOperaciones(2);
        if (contador[i] < 0 ) {
            Ej2Solver.incrementarOperaciones(2);
            contador[i] = this.base - 1;
        }

        this.overflow = false;
        Ej2Solver.incrementarOperaciones(2); // acceso al elemento i + comparación
        return contador[i] == this.base - 1;
    }
    
    
    /***
     * Incrementa el valor del contador. Si el contador ya estaba en el máximo
     * entonces al incrementarlo vuelve a cero.
     */
    public void inc()
    {
        boolean fin = false;
        int i;

        Ej2Solver.incrementarOperaciones(3); // comparación, !fin y && 
        for (i = 0; ((i < contador.length) && !fin); i++)
        {
            Ej2Solver.incrementarOperaciones(2);
            boolean carry = incDigito(i);
            fin = ! carry;
            
            Ej2Solver.incrementarOperaciones(4); // comparación, !fin, && e i++
        }
        
        this.overflow = (i >= contador.length) && !fin;
        Ej2Solver.incrementarOperaciones(3);
    }

    /***
     * Incrementa en uno el dígito indicado en el parametro 'i'
     * colocańdolo en cero si este es mayor que la base (overflow)
     * Devuelve el valor de la condición de overflow luego de 
     * incrementado el dígito 
     * @param i : dígito a incrementar
     * @return indicación de oveflow (o carry)
     */
    public boolean incDigito(int i) {
        Ej2Solver.incrementarOperaciones(2);
    	contador[i]++;

    	// Se usa mayor o igual porque con setDigito() puede colocarse un valor
    	// mayor que el de la base
        Ej2Solver.incrementarOperaciones(2);
        if (contador[i] >= this.base) {
            Ej2Solver.incrementarOperaciones(1);
            contador[i] = 0;
        }

        this.overflow = false;
        Ej2Solver.incrementarOperaciones(2); // acceso al elemento i + comparación
        return contador[i] == 0;
    }
    /***
     * Devuelve el dígito especificado por el parámetro. Este es base cero y su valior
     * debe ser menor a la mayor cantidad de dígitos menos uno
     * @param i : dígito del contador a devolver
     * @return devuelve el valor del dígito correspondiente
     * @throws Exception
     */
    public int getDigito(int i) throws Exception
    {
        Ej2Solver.incrementarOperaciones(1);
        if (i >= this.contador.length)
            throw new Exception(
                "El dígito pedido es mayor que el mayor dígito del contador");
        Ej2Solver.incrementarOperaciones(1);
        return this.contador[i];
    }
    
    public int getDigitos() {
    	return contador.length;
    }
    
    /***
     * Coloca el valor del parámetro 'valor' en la posición 'i'
     * Tener cuidado al usar este método porque no se hace ningún
     * check, puede colocarse un valor mayor que la base o incluso
     * negativo
     * @param i
     * @param valor
     */
    public void setDigito(int i, int valor) {
        Ej2Solver.incrementarOperaciones(1);
    	this.contador[i] = valor;
        this.overflow = false;
    }
    
    /***
     * Indica si la última operación generó un overflow del contador
     * @return
     */
    public boolean dioOverflow() {
    	return this.overflow;
    }
    
    /***
     * Devuelve la base del contador
     * @return
     */
    public int getBase() {
    	return this.base;
    }
}
