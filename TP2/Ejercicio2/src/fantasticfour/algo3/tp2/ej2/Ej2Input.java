package fantasticfour.algo3.tp2.ej2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Ej2Input 
{
	private Scanner s;
		
	public Ej2Input(File archivoEntrada ) 
		throws FileNotFoundException 
	{
		s = new Scanner(  archivoEntrada );
   	}
	
	public Pueblos dameProximaInstancia() 
	{
		Pueblos pueblos = null;
		
		int cantPueblos = s.nextInt();
		if (cantPueblos != 0) { 
			int cantTratados = s.nextInt();
			pueblos = new Pueblos(cantPueblos, cantTratados);
			
			/* En el archivo de entrada, los pueblos van de 1 a n.
			 * Para su ejecución, el algoritmo los asume de 0 a n - 1 
			 * Por eso, es necesario restarle 1 a cada pueblo */
			for (int i=0; i < cantTratados; i++) {
				int pueblo1 = s.nextInt() - 1;
				int pueblo2 = s.nextInt() - 1;
				pueblos.agregarTratado(pueblo1, pueblo2);
				//System.out.println("Agrego tratado: [" + pueblo1 + ", " + pueblo2 + "]");
			}
		}
        return pueblos;
	}
	
	public void cerrar() 
	{	
		s.close();	
	}
}
