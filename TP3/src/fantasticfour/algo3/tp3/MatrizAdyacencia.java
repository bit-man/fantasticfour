/**
 * 
 */
package fantasticfour.algo3.tp3;

import java.util.List;

/**
 * @author bit-man
 *
 */
public class MatrizAdyacencia {
	/***
	 * Matriz de adyacencia propiamente dicha, en la que la primer dimensión
	 * representa a los nodos V1 y al segunda a los nodos V2.
	 * 
	 * ady[i][j] : el nodo v1 en la posición i es adyacente al nodo v2 en
	 *             la posición j
	 */ 
	private boolean[][] ady;
	List<Eje> ejes;
	List<Nodo> nodos;
	
	public MatrizAdyacencia( Grafo g ) {
		int dimV1 = g.dameNodosV1().size();
		int dimV2 = g.dameNodosV2().size();
		ady = new boolean[dimV1][dimV2];
		resetAdy();
		initAdy(g);
		
		initEjes(g);
	}

	public MatrizAdyacencia( Instancia i ) {
		int dimV1 = i.dameNodosV1().size();
		int dimV2 = i.dameNodosV2().size();
		ady = new boolean[dimV1 + i.dameNodosIV1().size()][dimV2 + i.dameNodosIV2().size()];
		resetAdy();
		initAdy(i);
		
		initEjesYNodos(i);
	}



	/**
	 * @param g
	 */
	private void initEjes(Grafo g) {
		ejes = g.dameEjes();
		nodos = g.dameNodosV1();
	}

	/**
	 * @param g
	 */
	private void initEjesYNodos(Instancia i) {
		ejes = i.dameEjes();
		nodos = i.dameNodosV1();
	}
	
	/***
	 * Limpia la matriz de adyacencia (coloca a todos los elementos en false)
	 */
	private void resetAdy() {
		for(int i=0; i < ady.length; i++)
			for(int j=0; j < ady[i].length; j++)
				ady[i][j] = false;
	}
	
	
	/**
	 * Inicializa la matriz de adyacencia según los nodos adyacentes
	 * del grafo pasado como parámetro
	 * @param g
	 */
	private void initAdy(Grafo g) {
		for( Eje e : g.dameEjes() ) {
			int posV1 = pos( e.dameDesde().nodo, g.dameNodosV1() );
			int posV2 = pos( e.dameHasta().nodo, g.dameNodosV2()  );
			ady[posV1][posV2] = true;
		}
	}
	

	/**
	 * Inicializa la matriz de adyacencia según los nodos adyacentes
	 * de  la Instancia pasada como parámetro
	 * @param i
	 */
	private void initAdy(Instancia i) {
		for( Eje e : i.dameEjesTodos() ) {
			int posV1 = pos( e.dameDesde().nodo, i.dameNodosV1Todos() );
			int posV2 = pos( e.dameHasta().nodo, i.dameNodosV2Todos()  );
			ady[posV1][posV2] = true;
		}
	}
	

	/**
	 * Obtiene en qué posición esta el nodo pasado como parámetro en la lista
	 * de nodos
	 * @param ln : lista de nodo
	 * @return
	 */
	
	/***
	 * TODO si es necesario disminuir la complejidad puede recorrerse una vez la
	 * 		matriz y almacenarse la posición de cada nodo en un array 
	 * 		( posición del nodo n ::= pos[n] )
	 * 
	 * TODO discutir lo que decía Gatón sobre almacenar siempre la posición del nodo
	 * 		y al fional convertir todo al nro. de nodo para no tener que andar haciendo
	 * 		conversiones cada vez
	 */ 
	
	private int pos(int nodo, List<Nodo> ln) {
		int pos = 0;
		
		for( Nodo n : ln ) {
			if ( n.nodo == nodo )
				break;
			pos++;
		}
		
		return pos;
	}

	public boolean[][] getMatriz() {
		return ady;
	}
}
