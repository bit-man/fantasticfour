package fantasticfour.algo3.tp2.ej2.test;

import java.io.FileWriter;
import java.io.File;
import java.io.IOException;

public class TestGenerator
{
    private static String OUTPUT_DIR = "C:\\gaston\\uba\\algo3\\200801\\tp\\fantasticfour\\TP2\\Ejercicio2\\src\\Test";

    public static void main(String[] args)
    {
        try
        {
            TestGenerator.createTest1();
            TestGenerator.createTest2();
            TestGenerator.createTest3();
            TestGenerator.createTest4();
            TestGenerator.createTest5();
            TestGenerator.createTest6();
            TestGenerator.createTest7();
            //TestGenerator.createTest8();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
        System.exit(0);
    }


    /**
     * Crea instancias con acuerdos del siguiente tipo, 5-1, 1-4, 4-3, 2-3
     * Ejemplo:
     * 1 0
     * 2 0
     * @throws Exception
     */
    private static void createTest8()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test9.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 6;

        for (int i = 3; i < cantidad ; i=i+2)
        {
            //encabezado del caso
            sb = sb + i + " " + (i-1) + "\n";
            for (int j = 1; j < (i/2)+1; j++)
            {
                sb = sb + (i-j) + " " + ((i + j) % (i -1) ) + "\n";
                sb = sb + ((i + j) % (i-1))+ " " + (i-j-1) + "\n";
            }
        }


        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }

    /**
     * Crea instancias del tipo todos con todos. Sin acuerdo entre vecinos
     * @throws Exception
     */
    private static void createTest7()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test8.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 40;

        for (int i = 4; i < cantidad ; i++)
        {
            int acuerdos = (i * (i - 1)) / 2;
            //encabezado del caso
            sb = sb + i + " " + (acuerdos - (i-1))+ "\n";
            for (int j = 1 ; j < i; j++)
            {
                for (int k = j+1 ; k < i + 1; k++)
                {
                    if (!(j+1 == k || j == k+1))
                    {
                        sb = sb + j + " " + k + "\n";
                    }
                }
            }
        }
        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }

    /**
     * Sin solucion.
     * Crea instancias del tipo todos con todos mas un puerto sin acuerdo.
     * @throws Exception
     */
    private static void createTest6()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test7.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 40;

        for (int i = 2; i < cantidad ; i++)
        {
            int acuerdos = (i * (i - 1)) / 2;
            //encabezado del caso
            sb = sb + (i + 1) + " " + acuerdos + "\n";
            for (int j = 1 ; j < i; j++)
            {
                for (int k = j+1 ; k < i + 1; k++)
                {
                    sb = sb + j + " " + k + "\n";
                }
            }
        }

        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }


    /**
     * Crea instancias con acuerdos del siguiente tipo, 5-1, 1-4, 4-3, 2-3
     * Ejemplo:
     * 1 0
     * 2 0
     * @throws Exception
     */
    private static void createTest5()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test6.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 40;

        for (int i = 3; i < cantidad ; i=i+2)
        {
            //encabezado del caso
            sb = sb + i + " " + (i-1) + "\n";
            for (int j = 1; j < (i/2)+1; j++)
            {
                sb = sb + (i-j+1) + " " + j + "\n";
                sb = sb + j + " " + (i-j) + "\n";
            }
        }

        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }

    /**
     * Crea instancias con ciudades sin acuerdos
     * Ejemplo:
     * 1 0
     * 2 0
     * @throws Exception
     */
    private static void createTest4()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test5.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 40;

        for (int i = 1; i < cantidad ; i++)
        {
            //encabezado del caso
            sb = sb + i + " " + 0 + "\n";
        }

        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }

    /**
     * Crea instancias del tipo 1 con acuerdo con 2 y 3, 2 con acuerdo 3 y 4, etc
     * Ejemplo:
     * 4 3
     * 1 2
     * 1 3
     * 2 3
     * 2 4
     * 3 4
     * @throws Exception
     */
    private static void createTest3()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test4.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 40;

        for (int i = 3; i < cantidad ; i++)
        {
            int acuerdos = ((i -2) * 2) + 1;
            //encabezado del caso
            sb = sb + i + " " + acuerdos + "\n";
            for (int j = 1 ; j < i-1; j++)
            {
                sb = sb + j + " " + (j + 1) + "\n";
                sb = sb + j + " " + (j + 2) + "\n";
            }
            sb = sb + (i-1) + " " + (i) + "\n";
        }

        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }

    /**
     * Crea instancias del tipo todos con todos.
     * @throws Exception
     */
    private static void createTest2()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test3.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 40;

        for (int i = 2; i < cantidad ; i++)
        {
            int acuerdos = (i * (i - 1)) / 2;
            //encabezado del caso
            sb = sb + i + " " + acuerdos + "\n";
            for (int j = 1 ; j < i; j++)
            {
                for (int k = j+1 ; k < i + 1; k++)
                {
                    sb = sb + j + " " + k + "\n";
                }
            }
        }

        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }

    /**
     * Crea instancias del tipo 1 con acuerdo con 2, 2 con acuerdo con 3, etc.
     * @throws Exception
     */
    private static void createTest1()
        throws Exception
    {
        String filePath = OUTPUT_DIR + File.separator + "Tp2Ej2Test2.in";
        File file = crearArchivo(filePath);
        FileWriter fw = new FileWriter(file);
        String sb = "";
        int cantidad = 40;

        for (int i = 2; i < cantidad ; i++)
        {
            //encabezado del caso
            sb = sb + i + " " + (i -1 ) + "\n";
            for (int j = 1; j < i; j++)
            {
                sb = sb + j + " " + (j+1) + "\n";
            }
        }
        sb = finArchivo(sb);
        grabarArchivo(fw, sb);
    }

    private static File crearArchivo(String filePath)
        throws Exception
    {
        File file = new File(filePath);
        file.delete();
        boolean canCreate = file.createNewFile();
        if (!canCreate)
        {
            throw new Exception("Could not create file:" + filePath);
        }
        return file;
    }

    private static void grabarArchivo(FileWriter fw, String sb) throws IOException
    {
        //escribir, grabar, cerrar
        fw.write(sb);
        fw.flush();
        fw.close();
    }

    private static String finArchivo(String sb)
    {
        //fin del archivo
        sb = sb + "0 0";
        return sb;
    }
}