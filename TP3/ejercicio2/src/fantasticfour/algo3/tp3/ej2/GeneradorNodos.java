/**
 * 
 */
package fantasticfour.algo3.tp3.ej2;

/**
 * Genera una secuencia de nodos
 * @author bit-man
 */
public class GeneradorNodos extends Contador {

	private boolean	overflow;
	
	/** 
	 * Genera un grafo de n nodos
	 * @param nodos : cant. de nodos que posee el grafo
	 */
	public GeneradorNodos(int nodos) {
		super(nodos, nodos);
		reset();
		overflow = false;
	}

	/***
	 * Cada posición del contador referencia a un cierto nodo,
	 * y siempre el mismo. El contador[i] corresponda al mismo nodo
	 */
	public void reset() {
        Ej2Solver.incrementarOperaciones( this.getDigitos() );
		int[] init = new int[ this.getDigitos() ];
        Ej2Solver.incrementarOperaciones(1);
        for( int i=0; i < this.getDigitos(); i++)
        {
            init[i] = i;
            Ej2Solver.incrementarOperaciones(1); // acceso al array
            
            Ej2Solver.incrementarOperaciones(2); // i < this.getD... e i++
        }
        this.reset(init);
	}
	
	/***
	 * Hay más grafos, o ya los generé todos ??
	 */
	public boolean hasyMasGrafos() {
        Ej2Solver.incrementarOperaciones(1);
        return !overflow;
	}

	/***
	 * Genera un nuevo grafo. 
	 * No se garantiza el funcionamiento si ya se generaron
	 * todos los grafos necesarios
	 */
	public void otroGrafo() {
		try {
			boolean fin = false;
			int i;

            Ej2Solver.incrementarOperaciones(3); // <, && y !
			for (i=0; i < this.getDigitos() && !fin; i++) {
                int aux = this.getDigito(i);
				boolean carry = this.incDigito(i);
				
				if (carry) {
					setDigito(i,i); // vuelve a su posición inicial
					moverHastaElFinalUnLugar(i+1);
		            Ej2Solver.incrementarOperaciones(1); // i+1
				} else 
					this.setDigito( nodoQueTieneLaPosicion(this.getDigito(i), i), aux );					


	            Ej2Solver.incrementarOperaciones(1);
				fin = ! carry;

	            Ej2Solver.incrementarOperaciones(4); // <, &&, ! e i++
			}

			Ej2Solver.incrementarOperaciones(3);
			overflow = (!fin) && (i == this.getDigitos());
		} catch (Exception e) {
			System.out.print("Este código no debería haberse ejecutado");
			e.printStackTrace();
			// el assert(false) evita que se deba declarar y o atrapar una excepción
			// además de considerarse peligrosa esta condición y abortar el programa
			assert(false);
		}
		
	}
	

	/***
	 * Genera un nuevo grafo. Tiene una doble función :
	 * <li> ser un nombre más corto para el método otroGrafo()
	 * <li> evitar el uso de inc() dque, inadvertidamente usa el del contador, 
	 *      lo que lleva a inconsistencias
	 */
	public void inc() {
		this.otroGrafo();
	}
	
	/**
	 * Comenzando desde el dígito indicado en 'inicial', y hasta el último, genera
	 * un incremento en la posición de los dígitos
	 * @param inicial : primer dígito a incrementar
	 */
	private void moverHastaElFinalUnLugar(int inicial) {
        Ej2Solver.incrementarOperaciones(1);
		for(int i = inicial; i< getDigitos(); i++)
        {
            incDigito(i);
            Ej2Solver.incrementarOperaciones(2);
        }
    }

	/**
	 * @param digito
	 * @return
	 * @throws Exception 
	 */
	private int nodoQueTieneLaPosicion(int pos, int nodoMovido){
		int ret = 0;
		
		try {            
			Ej2Solver.incrementarOperaciones(1);
			for(int i=0; i < this.getDigitos(); i++)
            {
                Ej2Solver.incrementarOperaciones(3);
                if( i != nodoMovido && this.getDigito(i) == pos)
                    ret = i;

                Ej2Solver.incrementarOperaciones(2); // i < this.getD... e i++
            }
        }
        catch (Exception e) {
			System.out.print("Este código no debería haberse ejecutado");
			e.printStackTrace();
			// el assert(false) evita que se deba declarar y o atrapar una excepción
			// además de considerarse peligrosa esta condición y abortar el programa
			assert(false);
		}
			
		return ret;
	}

	public int[] getOrdenNodos() {
        Ej2Solver.incrementarOperaciones( this.getDigitos() );
		int[] ret = new int[ this.getDigitos() ];
		
        Ej2Solver.incrementarOperaciones(1);
		for( int i = 0; i < this.getDigitos(); i++)
        {
            try {
                Ej2Solver.incrementarOperaciones(1);
				ret[i] = this.getDigito(i);
			} catch (Exception e) {
				System.out.print("Este código no debería haberse ejecutado");
				e.printStackTrace();
				// el assert(false) evita que se deba declarar y o atrapar una excepción
				// además de considerarse peligrosa esta condición y abortar el programa
				assert(false);
			}
			Ej2Solver.incrementarOperaciones(2); // i < this.getD... e i++

        }

        return ret;
	}
}
