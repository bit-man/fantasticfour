/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;

import java.io.FileNotFoundException;

import fantasticfour.algo3.tp2.lib.Constantes;

/**
 * @author bit-man
 *
 */
public class Ejercicio3 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if ( args.length != 1 ) {
			ayuda();
		    System.exit( Constantes.SALIR_CON_ERROR );
		}
		

		String prefijo = args[0];

		try {
			Ej3Solver s = new Ej3Solver(
									prefijo + Constantes.SUFIJO_IN,
									prefijo + Constantes.SUFIJO_OUT,
									prefijo + Constantes.SUFIJO_P 
									);
			
			s.resolver();
			s.finalizar();
			
		} catch (FileNotFoundException e) {
			System.err.println("No se puede encontrar/leer el archivo " +
					  		 prefijo + Constantes.SUFIJO_IN );
			System.err.println("o bien no se pueden generar " +
					  		 prefijo + Constantes.SUFIJO_OUT + " o " + prefijo + Constantes.SUFIJO_P );
		} catch( Exception e ) {
			System.err.println( "Ha ocurrido el siguiente error : ");
			System.err.println( e.getMessage() );
			e.printStackTrace();
		}
		
	}
	
	private static void ayuda() {
		System.out.println("java  fantasticfour.algo3.tp2.ej3.Ejercicio3 archivo_sin_la extension");
	}

}
