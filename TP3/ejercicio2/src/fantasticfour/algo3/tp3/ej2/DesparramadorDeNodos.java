
package fantasticfour.algo3.tp3.ej2;

public class DesparramadorDeNodos
{

	private static final int UNICO_DIGITO = 0;
	private static final int	NO_ME_IMPORTA	= 0;
	private static final int	HUECOS_POR_INSTANCIA	= 1;
	private static final int	SIN_NODOS	= 0;

	/***
	 * Cantidad mínima de huecos
	 */ 
	public static final int	CANT_HUECOS_MIN	= 2;

	private int nextInc;
	private Contador c;
	private int cNodos;

	private DesparramadorDeNodos n;
	private int nNodos;
	private int nHuecos;
	
	private boolean soyElUltimo;
	private boolean overflow;
	
    /***
     * Desparrama la cantidad de nodos en la cantidad de huecos, devolviendo en cada
     * dígito cuántos nodos deben almacenarse en el hueco correspondiente.
     * Debido a que debe existir al menos un nodo la cantidad de huecos mínima es 2.
     * @param huecos : cant. de huecos
     * @param nodos : cant. de nodos
     */
    public DesparramadorDeNodos(int huecos, int nodos)
    {
    	/***
    	 * Cada instancia toma un hueco (implementada con un contador de 1 sólo dígito) pero los dos
    	 * último huecos se implementan como un contador de 2 dígitos.
    	 * Por así decirlo, el contenido de todos los contadores es cuántos nodos hay en ese hueco, en
    	 * cambio en la última instancia (donde hay 2 contadores) cada contador representa un nodo (en 
    	 * lugar de un hueco) y en qué hueco se encuentra   
    	 */

        Ej2Solver.incrementarOperaciones(1);
    	if( huecos > CANT_HUECOS_MIN) {
            Ej2Solver.incrementarOperaciones(1);
    		cNodos = nodos + 1;
    		c = new Contador( HUECOS_POR_INSTANCIA, cNodos);
    		c.setDigito(UNICO_DIGITO, nodos);
    		
    		n = new DesparramadorDeNodos(huecos - HUECOS_POR_INSTANCIA, SIN_NODOS);
    		nNodos = SIN_NODOS;

            Ej2Solver.incrementarOperaciones(1);
    		nHuecos = huecos - HUECOS_POR_INSTANCIA;
    		
    		soyElUltimo = false;
    	} else {
    		c = new Contador(nodos, CANT_HUECOS_MIN);
    		cNodos = NO_ME_IMPORTA;
    		
    		n = null;
    		nNodos = NO_ME_IMPORTA;
    		nHuecos = NO_ME_IMPORTA;
    		
    		soyElUltimo = true;
    	}
		nextInc = 0;
		this.overflow = false;
    }

    public void next() {
    	if (soyElUltimo)
    		incContador();
    	else {
    		n.next();
    		if (n.noHayMasCombinaciones()) {
    	        Ej2Solver.incrementarOperaciones(1); // ++nNodos
        		n = new DesparramadorDeNodos(nHuecos, ++nNodos);
    			c.dec();
    			this.overflow = c.dioOverflow();
    		}
    	}
    	
    }

	/**
	 * Incremento el contador interno. Debe ser el último de la cadena
	 */
	private void incContador() {

        Ej2Solver.incrementarOperaciones(1); // comparar por ==
		if ( c.getDigitos() == nextInc ) {
    		nextInc = 0;
    		c.inc(); // me aseguro que de overflow
    	} else {
            Ej2Solver.incrementarOperaciones(1); // nextInc++
    		c.incDigito(nextInc++);
    	}
	}
    
	public int nodosEnElHueco(int h) throws Exception {
		if (soyElUltimo)
			return nodosEnElHuecoContador(h);
		else {
	        Ej2Solver.incrementarOperaciones(2); // h == 0 y h-1
			return ( h == 0 ? c.getDigito(UNICO_DIGITO) : n.nodosEnElHueco(h-1) ); 
		}
	}

    private int nodosEnElHuecoContador(int h) {
    	int ret = 0;
		try {

	        Ej2Solver.incrementarOperaciones(1);  // i < c.getD...
			for(int i = 0; i < c.getDigitos(); i++) {
		        Ej2Solver.incrementarOperaciones(1); // c.getD... == h
				if (c.getDigito(i) == h) {
			        Ej2Solver.incrementarOperaciones(1);
					ret++;
				}

		        Ej2Solver.incrementarOperaciones(2); // i++ y i < c.getD...
			}
		} catch (Exception e) {
			e.printStackTrace();
			assert(false);
		}
			
		return ret;
    }
    
    /***
     * Identifica si hay más combinaciones para extraer
     * @return
     */
    public boolean noHayMasCombinaciones() {
    	if (soyElUltimo)
    		return c.dioOverflow();
    	else
    		return this.overflow;
    }
    
    /***
     * Devuelve la cant. de huecos
     * @return cantidad de huecos
     */
    public int getHuecos() {
    	if (soyElUltimo)
    		return c.getBase();
    	else {
            Ej2Solver.incrementarOperaciones(1);
    		return HUECOS_POR_INSTANCIA + n.getHuecos();
    	}
    }
    
    public int getNodos() {
    	int ret = 0;
    	
    	if (soyElUltimo)
    		ret = c.getDigitos();
		else
			try {
		        Ej2Solver.incrementarOperaciones(1);
				ret = c.getDigito(UNICO_DIGITO) + n.getNodos();
			} catch (Exception e) {
				e.printStackTrace();
				assert(false);
			}	
		
		return ret;
    }

}

