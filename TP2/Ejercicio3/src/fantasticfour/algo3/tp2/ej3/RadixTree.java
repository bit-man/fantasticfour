/**
 * 
 */
package fantasticfour.algo3.tp2.ej3;

import java.util.Collection;
import java.util.TreeMap;

import fantasticfour.algo3.tp2.lib.Constantes;
import fantasticfour.algo3.tp2.lib.NumbersUtil;
import fantasticfour.algo3.tp2.lib.StringUtil;
import fantasticfour.algo3.tp2.lib.Tupla3Generica;

/**
 * @author bit-man
 *
 */
public class RadixTree {

	private static final int UN_SOLO_HIJO = 1;
	private static final String STRING_VACIO = "";
	private int cantCadenas = 0;
	TreeMap<Character,Nodo> siguiente = null;
	
	private int nInstruc = 0;
	private int indiceBifurcante = 0;
	
	public RadixTree() {
		siguiente = new TreeMap<Character,Nodo>();
		cantCadenas = 0;
		nInstruc = 0;
		indiceBifurcante = 0;
	}
	
	public int cardinal() {
		nInstruc++;
		instructRecolectarInstruccEjecutadasDeLosNodos();
		return cantCadenas;
	}
	
	public boolean pertenece( String s ) {
		boolean pertenece = true;
		String st = new String(s);
		nInstruc += s.length();

		Nodo n = siguienteNodo( st );
		
		nInstruc += 1;
		if ( esRaiz(n) )
	        pertenece = false;

		nInstruc += 2;
	    while ( ! st.equals(STRING_VACIO) && pertenece ) {
	    	nInstruc += 1;
	        if ( ! haySiguiente(n) )
	            pertenece = false;
	        else {
        		nInstruc += StringUtil.indicePrimerCharDistinto( n.valor, st );
	            if ( st.startsWith( n.valor ) ) {
	        		nInstruc += n.valor.length() + 1;
	            	st = st.substring( n.valor.length() );

	            	nInstruc += 1;
	            	if ( ! st.equals(STRING_VACIO) )
	            		n =  n.siguienteNodo(st);
	            } else
	                pertenece = false;
	        }
	        
	        nInstruc += 2; // ! st.equals(STRING_VACIO) && pertenece
	    }


	    nInstruc += 1;
		if (n != null ) {
		    // Evito que pregunte por un prefijo de una subcadena
		    // y sea considerada como que pertenece al RadixTree
			nInstruc += 1;
			pertenece &= n.esHoja();
		}
		
		instructRecolectarInstruccEjecutadasDeLosNodos();
	    return pertenece;
	}
	
	
	public int instruc() {
		int ret = nInstruc;
		nInstruc = 0;
		return ret;
	}

	
	public void agregar( String s ) {
		//assert( ! pertenece(s) && ! s.equals(STRING_VACIO));
			 
		Tupla3Generica nb = nodoBifurcante( s );
		String restante = (String) nb.b;
		Nodo ins = (Nodo) nb.a;
		boolean estaAlFinal = (Boolean) nb.c;
		
		nInstruc += 1;
		if ( ! esRaiz(ins) ) {

			nInstruc += 1;
			if ( ! estaAlFinal ) {
				ins = dividirNodo( restante , ins );

				nInstruc += ins.valor.length() + 1;
				restante = restante.substring( ins.valor.length() );
			}
			Nodo hijo = new Nodo( restante );
			agregarHijo( ins, hijo );
		} else {
			Nodo hijo = new Nodo( restante );
			actualizarHijo( hijo );
		}

		nInstruc += 1;
		cantCadenas++;

		instructRecolectarInstruccEjecutadasDeLosNodos();
	}
	
	public void sacar( String s ) {
	    //assert( pertenece(s) );

	   Nodo ultimo  = ultimoNodo( s );
	   comprimir( ultimo );

	   nInstruc += 1;
	   cantCadenas--;   

	   instructRecolectarInstruccEjecutadasDeLosNodos();
	}
	
	private Nodo ultimoNodo( String s ) {
	    //assert(! s.equals(STRING_VACIO) && pertenece( s ) );

		nInstruc += s.length();
	    String st = new String(s);
	    Nodo n = siguienteNodo(s);
	    boolean fin = false;


		nInstruc += 1;
	    while ( ! fin ) {
	 	    nInstruc += 3;
	    	if ( st.length() == n.valor.length() )  // como se que st pertenece al radix, entonces no necesito comparar los strings sino s�lo su longitud  
	            fin = true;
	        else {
	        	nInstruc += n.valor.length() + 1;
	            st = st.substring( n.valor.length() );
	            n = n.siguienteNodo( st );
	        }
	 	    nInstruc += 1; // while( ! fin )
	    }

	    return n;
	}
	
	private void comprimir( Nodo n ) {
	    //assert( ! esRaiz(n) );

	    Nodo padre = n.padre;

	    /**
	     * El nodo n va a ser borrado, as� que necesito extraerle la cantidad de
	     * instrucciones ejecutadas antes que desaparezca
	     */
    	nInstruc += n.instruc();
    	
 	    nInstruc += 1;
	    if ( esRaiz(padre) ) {
	    	borrarHijo(n);
	    	return;
	    };
	    
	    padre.borrarHijo( n );

 	    nInstruc += 1;
	    if ( padre.cantHijos() == UN_SOLO_HIJO ) {
	        Nodo unicoHijo = padre.dameElUnicoHijo();

	 	    nInstruc += unicoHijo.valor.length();
	        unicoHijo.valor = padre.valor + unicoHijo.valor;  
	        unicoHijo.padre = padre.padre;

	 	    nInstruc += 1;
	        if ( esRaiz( padre.padre )) {
	        	borrarHijo( padre );
	        	actualizarHijo( unicoHijo );
	        } else {
	        	padre.padre.borrarHijo( padre );
	            padre.padre.actualizarHijo( unicoHijo, unicoHijo );
	        }
	    }
	}

	private Nodo siguienteNodo( String s ){
 	    nInstruc += instrucGetPutDel() + 1;
	    return  siguiente.get( s.charAt(0) );
	}
	
	private Tupla3Generica nodoBifurcante( String s ) {
 	    nInstruc += s.length();
		String ret = new String(s);
	    boolean coincide = true;
	    boolean alFinal = false;
	    
	    Nodo siguiente = siguienteNodo(ret);
 	    nInstruc += 1;
	    if ( esRaiz(siguiente) )
	    	coincide = false;


 	    nInstruc += 1;
	    while ( coincide ) {

	 	    nInstruc += 1;
	        if ( siguiente == null ) {
	            coincide = false;
	        } else { 
		 	    indiceBifurcante = StringUtil.indicePrimerCharDistinto( ret, siguiente.valor );
		 	    nInstruc += indiceBifurcante;

		 	    nInstruc += 2;
		 	    if ( indiceBifurcante == siguiente.valor.length() ) { // siguiente.valor es prefijo de ret ??
	    	 	    nInstruc += siguiente.valor.length() + 1;
	            	ret = ret.substring( siguiente.valor.length() );
	            	alFinal = true;

	            	if( haySiguiente(siguiente.siguienteNodo(ret)) )
	    	        	siguiente =  siguiente.siguienteNodo(ret);
	            	else
	            		coincide = false;
	            } else {
	                coincide = false;
	                alFinal = false;
	            }
	        }
	 	    nInstruc += 1; // while( coincide )
	    }

	    return new Tupla3Generica(siguiente, ret, alFinal);
	}
	
	private Nodo dividirNodo( String s, Nodo n ) {
		//assert( ! esRaiz(n) );

		int i = indiceBifurcante;
        
 	    nInstruc += i; 
	    String pre = n.valor.substring(0, i);
	    Nodo padre = new Nodo( pre );
	    padre.padre = n.padre;
	    n.padre = padre;

 	    nInstruc += 1; 
	    if ( esRaiz(padre.padre ) ) {
	    	borrarHijo(n);
	    	actualizarHijo(padre);
	    } else {
	    	padre.padre.borrarHijo(n);
	    	padre.padre.actualizarHijo(padre, padre);
	    }
	    
	    nInstruc += (n.valor.length() - i);
	    n.valor = n.valor.substring(i, n.valor.length());

	    padre.actualizarHijo(n, n);

	    return padre;
	}
	
	private void agregarHijo( Nodo padre, Nodo hijo ) {
	    padre.actualizarHijo( hijo, hijo );
	    hijo.padre = padre;
	}
	
	
	private void actualizarHijo( Nodo hijo ) {
 	    nInstruc += instrucGetPutDel() + 1; 
		siguiente.put( hijo.valor.charAt(0), hijo );
	}
	
	private void borrarHijo( Nodo hijo ) {
		nInstruc += instrucGetPutDel() + 1;
		siguiente.remove( hijo.valor.charAt(0) );
	}

	private boolean esRaiz( Nodo n ) {
 	    nInstruc += 1; 
		return ( n == null );
	}

	private boolean haySiguiente( Nodo n ) {
 	    nInstruc += 1; 
		return ( n != null );
	}
	
	/***
	 * Este m�todo es s�lo a los fines de saber cu�ntas instrucciones se ejecutan en cada nodo
	 * por lo tanto no debe sumar cant. de instrucciones ejecutadas a la estructura ni computarse
	 * su complejidad
	 ***/
	private void instructRecolectarInstruccEjecutadasDeLosNodos() {
		for ( char c : siguiente.keySet()) {
			Nodo hijo = siguiente.get(c); 
			nInstruc += hijo.instruc();
			instructObtenerInstruccDeTodosLosNodos( hijo.hijos() );
		}
	}

	/***
	 * Este m�todo es s�lo a los fines de saber cu�ntas instrucciones se ejecutan en cada nodo
	 * por lo tanto no debe sumar cant. de instrucciones ejecutadas a la estructura ni computarse
	 * su complejidad
	 ***/
	private void instructObtenerInstruccDeTodosLosNodos( Collection<Nodo> nodo ) {
		for ( Nodo n : nodo ) { 
			nInstruc += n.instruc();
			instructObtenerInstruccDeTodosLosNodos( n.hijos() );
		}
	}

	private int instrucGetPutDel() {
		if (siguiente.size() == 0)
			return 0;
		else
			return (int) Math.ceil( NumbersUtil.logaritmo( (double) siguiente.size(), Constantes.BASE_2) );
	} 
}
