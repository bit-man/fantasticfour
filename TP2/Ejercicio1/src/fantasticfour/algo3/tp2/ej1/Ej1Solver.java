package fantasticfour.algo3.tp2.ej1;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Vector;

public class Ej1Solver 
{
	private Ej1Input entrada;
	private Ej1Output salida;
	private int cantOperaciones;
	int[] ganadores;
	
	public Ej1Solver(Ej1Input entrada, Ej1Output salida) 
	{
		this.entrada = entrada;
		this.salida = salida;
		this.cantOperaciones = 0;
	}
	
	public void solve() 
		throws IOException	
	{
		Grafo partidosArreglados = null;
		while ((partidosArreglados = entrada.getProximaInstancia()) != null) {
			cantOperaciones = 0;
			Vector<Grafo> sccs = partidosArreglados.getComponentesFuertementeConexas();
			int[] pertenencia = partidosArreglados.getVectorDePertenencia();
			
			agregarOps(partidosArreglados.getCantidadDeOperaciones() + 2);
			
//			for (Grafo g : sccs) {
//				System.out.println(g.toString());
//			}			
//			System.out.print("Vector de pertenencia (a que SCC pertenece cada jugador) [");
//			for (int i : pertenencia) {
//				System.out.print(i + ", ");
//			}
//			System.out.println("]");
	
			boolean[] ganadores = new boolean[sccs.size()];
			agregarOps(3);
			for (int i = 0; i < ganadores.length; i++) {
				ganadores[i] = true;
				agregarOps(4);				
			}
	
			agregarOps(2);
			for (int jugador = 1; jugador <= partidosArreglados.getCantNodos(); jugador++) {
				LinkedList<Integer> partidosArregladosDelJugador = partidosArreglados.getEjesDe(jugador);
				agregarOps(3);
				if (partidosArregladosDelJugador != null) {
					agregarOps(2);
					for (int partidoArreglado = 0; partidoArreglado < partidosArregladosDelJugador.size(); partidoArreglado++) {
						Integer perdedor = partidosArregladosDelJugador.get(partidoArreglado);
						agregarOps(4);
						if (pertenencia[jugador - 1] != pertenencia[perdedor - 1]) {
							// No están en la misma SCC.
							// Entonces la SCC del jugador le GANA a la SCC del perdedor.
							ganadores[pertenencia[perdedor - 1]] = false;
							agregarOps(4);
						}
						agregarOps(3);
					}
				}
			}
	
			int cantGanadores = 0;
			int indiceDeCCGanadora = -1;
			agregarOps(4);
			for (int i = 0; i < ganadores.length; i++) {
				agregarOps(1);
				if (ganadores[i]) {					
					cantGanadores++;
					agregarOps(3);
					if (indiceDeCCGanadora == -1) { 
						indiceDeCCGanadora = i;
						agregarOps(1);
					}
				}
				agregarOps(3);
			}

//			if (cantGanadores == 1) {
//				System.out.println("Se puede arreglar el Torneo!");
//				System.out.println("Para la SCC: " + indiceDeCCGanadora);
//				System.out.print(sccs.get(indiceDeCCGanadora));
//			} else {
//				System.out.println("No se puede arreglar el Torneo!");
//			}
			if (cantGanadores == 1) {
				salida.grabarResultado(sccs.get(indiceDeCCGanadora));
			} else {
				salida.grabarNoResultado();
			}
			
			// Tamaño = n + m (cant de jugadores + cant de partidos arreglados)
			salida.grabarCantOperaciones(entrada.getJugadores() + entrada.getCantPartidosArreglados(), this.cantOperaciones);
		}
	}
	
	private void agregarOps(int i)
	{
		this.cantOperaciones += i;
	}
}