package fantasticfour.algo3.tp2.ej1;

import java.io.File;
import fantasticfour.algo3.tp2.ej1.Ej1Solver;
import fantasticfour.algo3.tp2.ej1.Ej1Input;
import fantasticfour.algo3.tp2.ej1.Ej1Output;

public class Ejercicio1 
{
	public static void main(String[] args)
	{
		if (args.length != 2) {
			printHeader();
			System.out.println("Error: Forma de uso: Ejercicio2 <archInput.in> <archOutput.out>");
			System.exit(1);
		}
		
		File inputFile = new File(args[0]);
		
		if (!inputFile.isFile()) {
			printHeader();
			System.out.println("Error: El archivo " + args[0] + " no existe o es un directorio");
			System.exit(1);
		}
		
		File outputFile = new File(args[1]);

		Ej1Input ej1Input = null;
		Ej1Output ej1Output = null;
		try {
			ej1Input = new Ej1Input(inputFile); 
			ej1Output = new Ej1Output(outputFile);
			Ej1Solver solver = new Ej1Solver(ej1Input, ej1Output);
			
			solver.solve();
			
		} catch (Exception e) {
			printHeader();
			System.out.println("Ocurrió un error durante la ejecución.");
			e.printStackTrace();
			System.exit(1);
		} finally {
			if (ej1Input != null) {
				ej1Input.cerrar();
			}
			
			if (ej1Output != null) {
				ej1Output.cerrar();
			}
		}
	}

	private static void printHeader() 
	{
		System.out.println("Algoritmos III - TP2 - Ej1.");
		System.out.println("Grupo XXX");
	}
}
