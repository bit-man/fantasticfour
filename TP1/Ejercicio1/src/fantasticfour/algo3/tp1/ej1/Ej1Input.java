package fantasticfour.algo3.tp1.ej1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Ej1Input 
{
	private Scanner scanner;

	public Ej1Input(File inputFile) 
	{
		try {
			this.scanner = new Scanner(inputFile);
		} catch (FileNotFoundException e) {	}
	}

	public void close() 
	{
		if (scanner != null) {
			scanner.close();
		}
	}

	public long getNextValue() 
	{
		return scanner.nextLong();
	}
	
	public boolean hasMoreValues() 
	{
		return scanner.hasNextLong();
	}
}
