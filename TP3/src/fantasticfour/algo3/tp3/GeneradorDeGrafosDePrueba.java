package fantasticfour.algo3.tp3;

import fantasticfour.algo3.tp3.ej2.Ej2Solver;
import fantasticfour.algo3.tp3.ej3.Ej3Solver;
import fantasticfour.algo3.tp3.ej4.Ej4Solver;
import fantasticfour.algo3.tp3.ej5.Ej5Solver;

import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GeneradorDeGrafosDePrueba
{
	private static final int	CANT_ARGUMENTOS	= 1;
	private static final int	EXIT_ERROR	= -1;

	private int numeradorNodosV;
	private int denominadorNodosV;
	private int numeradorNodosIV;
	private int denominadorNodosIV;
    private int cantidadNodosTotales;
    private int numeradorProbEjes;
    private int denominadorProbEjes;
    private Random randomGenerator;

	public GeneradorDeGrafosDePrueba(int numeradorNodosV, int denominadorNodosV, int numeradorNodosIV, int denominadorNodosIV, int cantidadNodosTotales, int numeradorProbEjes, int denominadorProbEjes)
	{
		assert numeradorProbEjes <= denominadorProbEjes;
        assert numeradorNodosV <= denominadorNodosV;
        assert numeradorNodosIV <= denominadorNodosIV;

        this.numeradorNodosV = numeradorNodosV;
		this.denominadorNodosV = denominadorNodosV;
		this.numeradorNodosIV = numeradorNodosIV;
		this.denominadorNodosIV = denominadorNodosIV;
        this.cantidadNodosTotales = cantidadNodosTotales;
        this.numeradorProbEjes = numeradorProbEjes;
        this.denominadorProbEjes = denominadorProbEjes;

        randomGenerator = new Random(System.currentTimeMillis());
	}

	public List<Instancia> generar(int cantidadInstancias)
	{
        int cantNodosV = cantidadNodosTotales * numeradorNodosV / denominadorNodosV;
        int cantNodosV1 = cantNodosV / 2;
		int cantNodosV2 = cantNodosV / 2;

        int cantNodosIV = cantidadNodosTotales * numeradorNodosIV / denominadorNodosIV; 
        int cantNodosIV1 = cantNodosIV / 2;
		int cantNodosIV2 = cantNodosIV / 2;

		List<Instancia> listaDeInstancias = new ArrayList<Instancia>(cantidadInstancias);

		for (int instancia = 0; instancia < cantidadInstancias; instancia++) {

			LinkedList<Nodo> nodosV1 = new LinkedList<Nodo>();
			LinkedList<Nodo> nodosV2 = new LinkedList<Nodo>();
			LinkedList<Nodo> nodosIV1 = new LinkedList<Nodo>();
			LinkedList<Nodo> nodosIV2 = new LinkedList<Nodo>();

			// Genero listas de nodos.
			for (int i = 1; i <= cantNodosV1; i++) {
				nodosV1.addLast(new Nodo(i, true));
			}

			for (int i = 1; i <= cantNodosV2; i++) {
				nodosV2.addLast(new Nodo(i + cantNodosV1, true));
			}

			for (int i = 1; i <= cantNodosIV1; i++) {
				nodosIV1.addLast(new Nodo(i + cantNodosV, false));
			}

			for (int i = 1; i <= cantNodosIV2; i++) {
				nodosIV2.addLast(new Nodo(i + cantNodosV + cantNodosIV1, false));
			}

			LinkedList<Eje> ejesV1V2 = new LinkedList<Eje>();
			LinkedList<Eje> ejesI = new LinkedList<Eje>();

			// Ejes V1 -> V2 U IV2
			for (Nodo nodoV1 : nodosV1) {
				// Originales (V1 -> V2).
				for (Nodo nodoV2 : nodosV2) {
					int nextInt = randomGenerator.nextInt(denominadorProbEjes);
					if (nextInt < numeradorProbEjes) {
						ejesV1V2.add(new Eje(nodoV1, nodoV2));
					}
				}

				// V1 -> IV2.
				for (Nodo nodoIV2 : nodosIV2) {
					int nextInt = randomGenerator.nextInt(denominadorProbEjes);
					if (nextInt < numeradorProbEjes) {
						ejesI.add(new Eje(nodoV1, nodoIV2));
					}
				}
			}

			// Ejes IV1 -> V2 U IV2
			for (Nodo nodoIV1 : nodosIV1) {
				// IV1 -> V2.
				for (Nodo nodoV2 : nodosV2) {
					int nextInt = randomGenerator.nextInt(denominadorProbEjes);
					if (nextInt < numeradorProbEjes) {
						ejesI.add(new Eje(nodoIV1, nodoV2));
					}
				}
				// IV1 -> IV2.
				for (Nodo nodoIV2 : nodosIV2) {
					int nextInt = randomGenerator.nextInt(denominadorProbEjes);
					if (nextInt < numeradorProbEjes) {
						ejesI.add(new Eje(nodoIV1, nodoIV2));
					}
				}
			}

			listaDeInstancias.add(new Instancia(nodosV1, nodosV2, ejesV1V2, nodosIV1, nodosIV2, ejesI));
		}
		return listaDeInstancias;
	}

	/**
	 * Muestra los parámetros necesarios para ejecutar los tests
	 */
	private static void usage() {
		System.out.println(
			   "salida : path (prefijo) al archivo donde se graban las instancias\n"
			+  "numeradorProbEjes : numerador de la probabilidad de ejes\n"
			+  "denominadorProbEjes : denominador de la probabilidad de ejes\n"
			+  "numeradorNodosV : numerador de la probabilidad de nodos V\n"
			+  "denominadorNodosV : denominador de la probabilidad de nodos V\n"
			+  "cantidadNodosTotales : cantidad de nodos (nodosV + nodosIV)\n"
            +  "cantidadInstancias : cantidad de instancias a ejecutar\n"
        );
	}

	/***
	 * Genera grafos y ejecuta todos los métodos, excepto el exacto
	 * @param salida : path (prefijo) al archivo donde se graban las instancias
	 * @param numeradorProbEjes : numerador de la probabilidad de ejes
	 * @param denominadorProbEjes : denominador de la probabilidad de ejes
	 * @param numeradorNodosV : numerador de la probabilidad de nodos V
	 * @param denominadorNodosV : denominador de la probabilidad de nodos V
	 * @param cantidadNodosTotales : cantidad de nodos (nodosV + nodosIV)
	 * @throws IOException
	 */
	public static void main(String args[])
							throws IOException
	{
        int[] cantidadNodosTotales = new int[]{10,11,12,13,14,15,16,17,18,19,20,25,30,40,50,60,70};
        int[] numeradorProbEjes = new int[]{1, 3, 5};
        int[] denominadorProbEjes = new int[]{10, 10, 10};
        int[] numeradorNodosV = new int[]{2, 1, 1};
        int[] denominadorNodosV = new int[]{3, 2, 3};
        int cantInstancias = 1;

        if ( (args.length+1) < CANT_ARGUMENTOS)
        {
			usage();
			System.exit(EXIT_ERROR);
		}

        String path = args[0];
        //int numeradorProbEjes = Integer.parseInt(args[1]);
		//int denominadorProbEjes  = Integer.parseInt(args[2]);
		//int numeradorNodosV = Integer.parseInt(args[3]);
		//int denominadorNodosV = Integer.parseInt(args[4]);
        //int cantidadNodosTotales = Integer.parseInt(args[5]);
        //int cantInstancias = Integer.parseInt(args[5]);

        for (int i = 0; i < cantidadNodosTotales.length; i++)
        {
            int cantidadNodosActual = cantidadNodosTotales[i];
            for (int k = 0; k < numeradorProbEjes.length; k++)
            {
                int numeradorProbEjesActual = numeradorProbEjes[k];
                int denominadorProbEjesActual = denominadorProbEjes[k];
                for (int l = 0; l < numeradorNodosV.length; l++)
                {
                    int numeradorNodosVActual = numeradorNodosV[l];
                    int denominadorNodosVActual = denominadorNodosV[l];
                    int numeradorNodosIVActual = denominadorNodosVActual - numeradorNodosVActual;
                    int denominadorNodosIVActual = denominadorNodosVActual;

                    GeneradorDeGrafosDePrueba gen = new GeneradorDeGrafosDePrueba(numeradorNodosVActual, denominadorNodosVActual, numeradorNodosIVActual, denominadorNodosIVActual, cantidadNodosActual, numeradorProbEjesActual, denominadorProbEjesActual);
                    List<Instancia> listaDeInstancias = gen.generar(cantInstancias);

                    for (int j = 0; j < listaDeInstancias.size(); j++)
                    {
                        Instancia instancia = listaDeInstancias.get(j);
                        String archivoEntradaNombre = path + File.separator + "inst" + j + "_" + numeradorProbEjesActual + "_" + denominadorProbEjesActual + "probEjes_" + numeradorNodosVActual + "_" + denominadorNodosVActual + "probNodos_" + cantidadNodosActual + "cantNodos_" +  "iv.in";
                        InputGrabar inG = new InputGrabar(archivoEntradaNombre);

                        grabarInstanciaGenerada(inG, instancia);
                        inG.cerrar();

                        if (cantidadNodosActual <= 20)
                        {
                            try
                            {
                                String archivoSalidaNombre = archivoEntradaNombre + ".out2";
                                Output output = new Output(archivoSalidaNombre);
                                //OutputOperaciones operaciones = new OutputOperaciones(archivoSalidaNombre);

                                //int cantidadCrucesOriginal = instancia.dameGrafo().cantidadCruces();
                                Ej2Solver ej2Solver = new Ej2Solver(instancia);
                                Grafo grafo = ej2Solver.solve();
                                output.setIV1(grafo.dameNodosV1());
                                output.setIV2(grafo.dameNodosV2());
                                output.setKID(grafo.cantidadCruces());
                                output.grabar();
                                output.nuevosResultados();

                                //operaciones.setValues(grafo.dameEjes().size(), ej2Solver.cantidadOperaciones(), cantidadCrucesOriginal - grafo.cantidadCruces());
                                //operaciones.grabar();
                                output.cerrar();
                            }
                            catch (Throwable t)
                            {
                                t.printStackTrace();
                            }
                        }

                        try
                        {
                            String archivoSalidaNombre = archivoEntradaNombre + ".out3";
                            Output output = new Output(archivoSalidaNombre);
                            //OutputOperaciones operaciones = new OutputOperaciones(archivoSalidaNombre);

                            //int cantidadCrucesOriginal = instancia.dameGrafo().cantidadCruces();
                            Ej3Solver ej3Solver = new Ej3Solver(instancia);
                            Grafo grafo = ej3Solver.solve();
                            output.setIV1(grafo.dameNodosV1());
                            output.setIV2(grafo.dameNodosV2());
                            output.setKID(grafo.cantidadCruces());
                            output.grabar();
                            output.nuevosResultados();

                            //operaciones.setValues(grafo.dameEjes().size(), ej3Solver.cantidadOperaciones(), cantidadCrucesOriginal - grafo.cantidadCruces());
                            //operaciones.grabar();
                            output.cerrar();
                        }
                        catch (Throwable t)
                        {
                            t.printStackTrace();
                        }

                        try
                        {
                            String archivoSalidaNombre = archivoEntradaNombre + ".out4";
                            Output output = new Output(archivoSalidaNombre);
                            //OutputOperaciones operaciones = new OutputOperaciones(archivoSalidaNombre);

                            //int cantidadCrucesOriginal = instancia.dameGrafo().cantidadCruces();
                            Ej4Solver ej4Solver = new Ej4Solver(instancia);
                            Grafo grafo = ej4Solver.solve(false);
                            output.setIV1(grafo.dameNodosV1());
                            output.setIV2(grafo.dameNodosV2());
                            output.setKID(grafo.cantidadCruces());
                            output.grabar();
                            output.nuevosResultados();

                            //operaciones.setValues(grafo.dameEjes().size(), ej4Solver.getCantidadDeOperaciones(), cantidadCrucesOriginal - grafo.cantidadCruces());
                            //operaciones.grabar();
                            output.cerrar();
                        }
                        catch (Throwable t)
                        {
                            t.printStackTrace();
                        }

                        try
                        {
                            String archivoSalidaNombre = archivoEntradaNombre + ".out5";
                            Output output = new Output(archivoSalidaNombre);
                            //OutputOperaciones operaciones = new OutputOperaciones(archivoSalidaNombre);

                            //int cantidadCrucesOriginal = instancia.dameGrafo().cantidadCruces();
                            Ej5Solver ej5Solver = new Ej5Solver(instancia);
                            Grafo grafo = ej5Solver.solve();
                            output.setIV1(grafo.dameNodosV1());
                            output.setIV2(grafo.dameNodosV2());
                            output.setKID(grafo.cantidadCruces());
                            output.grabar();
                            output.nuevosResultados();

                            //operaciones.setValues(grafo.dameEjes().size(), ej5Solver.cantidadOperaciones(), cantidadCrucesOriginal - grafo.cantidadCruces());
                            //operaciones.grabar();
                            output.cerrar();
                        }
                        catch (Throwable t)
                        {
                            t.printStackTrace();
                        }
                    }
                }
            }
        }
    }

	/**
	 * Graba la instancia pasada como parámetro
	 * @param inG
	 * @param instancia
	 */
	private static void grabarInstanciaGenerada(InputGrabar inG, Instancia instancia) 
	{
		inG.setNodosV1(instancia.dameNodosV1());
		inG.setNodosV2(instancia.dameNodosV2());
		inG.setEjes(instancia.dameEjes());
		inG.setNodosIV1(instancia.dameNodosIV1());
		inG.setNodosIV2(instancia.dameNodosIV2());
		inG.setEjesI(instancia.dameEjesI());
		inG.grabar();
	}
}
