/**
 * 
 */
package fantasticfour.algo3.tp2.lib;

/**
 * @author bit-man
 *
 */
public class TuplaGenerica {
	public Object x = null;
	public Object y = null;

	public TuplaGenerica() {
		x = null;
		y = null;
	}
	
	public TuplaGenerica( Object x, Object y ) {
		this.x = x;
		this.y = y;
	}
	
	public TuplaGenerica( Object x ) {
		this.x = x;
		y = null;
	}
	
	public void swap() {
		Object aux = x;
		x = y;
		y = aux;
	}
}
