package fantasticfour.algo3.tp3;

public class Eje {
    private Nodo desde;
    private Nodo hasta;

    public Eje(Nodo desde, Nodo hasta)
    {
        this.desde = desde;
        this.hasta = hasta;
    }

	public Eje(Eje e) {
		this(e.desde, e.hasta);
	}

	public Nodo dameDesde()
    {
        return this.desde;
    }

    public Nodo dameHasta()
    {
        return this.hasta;
    }

    public boolean equals(Object o)
    {
        if (o == this)
        {
            return true;
        }
        if (!(o instanceof Eje))
        {
            return false;
        }
        Eje eje = (Eje) o;

        return (desde == null ? eje.desde == null : desde.equals(eje.desde)) &&
                (hasta == null ? eje.hasta == null : hasta.equals(eje.hasta));
    }

    public int hashCode()
    {
        int result = 17;
        result = 37 * result + desde.hashCode();
        result = 37 * result + hasta.hashCode();
        return result;
    }
    
    public String toString() 
    {
    	return "[" + this.desde + ", " + this.hasta + "]";
    }
}