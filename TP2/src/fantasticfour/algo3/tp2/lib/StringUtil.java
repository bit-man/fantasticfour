/**
 * 
 */
package fantasticfour.algo3.tp2.lib;

/**
 * @author bit-man
 *
 */
public class StringUtil {
	
	public static int indicePrimerCharDistinto( String str1, String str2) {
		assert( ! str1.equals(str2));

		int i = 0;

		while( i < str1.length() && i < str2.length() &&
				str1.charAt(i) == str2.charAt(i)   )
			i++;
		
		return i;
	}

}
