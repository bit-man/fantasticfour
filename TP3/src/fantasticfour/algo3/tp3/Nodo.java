package fantasticfour.algo3.tp3;

public class Nodo
{
    int nodo;
    boolean perteneceAGrafoOriginal = false;
    
    public Nodo (int nodo) 
    {
    	this(nodo,false);
    }
   
    public Nodo(int nodo, boolean perteneceAOriginal)
    {
        this.nodo = nodo;
        this.perteneceAGrafoOriginal = perteneceAOriginal;
    }

    public boolean equals(Object o)
    {
        if (o == this)
        {
            return true;
        }
        if (!(o instanceof Nodo))
        {
            return false;
        }
        Nodo nodo = (Nodo) o;

        return (this.nodo == nodo.nodo);
    }

    public int hashCode()
    {
        int result = 17;
        result = 37 * result + nodo;
        return result;
    }

    public String toString()
    {
        return "" + this.nodo;
    }
    
    public boolean esOriginal()
    {
    	return this.perteneceAGrafoOriginal;
    }
}