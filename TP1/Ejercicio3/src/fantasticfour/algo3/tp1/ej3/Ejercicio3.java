package fantasticfour.algo3.tp1.ej3;

import java.io.File;
import fantasticfour.algo3.tp1.ej3.Ej3Solver;
/*
 * @author mrugnone
 */
public class Ejercicio3 
{
	public static void main(String[] args)
	{
		if (args.length != 2) {
			printHeader();
			System.out.println("Error: Forma de uso: Ejercicio3 <archInput.in> <archOutput.out>");
			System.exit(1);
		}
		
		File inputFile = new File(args[0]);
		
		if (!inputFile.isFile()) {
			printHeader();
			System.out.println("Error: El archivo " + args[0] + " no existe o es un directorio");
			System.exit(1);
		}
		
		File outputFile = new File(args[1]);

		Entrada entrada = null;
		Salida salida = null;
		try {
			entrada = new Entrada(inputFile); 
			salida = new Salida(outputFile);
			Ej3Solver solver = new Ej3Solver(entrada, salida);
			
			solver.solve();
			
		} catch (Exception e) {
			printHeader();
			System.out.println("Ocurrió un error durante la ejecución.");
			e.printStackTrace();
			System.exit(1);
		} finally {
			if (entrada != null) {
				entrada.cerrar();
			}
			
			if (salida != null) {
				salida.cerrar();
			}
		}
	}

	private static void printHeader() 
	{
		System.out.println("Algoritmos III - TP1 - Ej3.");
		System.out.println("Grupo XXX");
	}
}

