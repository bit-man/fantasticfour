package fantasticfour.algo3.tp2.ej2;

public class Pueblos 
{
	
	public Matriz tratados;
	private int cantPueblos;
	private int cantTratados;
	
	public Pueblos(int cantPueblos, int cantTratados)
	{
		this.cantPueblos = cantPueblos;
		this.cantTratados = cantTratados; 
		this.tratados = new Matriz(cantPueblos);
	}
	
	public int puebloSiguiente(int pueblo)
	{
		/* Recordar que los pueblos están numerados de 0 a n - 1 */
		if (pueblo == cantPueblos - 1)
			return 0;
		else	
			return pueblo + 1;
	}
	
	public int puebloAnterior(int pueblo)
	{
		/* Recordar que los pueblos están numerados de 0 a n - 1 */
		if (pueblo == 0)
			return cantPueblos - 1;
		else	
			return pueblo - 1;
	}
	
	public void agregarTratado(int pueblo1, int pueblo2)	
	{
		tratados.agregar(pueblo1, pueblo2);
	}
	
	public boolean hayTratadoEntre(int pueblo1 , int pueblo2)
	{
		return tratados.obtenerValor(pueblo1, pueblo2);
	}
	
	public int obtenerCantPueblos()
	{
		return this.cantPueblos;
	}
	
	public int obtenerCantTratados()
	{
		return this.cantTratados;
	}
	
	
	public int puebloAnteriorADistancia(int pueblo, int distancia) 
	{
		int retVal = pueblo;
		while(distancia != 0) {
			retVal = this.puebloAnterior(retVal);
			distancia--;
		}
		
		return retVal;
	}
}
