/**
 * 
 */
package fantasticfour.algo3.tp2.lib;

/**
 * @author bit-man
 *
 */
public class Tupla3Generica {
	public Object a = null;
	public Object b = null;
	public Object c = null;

	public Tupla3Generica() {
		a = null;
		b = null;
		c = null;
	}
	
	public Tupla3Generica( Object x, Object y, Object z ) {
		this.a = x;
		this.b = y;
		this.c = z;
	}
}
