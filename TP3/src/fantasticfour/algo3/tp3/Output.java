/**
 * 
 */
package fantasticfour.algo3.tp3;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * @author bit-man
 * Genera el archivo de salida
 */
public class Output {

	/**
	 * 
	 */
	private static final String	NEW_LINE	= "\n";
	/**
	 * 
	 */
	private static final String	ARCHIVO_VACIO	= "";
	/**
	 * 
	 */
	private static final String	ESPACIO_EN_BLANCO	= " ";
	private static final String	EXTENSION_OUT	= ".out";
	private PrintWriter pw = null;
	private String nombreArchivo;
	private int cantCruces;
	private List<Nodo> nodosIV1 = null;
	private List<Nodo> nodosIV2 = null;
	private String toString;

	/***
	 * Constructor que requiere el prefijo del archivo y el método usado
	 * (nro. de ejercicio)
	 * @param prefijo : parte inicial del archivo
	 * @param metodo : nro. de ejercicio (valores válidos, de 2 a 5)
	 * @throws FileFoundException : si el archivo ya existe
	 * @throws IOException : cuando hay un error en la operación de I/O del archivo
	 */
    public Output( String prefijo, int metodo ) throws FileFoundException, IOException {
    	nombreArchivo = prefijo + metodo + EXTENSION_OUT;
    	File f = new File( nombreArchivo );
    	if (f.exists())
        {
            f.delete();
        }
    	
    	f.createNewFile();
	    pw = new PrintWriter( f );
	    nuevosResultados();
	    toString = ARCHIVO_VACIO;
    }

    public Output( String nombreArchivo ) throws FileFoundException, IOException {
    	File f = new File( nombreArchivo );
    	if (f.exists())
        {
            f.delete();
        }

    	f.createNewFile();
	    pw = new PrintWriter( f );
	    nuevosResultados();
	    toString = ARCHIVO_VACIO;
    }

    /***
     * Establece la cantidad de cruces mínimos para esta instancia
     * del problema
     * @param kid : cant de cruces de la instancia
     */
    public void setKID( int kid ) {
    	this.cantCruces = kid;
    }
    
    /***
     * Establece los nodos IV1 para esta instancia del problema
     * @param nodosIV1 : lista de nodos IV1
     */
    public void setIV1( List<Nodo> nodosIV1 ) {
    	this.nodosIV1 = nodosIV1;
    }
    
    /***
     * Establece los nodos IV2 para esta instancia del problema
     * @param nodosIV2 : lista de nodos IV2
     */
    public void setIV2( List<Nodo> nodosIV2 ) {
    	this.nodosIV2 = nodosIV2;
    }

    
    /***
     * Grabar los resultados en el archivo, según el formato especificado
     * en el enunciado
     */
    public void grabar() {
    	pw.println(this.cantCruces);
    	toString += cantCruces + NEW_LINE;
    	imprimirNodos(nodosIV1);
    	imprimirNodos(nodosIV2);
    	pw.flush();
    }
    
    /**
     * Graba en el archivo la lista de nodos pasada como parámetro
	 * @param nodos : lista de nodos a grabar
	 */
	private void imprimirNodos(List<Nodo> nodos) {
		pw.print(nodos.size());	
		toString += nodos.size();
		
		for( Nodo e : nodos ) {
			pw.print(ESPACIO_EN_BLANCO + e);
			toString += ESPACIO_EN_BLANCO + e;
		}
		
		pw.println();
		toString += NEW_LINE;
	}

	public void nuevosResultados() {
    	cantCruces = 0;
    	nodosIV1 = new LinkedList<Nodo>();
    	nodosIV2 = new LinkedList<Nodo>();
    }
    
    public void cerrar() {
            pw.close();
    }
    
    public String toString() {
    	return toString;
    }
    
    public String getNombreArchivo() {
    	return nombreArchivo;
    }
}
