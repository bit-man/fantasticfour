package fantasticfour.algo3.tp1.ej3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class Entrada {
	private Scanner scanner;

	public Entrada(File archivoEntrada) 
	{
		try {
			this.scanner = new Scanner(archivoEntrada);
		} catch (FileNotFoundException e) {	}
	}

	public void cerrar() 
	{
		if (scanner != null) {
			scanner.close();
		}
	}

	public int dameProxValor() 
	{
		return scanner.nextInt();
	}
	
	public boolean hayMasValores() 
	{
		return scanner.hasNextInt();
	}
}
