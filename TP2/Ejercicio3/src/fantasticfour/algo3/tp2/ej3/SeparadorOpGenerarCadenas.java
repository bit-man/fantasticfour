package fantasticfour.algo3.tp2.ej3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import fantasticfour.algo3.tp2.lib.Constantes;
import fantasticfour.algo3.tp2.ej3.Const;

public class SeparadorOpGenerarCadenas {

	private static Scanner archivoEntrada;
	private static PrintWriter archivoSalida;
	private static int cantCadenas;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if( args.length < 3 ) {
			ayuda();
			System.exit( Constantes.SALIR_CON_ERROR );
		}
		

		try {
			archivoEntrada = new Scanner( new File (args[0]) );
			archivoSalida = new PrintWriter( new File (args[1]) );
			String tipo = args[2];

			if ( tipo.equals(Const.TIPO_COMPARTIDAS) && args.length > 3 ) {
				cantCadenas = Integer.parseInt( args[3] );
			}
			
			convertirArchivo(tipo);
			
		} catch (FileNotFoundException e) {
			System.out.println("No puedo abrir el archivo " + args[0]);
			System.out.println( e.getMessage() );
		} catch( Exception e) {
			System.out.println("Se ha producido un error !!! :-(");
			System.out.println( e.getMessage() );
		}
			
	}

	private static void convertirArchivo(String tipo) throws Exception {
		if ( tipo.equals(Const.TIPO_UNICAS) )
			convertirArchivoCadenasUnicas();
		else if ( tipo.equals(Const.TIPO_COMPARTIDAS) )
			convertirArchivoCadenasCompartidas();
		else
			throw new Exception("No existe el tipo de cadena " + tipo);
		
	}

	/**
	 * 
	 */
	private static void convertirArchivoCadenasCompartidas() {
		String[] linea = new String[cantCadenas];

		for( int i=0; i < cantCadenas && archivoEntrada.hasNext(); i++) {  // Toma la cant. de instrucciones usadas para ejecutar :
			linea[i] = archivoEntrada.next() + ",";		// comando agregar
			linea[i] += archivoEntrada.next() + ",";			// comando cardinal
			linea[i] += archivoEntrada.next() + ",";			// comando pertenece		
		}
		
		for( int i=0; i < cantCadenas && archivoEntrada.hasNext(); i++) {
			linea[i] += archivoEntrada.next();
			archivoSalida.println( linea[i] );
		}

		archivoSalida.close();
	}

	private static void convertirArchivoCadenasUnicas() {
		while( archivoEntrada.hasNext() ) {  // Toma la cant. de instrucciones usadas para ejecutar :
			String linea = archivoEntrada.next() + ",";		// comando agregar
			linea += archivoEntrada.next() + ",";			// comando cardinal
			linea += archivoEntrada.next() + ",";			// comando pertenece
			linea += archivoEntrada.next();					// comando sacar
			archivoSalida.println(linea);
		}

		archivoSalida.close();
	}

	private static void ayuda() {
		System.out.println("java fantasticfour.algo3.tp2.ej3.SeparadorOpGenerarCadenas\n"
					     + "	   archivo_op archivo_de_salida tipo_cadena [cant_cadenas]");
		
	}
	
}
