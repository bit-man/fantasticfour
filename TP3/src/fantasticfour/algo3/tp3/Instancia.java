package fantasticfour.algo3.tp3;

import java.util.LinkedList;

public class Instancia
{
    private LinkedList<Nodo> nodosV1; // nodos de V1
    private LinkedList<Nodo> nodosV2; // nodos V2
    private LinkedList<Nodo> nodosIV1; // nodos IV1
    private LinkedList<Nodo> nodosIV2; // nodos IV2
    private LinkedList<Nodo> nodosV1Todos; // nodos V1 + IV1
    private LinkedList<Nodo> nodosV2Todos; // nodos V2 + IV2
    private LinkedList<Eje> ejes; // ejes entre V1 y V2
    private LinkedList<Eje> ejesI; // ejes a agregar al grafo original
    private LinkedList<Eje> ejesTodos; // ejes + ejesI
    private Grafo grafoOriginal;
    private Grafo grafo;
    private long cantidadOperaciones = 0;

    protected Instancia clone()
            throws CloneNotSupportedException
    {
        return new Instancia(new LinkedList<Nodo>(this.nodosV1),
                             new LinkedList<Nodo>(this.nodosV2),
                             new LinkedList<Eje>(this.ejes),
                             new LinkedList<Nodo>(this.nodosIV1),
                             new LinkedList<Nodo>(this.nodosIV2),
                             new LinkedList<Eje>(this.ejesI));
    }

    public Instancia(LinkedList<Nodo> nodosV1,  LinkedList<Nodo> nodosV2,  LinkedList<Eje> ejes,
    				 LinkedList<Nodo> nodosIV1, LinkedList<Nodo> nodosIV2, LinkedList<Eje> ejesI)
    {
        this.nodosV1 = nodosV1;
        this.nodosV2 = nodosV2;
        this.ejes = ejes;
        this.nodosIV1 = nodosIV1;
        this.nodosIV2 = nodosIV2;
        this.ejesI = ejesI;
        this.grafoOriginal = new Grafo(nodosV1, nodosV2, ejes);
        cantidadOperaciones += grafoOriginal.getCantidadDeOperaciones();
        LinkedList<Nodo> todosNodosV1 = new LinkedList<Nodo>(nodosV1);
        todosNodosV1.addAll(nodosIV1);
        cantidadOperaciones += nodosV1.size() + nodosIV1.size();
        LinkedList<Nodo> todosNodosV2 = new LinkedList<Nodo>(nodosV2);
        todosNodosV2.addAll(nodosIV2);
        cantidadOperaciones += nodosV2.size() + nodosIV2.size();
        LinkedList<Eje> todosEjes = new LinkedList<Eje>(ejes);
        todosEjes.addAll(ejesI);
        cantidadOperaciones += ejes.size() + ejesI.size();
        this.grafo = new Grafo(todosNodosV1, todosNodosV2, todosEjes);
        cantidadOperaciones += grafo.getCantidadDeOperaciones();
    }

	public Grafo dameGrafoOriginal()
    {
        return this.grafoOriginal;
    }

    public Grafo dameGrafo()
    {
        return this.grafo;
    }

    public LinkedList<Nodo> dameNodosV1()
    {
        return this.nodosV1;
    }

    public LinkedList<Nodo> dameNodosV2()
    {
        return this.nodosV2;
    }

    public LinkedList<Eje> dameEjes()
    {
        return this.ejes;
    }

    public LinkedList<Nodo> dameNodosIV1()
    {
        return this.nodosIV1;
    }

    public LinkedList<Nodo> dameNodosIV2()
    {
        return this.nodosIV2;
    }

    public LinkedList<Eje> dameEjesI()
    {
        return this.ejesI;
    }

    public LinkedList<Nodo> dameNodosV1Todos()
    {
    	cantidadOperaciones += 1;
        if (nodosV1Todos == null)
        {
            nodosV1Todos = new LinkedList<Nodo>(this.nodosV1);
            nodosV1Todos.addAll(nodosIV1);
            cantidadOperaciones += nodosV1.size() + nodosIV1.size();
        }
        return nodosV1Todos;
    }

    public LinkedList<Nodo> dameNodosV2Todos()
    {
        if (nodosV2Todos == null)
        {
            nodosV2Todos = new LinkedList<Nodo>(this.nodosV2);
            nodosV2Todos.addAll(nodosIV2);
            cantidadOperaciones += nodosV2.size() + nodosIV2.size();
        }
        return nodosV2Todos;
    }

    public LinkedList<Eje> dameEjesTodos()
    {
    	cantidadOperaciones += 1;
        if (ejesTodos == null)
        {
            ejesTodos = new LinkedList<Eje>(this.ejes);
            ejesTodos.addAll(ejesI);
            cantidadOperaciones += ejes.size() + ejesI.size();
        }
        return ejesTodos;
    }

    /*
     * Devuelve la lista de nodos adyacentes a "nodo" en el grafo pasado por parámetro.
     */
    public LinkedList<Eje> dameEjesConAdyacentes(Grafo grafo, Nodo nodo, boolean estaEnV1)
    {
        LinkedList<Eje> ejes = new LinkedList<Eje>();
        cantidadOperaciones = cantidadOperaciones + 1;
        LinkedList<Nodo> adyacentes = this.grafo.dameAdyacentesA(nodo);

        for (Nodo adyacente : adyacentes) {
            cantidadOperaciones = cantidadOperaciones + 
            					( grafo.dameNodos().contains(adyacente) ? 
            							grafo.dameNodos().indexOf(adyacente) :
            							grafo.dameNodos().size() 
            					);
            if (grafo.dameNodos().contains(adyacente)) {
                ejes.add(estaEnV1 ? new Eje(nodo, adyacente) : new Eje(adyacente, nodo));
                cantidadOperaciones = cantidadOperaciones + 2;
            }
        }

        return ejes;
    }

    public long getCantidadDeOperaciones()
    {
        long ops = cantidadOperaciones;
        this.cantidadOperaciones = 0;
        return ops;
    }
}
