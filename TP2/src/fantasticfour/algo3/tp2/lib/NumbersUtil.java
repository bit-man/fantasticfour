package fantasticfour.algo3.tp2.lib;


public class NumbersUtil {

	public static int minimo( int a, int b) {
		if (a < b )
			return a;
		else
			return b;	
	}
	
	public static int maximo( int a, int b) {
		if (a > b )
			return a;
		else
			return b;	
	}
	
	public static double logaritmo( double a, int base ){
		   return ( java.lang.Math.log( a ) / java.lang.Math.log( (double) base ) );
	}

}
