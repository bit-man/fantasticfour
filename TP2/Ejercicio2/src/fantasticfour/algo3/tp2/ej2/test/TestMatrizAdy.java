/**
 * 
 */
package fantasticfour.algo3.tp2.ej2.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fantasticfour.algo3.tp2.ej2.Matriz;


/**
 * @author bit-man
 *
 */
public class TestMatrizAdy {
	/**
	 * 
	 */
	private static final int	FILA_2	= 2;

	/**
	 * 
	 */
	private static final int	COLUMNA_3	= 3;

	/**
	 * 
	 */
	private static final int	DEFAULT_DIMENSION	= 5;

	private Matriz m;

	@Before	
	public void setUp() throws Exception {
		m = new Matriz(DEFAULT_DIMENSION);
		assertTrue( m.dimension() == DEFAULT_DIMENSION );
	}
	
	@Test
	public void testTodosFalse() throws Exception {
		
		for( int p1=1; p1 <= m.dimension(); p1++ )
			for( int p2=1; p2 <= m.dimension(); p2++ )
				assertFalse( m.obtenerValor(p1,p2) );
	}
	

	@Test
	public void testTodosFalseMenosUno() throws Exception {
		m.agregar(FILA_2, COLUMNA_3);
		assertTrue( m.obtenerValor(FILA_2,COLUMNA_3) );
		assertTrue( m.obtenerValor(COLUMNA_3,FILA_2) );
		
		for( int p1=1; p1 <= m.dimension(); p1++ )
			for( int p2=1; p2 <= m.dimension(); p2++ )
				if ( (p1 == FILA_2 && p2 == COLUMNA_3 ) ||
					 (p1 == COLUMNA_3 && p2 == FILA_2) ) 
					assertTrue( m.obtenerValor(p1,p2) );
				else
					assertFalse( m.obtenerValor(p1,p2) );
		
		String rep = "[ 0 0 0 0 0 ]\n"
		           + "[ 0 0 1 0 0 ]\n"
		           + "[ 0 1 0 0 0 ]\n"
		           + "[ 0 0 0 0 0 ]\n"
		           + "[ 0 0 0 0 0 ]\n";
		assertTrue( m.toString().equals(rep) );
	}
}
